/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup HdiAudio
 * @{
 *
 * @brief Audio模块接口定义。
 *
 * 音频接口涉及数据类型、驱动加载接口、驱动适配器接口、音频播放接口、音频录音接口等。
 *
 * @since 4.1
 * @version 2.0
 */

/**
 * @file IAudioCallback.idl
 *
 * @brief Audio播放的回调函数定义文件。
 *
 * 模块包路径：ohos.hdi.audio.v2_0
 *
 * 引用：ohos.hdi.audio.v2_0.AudioTypes
 *
 * @since 4.1
 * @version 2.0
 */


package ohos.hdi.audio.v2_0;

import ohos.hdi.audio.v2_0.AudioTypes;

/**
 * @brief Audio回调接口。
 *
 * @since 4.1
 * @version 2.0
 */
[callback] interface IAudioCallback {

    /**
     * @brief 放音回调函数。
     *
     * @param type 回调函数通知事件类型，详请参考{@link AudioCallbackType}。
     * @param reserved 保留字段。
     * @param cookie 用于传递数据。
     *
     * @return 成功返回值0，失败返回负值。
     *
     * @see RegCallback
     *
     * @since 4.1
     * @version 2.0
     */
    RenderCallback([in] enum AudioCallbackType type, [out] byte reserved, [out] byte cookie);
    /**
     * @brief 音频扩展参数回调函数。
     *
     * @param key 扩展参数键类型，详请参考{@link AudioExtParamKey}。
     * @param condition 扩展参数条件。
     * @param value 扩展参数条件的值
     * @param reserved 保留字段。
     * @param cookie 用于传递数据。
     *
     * @return 成功返回值0，失败返回负值。
     *
     * @see ParamCallback
     *
     * @since 4.1
     * @version 2.0
     */
    ParamCallback([in] enum AudioExtParamKey key, [in] String condition, [in] String value, [out] byte reserved, [in] byte cookie);
}
/** @} */
