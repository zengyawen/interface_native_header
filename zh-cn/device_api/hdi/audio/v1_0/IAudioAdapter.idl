/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup HdiAudio
 * @{
 *
 * @brief Audio模块接口定义。
 *
 * 音频接口涉及数据类型、驱动加载接口、驱动适配器接口、音频播放接口、音频录音接口等。
 *
 * @since 4.0
 * @version 1.0
 */

/**
 * @file IAudioAdapter.idl
 *
 * @brief Audio适配器的接口定义文件。
 *
 * 模块包路径：ohos.hdi.audio.v1_0
 *
 * 引用：
 * - ohos.hdi.audio.v1_0.AudioTypes
 * - ohos.hdi.audio.v1_0.IAudioRender
 * - ohos.hdi.audio.v1_0.IAudioCapture
 * - ohos.hdi.audio.v1_0.IAudioCallback
 *
 * @since 4.0
 * @version 1.0
 */


package ohos.hdi.audio.v1_0;

import ohos.hdi.audio.v1_0.AudioTypes;
import ohos.hdi.audio.v1_0.IAudioRender;
import ohos.hdi.audio.v1_0.IAudioCapture;
import ohos.hdi.audio.v1_0.IAudioCallback;

/**
 * @brief AudioAdapter音频适配器接口。
 *
 * 提供音频适配器（声卡）对外支持的驱动能力，包括初始化端口、创建放音、创建录音、获取端口能力集等。
 *
 * @see IAudioRender
 * @see IAudioCapture
 *
 * @since 4.0
 * @version 1.0
 */
interface IAudioAdapter {
    /**
     * @brief 初始化一个音频适配器所有的端口驱动。
     *
     * 在音频服务中，调用其他驱动接口前需要先调用该接口检查端口是否已经初始化完成，如果端口没有初始化完成，
     * 则需要等待一段时间（例如100ms）后重新进行检查，直到端口初始化完成后再继续操作。
     *
     * @param adapter 调用当前函数的AudioAdapter指针对象，该参数在编译为C接口后产生。
     *
     * @return 初始化完成返回值0，初始化失败返回负值。
     *
     * @since 4.0
     * @version 1.0
     */
    InitAllPorts();

    /**
     * @brief 创建一个音频播放接口的对象。
     *
     * @param adapter 调用当前函数的AudioAdapter指针对象，该参数在编译为C接口后产生。
     * @param desc 待打开的音频设备描述符，详请参考{@link AudioDeviceDescriptor}。
     * @param attrs 待打开的音频采样属性，详请参考{@link AudioSampleAttributes}。
     * @param render 获取的音频播放接口的对象实例保存到render中，详请参考{@link IAudioRender}。
     * @param renderId 获取的音频播放接口序号。
     *
     * @return 成功返回值0，失败返回负值。
      *
     * @see GetPortCapability
     * @see DestroyRender
     *
     * @since 4.0
     * @version 1.0
     */
    CreateRender([in] struct AudioDeviceDescriptor desc, [in] struct AudioSampleAttributes attrs,
                [out] IAudioRender render, [out] unsigned int renderId);

    /**
     * @brief 销毁一个音频播放接口的对象。
     *
     * @attention 在音频播放过程中，不能销毁该接口对象。
     *
     * @param adapter 调用当前函数的AudioAdapter指针对象，该参数在编译为C接口后产生。
     * @param renderId 待销毁的音频播放接口的序号。
     *
     * @return 成功返回值0，失败返回负值。
     * @see CreateRender
     *
     * @since 4.0
     * @version 1.0
     */
    DestroyRender([in] unsigned int renderId);

    /**
     * @brief 创建一个音频录音接口的对象。
     *
     * @param adapter 调用当前函数的AudioAdapter指针对象，该参数在编译为C接口后产生。
     * @param desc 待打开的音频设备描述符，详请参考{@link AudioDeviceDescriptor}。
     * @param attrs 待打开的音频采样属性，详请参考{@link AudioSampleAttributes}。
     * @param capture 获取的音频录音接口的对象实例保存到capture中，详请参考{@link IAudioCapture}。
     * @param captureId 获取的音频录音接口的序号。
     *
     * @return 成功返回值0，失败返回负值。
     * 
     * @see GetPortCapability
     * @see DestroyCapture
     
     * @since 4.0
     * @version 1.0
     */
    CreateCapture([in] struct AudioDeviceDescriptor desc, [in] struct AudioSampleAttributes attrs,
                  [out] IAudioCapture capture, [out] unsigned int captureId);

    /**
     * @brief 销毁一个音频录音接口的对象。
     *
     * @attention 在音频录音过程中，不能销毁该接口对象。
     *
     * @param adapter 调用当前函数的AudioAdapter指针对象，该参数在编译为C接口后产生。
     * @param captureId 待销毁的音频录音接口的序号。
     *
     * @return 成功返回值0，失败返回负值。
     * @see CreateCapture
     *
     * @since 4.0
     * @version 1.0
     */
    DestroyCapture([in] unsigned int captureId);

    /**
     * @brief 获取一个音频适配器的端口驱动的能力集。
     *
     * @param adapter 调用当前函数的AudioAdapter指针对象，该参数在编译为C接口后产生。
     * @param port 待获取的端口，详请参考{@link AudioPort}。
     * @param capability 获取的端口能力保存到capability中，详请参考{@link AudioPortCapability}。
     *
     * @return 成功返回值0，失败返回负值。
     *
     * @since 4.0
     * @version 1.0
     */
    GetPortCapability([in] struct AudioPort port, [out] struct AudioPortCapability capability);

    /**
     * @brief 设置音频端口驱动的数据透传模式。
     *
     * @param adapter 调用当前函数的AudioAdapter指针对象，该参数在编译为C接口后产生。
     * @param port 待设置的端口，详请参考{@link AudioPort}。
     * @param mode 待设置的传输模式，详请参考{@link AudioPortPassthroughMode}。
     *
     * @return 成功返回值0，失败返回负值。
     * @see GetPassthroughMode
     *
     * @since 4.0
     * @version 1.0
     */
    SetPassthroughMode([in] struct AudioPort port, [in] enum AudioPortPassthroughMode mode);

    /**
     * @brief 获取音频端口驱动的数据透传模式。
     *
     * @param adapter 调用当前函数的AudioAdapter指针对象，该参数在编译为C接口后产生。
     * @param port 待获取的端口，详请参考{@link AudioPort}。
     * @param mode 获取的传输模式保存到mode中，详请参考{@link AudioPortPassthroughMode}。
     *
     * @return 成功返回值0，失败返回负值。
     * @see SetPassthroughMode
     *
     * @since 4.0
     * @version 1.0
     */
    GetPassthroughMode([in] struct AudioPort port, [out] enum AudioPortPassthroughMode mode);

    /**
     * @brief 获取一个音频适配器的设备状态。
     *
     * @param adapter 调用当前函数的AudioAdapter指针对象，该参数在编译为C接口后产生。
     * @param status 获取的设备状态保存到status中，详请参考{@link AudioDeviceStatus}。
     *
     * @return 成功返回值0，失败返回负值。
     *
     * @since 4.0
     * @version 1.0
     */
    GetDeviceStatus([out] struct AudioDeviceStatus status);

    /**
     * @brief 更新音频路由。
     *
     * @param adapter 调用当前函数的AudioAdapter指针对象，该参数在编译为C接口后产生。
     * @param route 待更新的路由，详请参考{@link AudioRoute}。
     * @param routeHandle 更新后的音频路由句柄保存到routeHandle中。
     *
     * @return 成功返回值0，失败返回负值。
     *
     * @since 4.0
     * @version 1.0
     */
    UpdateAudioRoute([in] struct AudioRoute route, [out] int routeHandle);

    /**
     * @brief 释放音频路由。
     *
     * @param adapter 调用当前函数的AudioAdapter指针对象，该参数在编译为C接口后产生。
     * @param routeHandle 待释放的音频路由句柄。
     *
     * @return 成功返回值0，失败返回负值。
     *
     * @since 4.0
     * @version 1.0
     */
    ReleaseAudioRoute([in] int routeHandle);

    /**
     * @brief 设置音频静音。
     *
     * @param adapter 调用当前函数的AudioAdapter指针对象，该参数在编译为C接口后产生。
     * @param mute 表示是否将音频静音，true表示静音，false表示非静音。
     *
     * @return 成功返回值0，失败返回负值。
     *
     * @see SetMicMute
     *
     * @since 4.0
     * @version 1.0
     */
    SetMicMute([in] boolean mute);

    /**
     * @brief 获取音频静音状态。
     *
     * @param adapter 调用当前函数的AudioAdapter指针对象，该参数在编译为C接口后产生。
     * @param mute 获取的静音状态保存到mute中，true表示静音，false表示非静音。
     *
     * @return 成功返回值0，失败返回负值。
     *
     * @see GetMicMute
     *
     * @since 4.0
     * @version 1.0
     */
    GetMicMute([out] boolean mute);

    /**
     * @brief 设置语音呼叫的音量。
     *
     * 音量范围从0.0到1.0。如果音频服务中的音量水平在0到15的范围内，
     * 0.0表示音频静音，1.0指示最大音量级别（15）。
     *
     * @param adapter 调用当前函数的AudioAdapter指针对象，该参数在编译为C接口后产生。
     * @param volume 待设置的音量值，范围为（0.0-1.0），0.0表示最小音量值，1.0表示最大音量值。
     *
     * @return 成功返回值0，失败返回负值。
     * @see GetVolume
     *
     * @since 4.0
     * @version 1.0
     */
    SetVoiceVolume([in] float volume);

    /**
     * @brief 根据指定的条件设置音频拓展参数。
     *
     * @param adapter 调用当前函数的AudioAdapter指针对象，该参数在编译为C接口后产生。
     * @param key 指定的扩展参数键类型，详请参考{@link AudioExtParamKey}。
     * @param condition 指定的扩展参数查询条件。
     *
     * condition为多个键值对组成的字符串，多个键值对之间通过分号分割，键值对的格式为"keytype=keyvalue"。
     *
     * 当输入的key值为AudioExtParamKey::AUDIO_EXT_PARAM_KEY_VOLUME时，condition的格式必须为：
     * <i>"EVENT_TYPE=xxx;VOLUME_GROUP_ID=xxx;AUDIO_VOLUME_TYPE=xxx;"</i>
     * - EVENT_TYPE 表示音量事件类型: 其中1表示设置音量, 4表示设置静音。
     * - VOLUME_GROUP_ID 表示待设置的音频扩展参数相关的音量组。
     * - AUDIO_VOLUME_TYPE 表示待设置的音频扩展参数相关的音量类型。
     *
     * @param value 指定的扩展参数条件值。
     *
     * @return 成功返回值0，失败返回负值。
     *
     * @since 4.0
     * @version 1.0
     */
    SetExtraParams([in] enum AudioExtParamKey key, [in] String condition, [in] String value);

    /**
     * @brief 根据指定条件获取音频扩展参数的取值。
     *
     * @param adapter 调用当前函数的AudioAdapter指针对象，该参数在编译为C接口后产生。
     * @param key 指定的扩展参数键类型，详请参考{@link AudioExtParamKey}。
     * @param condition 指定的扩展参数查询条件。
     *
     * condition为多个键值对组成的字符串，多个键值对之间通过分号分割，键值对的格式为"keytype=keyvalue"。
     *
     * 当输入的key值为AudioExtParamKey::AUDIO_EXT_PARAM_KEY_VOLUME时，condition的格式必须为：
     * <i>"EVENT_TYPE=xxx;VOLUME_GROUP_ID=xxx;AUDIO_VOLUME_TYPE=xxx;"</i>
     * - EVENT_TYPE 表示音量事件类型: 其中1表示设置音量, 4表示设置静音。
     * - VOLUME_GROUP_ID 表示待查询的音频扩展参数相关的音量组。
     * - AUDIO_VOLUME_TYPE 表示待查询的音频扩展参数相关的音量类型。
     *
     * @param value 待返回的指定扩展参数条件的当前值。
     * @param lenth value的长度，该参数在编译为C接口后产生。
     *
     * @return 成功返回值0，失败返回负值。
     *
     * @since 4.0
     * @version 1.0
     */
    GetExtraParams([in] enum AudioExtParamKey key, [in] String condition, [out] String value);

    /**
     * @brief 注册扩展参数回调函数。
     *
     * @param adapter 调用当前函数的AudioAdapter指针对象，该参数在编译为C接口后产生。
     * @param callback 待注册的回调函数，详请参考{@link AudioCallback}。
     * @param cookie 用于传递数据。
     *
     * @return 成功返回值0，失败返回负值。
     *
     * @since 3.2
     * @version 1.0
     */
    RegExtraParamObserver([in] IAudioCallback audioCallback, [in] byte cookie);
}
/** @} */