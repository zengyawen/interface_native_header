/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup HdfPinAuth
 * @{
 *
 * @brief 提供口令认证驱动的标准API接口。
 *
 * 口令认证驱动为口令认证服务提供统一的访问接口。获取口令认证驱动代理后，口令认证服务可以调用相关接口获取执行器，获取口令认证执行器后，
 * 口令认证服务可以调用相关接口获取执行器信息，获取凭据模版信息，注册口令，认证口令，删除口令等。
 *
 * @since 3.2
 */

/**
 * @file IExecutorCallback.idl
 *
 * @brief 定义异步API接口回调，用于返回异步接口的请求处理结果和获取信息。
 *
 * 模块包路径：ohos.hdi.pin_auth.v1_0
 *
 * @since 3.2
 */

package ohos.hdi.pin_auth.v1_0;

/**
 * @brief 定义异步API接口回调，用于返回异步接口的请求处理结果和获取信息。使用细节见{@link IExecutor}。
 *
 * @since 3.2
 * @version 1.0
 */
[callback] interface IExecutorCallback {
    /**
     * @brief 定义操作请求处理结果回调函数。
     *
     * @param result 操作请求处理结果。
     * @param extraInfo 其他相关信息，如用户认证通过时用于返回执行器签发的认证令牌等。
     *
     * @return 0 表示操作成功。
     * @return 非0 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    OnResult([in] int result, [in] unsigned char[] extraInfo);
    /**
     * @brief 定义请求获取口令数据回调函数。 
     *
     * @param salt 盐值，用于对口令 明文进行单向处理。
     * @param authSubType 口令子类型，如六位数字PIN码等。
     *
     * @return 0 表示操作成功。
     * @return 非0 表示操作失败。
     *
     * @since 3.2
     * @version 1.0
     */
    OnGetData([in] unsigned long scheduleId, [in] unsigned char[] salt, [in] unsigned long authSubType);
}
/** @} */