/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup HdiDrm
 * @{
 * @brief DRM模块接口定义。
 *
 * DRM是数字版权管理，用于对多媒体内容加密，以便保护价值内容不被非法获取，
 * DRM模块接口定义了DRM实例管理、DRM会话管理、DRM内容解密的接口。
 *
 * @since 4.1
 * @version 1.0
 */

/**
 * @file IMediaKeySessionCallback.idl
 *
 * @brief 定义了DRM会话的事件通知接口。
 *
 * 模块包路径：ohos.hdi.drm.v1_0
 *
 * 引用：ohos.hdi.drm.v1_0.MediaKeySystemTypes
 *
 * @since 4.1
 * @version 1.0
 */

package ohos.hdi.drm.v1_0;

import ohos.hdi.drm.v1_0.MediaKeySystemTypes;

/**
* @brief 定义DRM会话的事件通知函数，用于DRM驱动通知DRM框架事件。
*
* @since 4.1
* @version 1.0
*/
[callback] interface IMediaKeySessionCallback {
    /**
     * @brief 发送事件通知。
     *
     * @param eventType 事件类型。
     * @param extra 事件附加信息。
     * @param data 事件详细信息。
     *
     * @return 0 表示执行成功。
     * @return 其他值表示执行失败。
     *
     * @since 4.1
     * @version 1.0
     */
    SendEvent([in] enum EventType eventType, [in] int extra, [in] unsigned char[] data);
    /**
     * @brief 发送事件通知。
     *
     * @param keyStatus 许可证中密钥索引及其状态。
     * @param newKeysAvailable 是否有新的许可证密钥可用，true表示有新的许可证密钥，
     * false表示无新的许可证密钥。
     *
     * @return 0 表示执行成功。
     * @return 其他值表示执行失败。
     *
     * @since 4.1
     * @version 1.0
     */
    SendEventKeyChange([in] Map<unsigned char[], enum MediaKeySessionKeyStatus> keyStatus, [in] boolean newKeysAvailable);
}
/** @} */