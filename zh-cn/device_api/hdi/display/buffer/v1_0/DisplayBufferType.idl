/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Display
 * @{
 *
 * @brief 显示模块驱动接口定义。
 *
 * 提供给上层图形服务使用的驱动接口，包括图层管理、设备控制、显示内存管理等相关接口。
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @file DisplayBufferType.idl
 *
 * @brief 显示内存类型定义，定义显示内存操作相关接口所使用的数据类型。
 *
 * 模块包路径：ohos.hdi.display.buffer.v1_0
 *
 * @since 3.2
 * @version 1.0
 */
package ohos.hdi.display.buffer.v1_0;

/**
 * @brief 定义待分配内存的信息。
 *
 * @since 3.2
 * @version 1.0
 */
struct AllocInfo {
    unsigned int width;               /**< 申请内存宽度 */
    unsigned int height;              /**< 申请内存高度 */
    unsigned long usage;              /**< 申请内存的使用场景 */
    unsigned int format;              /**< 申请内存格式 */
    unsigned int expectedSize;        /**< 申请内存大小 */
};

/**
 * @brief 用于验证内存分配信息的结构体定义。
 *
 * @since 3.2
 * @version 1.0
 */
struct VerifyAllocInfo {
    unsigned int width;               /**< 分配内存的宽度 */
    unsigned int height;              /**< 分配内存的高度 */
    unsigned long usage;              /**< 内存的用处 */
    unsigned int format;              /**< 分配内存的像素格式 */
};
/** @} */