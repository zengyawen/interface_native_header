/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup HdfFingerprintAuth
 * @{
 *
 * @brief 提供指纹认证驱动的API接口。
 *
 * 指纹认证驱动程序为指纹认证服务提供统一的接口，用于访问指纹认证驱动程序。获取指纹认证驱动代理后，服务可以调用相关API获取执行器。
 * 获取指纹认证执行器后，服务可以调用相关API获取执行器信息，获取凭据模板信息、注册指纹特征模板、进行用户指纹认证、删除指纹特征模板等。
 *
 * @since 3.2
 */

/**
 * @file IExecutorCallback.idl
 *
 * @brief 定义异步API接口回调，用于返回异步接口的请求处理结果和信息。
 *
 * 模块包路径：ohos.hdi.fingerprint_auth.v1_0
 *
 * @since 3.2
 */

package ohos.hdi.fingerprint_auth.v1_0;

/**
 * @brief 定义异步API接口回调。当执行器使用者调用异步函数时该回调需要被注册。使用细节见{@link IExecutor}。
 *
 * @since 3.2
 * @version 1.0
 */
[callback] interface IExecutorCallback {
    /**
     * @brief 定义操作结果回调函数。 
     *
     * @param result 操作请求处理结果。
     * @param extraInfo 其他相关信息，如用户认证通过时用于返回执行器签发的认证令牌等。
     *
     * @return 0 表示操作成功。
     * @return 非0 表示操作失败。
     */
    OnResult([in] int result, [in] unsigned char[] extraInfo);
    /**
     * @brief 定义操作过程信息反馈回调函数。 
     *
     * @param tip 提示信息编码{@link FingerprintTipsCode}。
     * @param extraInfo 其他相关信息，用于支持信息扩展。
     *
     * @return 0 表示操作成功。
     * @return 非0 表示操作失败。
     */
    OnTip([in] int tip, [in] unsigned char[] extraInfo);
}
/** @} */