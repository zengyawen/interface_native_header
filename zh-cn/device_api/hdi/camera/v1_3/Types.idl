/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

  /**
 * @addtogroup Camera
 * @{
 *
 * @brief Camera模块接口定义。
 *
 * Camera模块涉及相机设备的操作、流的操作、离线流的操作和各种回调等。
 *
 * @since 5.0
 * @version 1.3
 */

/**
 * @file Types.idl
 *
 * @brief Camera模块HDI接口使用的数据类型。
 *
 * 模块包路径：ohos.hdi.camera.v1_3
 *
 * 引用
 * - ohos.hdi.camera.v1_0.Types
 * - ohos.hdi.camera.v1_2.Types
 *
 * @since 3.2
 * @version 1.3
 */

package ohos.hdi.camera.v1_3;

import ohos.hdi.camera.v1_2.Types;
import ohos.hdi.camera.v1_0.Types;

sequenceable ohos.hdi.camera.v1_0.BufferHandleSequenceable;
sequenceable ohos.hdi.camera.v1_0.MapDataSequenceable;

/**
 * @brief 流使用模式。
 * @since 3.2
 * @version 1.3
 */
enum OperationMode : ohos.hdi.camera.v1_2.OperationMode_V1_2 {

    /**
     * 专业模式，专用于专业拍照场景。
     *
     * @since 4.1
     * @version 1.1
     */
    PROFESSIONAL_V1_3 = PROFESSIONAL,

    /**
     * 专业拍照模式，专用于专业拍照场景。
     *
     * @since 5.0
     * @version 1.0
     */
    PROFESSIONAL_PHOTO = 11,

    /**
     * 专业录像模式，专用于专业录像场景。
     *
     * @since 5.0
     * @version 1.0
     */
    PROFESSIONAL_VIDEO = 12,

    /**
     * 慢动作模式，专门用于视频录制慢动作。
     *
     * @since 5.0
     * @version 1.0
     */
    HIGH_FRAME_RATE = 13,

    /**
     * 高分辨率照片模式，专门用于捕捉记录高像素。
     *
     * @since 5.0
     * @version 1.0
     */
    HIGH_RESOLUTION_PHOTO = 14,

    /**
    * 安全模式，专用于安全模式。
    * @since 5.0
    * @version 1.0
    */
    SECURE = 15,

    /**
    * 闪拍模式，专为快速拍照模式设计。
    * @since 5.0
    * @version 1.0
    */
    QUICK_SHOT_PHOTO = 16,

    /**
    * 流光快门模式，专用于光绘模式。
    * @since 5.0
    * @version 1.0
    */
    LIGHT_PAINTING = 17,

    /**
    * 全景模式，专用于全景模式。
    * @since 5.0
    * @version 1.0
    */
    PANORAMA_PHOTO = 18,

    /**
    * 延时摄影模式，专用于延时摄影模式。
    * @since 5.0
    * @version 1.0
    */
    TIMELAPSE_PHOTO = 19,

    /**
    * 大光圈模式，专用于大光圈模式。
    * @since 5.0
    * @version 1.0
    */
    APERTURE_VIDEO = 20,

    /**
    * 荧光拍照模式，专用于荧光拍照模式。
    * @since 5.0
    * @version 1.0
    */
    FLUORESCENCE_PHOTO = 21,

    /**
    * 防晒检测模式，专用于防晒检测模式。
    * @since 5.0
    * @version 1.0
    */
    SUN_BLOCK = 22,
};

/**
 * @brief 扩展流信息的类型。
 * @since 4.0
 * @version 1.2
 */
enum ExtendedStreamInfoType : ohos.hdi.camera.v1_2.ExtendedStreamInfoType_V1_2 {

    /**
     * raw图的扩展流信息。
     * @since 5.0
     * @version 1.0
     */
    EXTENDED_STREAM_INFO_RAW = 2,

    /**
     * 深度流的扩展流信息。
     * @since 5.0
     * @version 1.0
     */
    EXTENDED_STREAM_INFO_DEPTH = 3,

    /**
     * meta流的扩展流信息。
     * @since 5.0
     * @version 1.0
     */
    EXTENDED_STREAM_INFO_META = 4,
    
    /**
     * 安全流的扩展流信息。
     * @since 5.0
     * @version 1.0
     */
    EXTENDED_STREAM_INFO_SECURE = 5,

    /**
     * 维测流的扩展流信息。
     * @since 5.0
     * @version 1.0
     */
    EXTENDED_STREAM_INFO_MAKER_INFO = 6,

    /**
     * exif流的扩展流信息。
     * @since 5.0
     * @version 1.0
     */
    EXTENDED_STREAM_INFO_EXIF = 7,

    /**
     * 高显图的扩展流信息。
     * @since 5.0
     * @version 1.0
     */
    EXTENDED_STREAM_INFO_GAINMAP = 8,

    /**
     * unrefocus流的扩展流信息。
     * @since 5.0
     * @version 1.0
     */
    EXTENDED_STREAM_INFO_UNREFOCUS = 9,

    /**
     * 线型图的的扩展流信息。
     * @since 5.0
     * @version 1.0
     */
    EXTENDED_STREAM_INFO_LINEAR = 10,

    /**
     * 水平裁切图的扩展流信息。
     * @since 5.0
     * @version 1.0
     */
    EXTENDED_STREAM_INFO_FRAGMENT =11,

    /**
     * uv附图的扩展流信息。
     * @since 5.0
     * @version 1.0
     */
    EXTENDED_STREAM_INFO_UV = 12,
};

/**
 * @brief 流类型。
 * @since 5.0
 * @version 1.0
 */
enum StreamType {

    /**
     * 流数据用于显示，即预览流。
     */
    STREAM_TYPE_PREVIEW = 0,

    /**
     * 数据用于编码生成录像，即录像流。
     */
    STREAM_TYPE_VIDEO = 1,

    /**
     * 流数据用于编码生成照片，即拍照流。
     */
    STREAM_TYPE_STILL_CAPTURE = 2,

    /**
     * 流数据用于保存缩略图。
     */
    STREAM_TYPE_POST_VIEW = 3,

    /**
     * 流数据用于图像分析。
     */
    STREAM_TYPE_ANALYZE = 4,

    /**
     * 自定义类型。
     */
    STREAM_TYPE_CUSTOM = 5,

    /**
     * 深度流类型。
     */
    STREAM_TYPE_DEPTH = 6,
};

/**
 * @brief 执行模式的类型。
 * @since 4.1
 * @version 1.1
 */
enum ExecutionMode : ohos.hdi.camera.v1_2.ExecutionMode {
    /**
     * 默认模式。
     */
    DEFAULT = 3,
};

/**
 * @brief 设备错误类型，用于设备错误回调{@link OnError}。
 * @since 3.2
 * @version 1.1
 */
enum ErrorType : ohos.hdi.camera.v1_0.ErrorType {
    /**
     * 传感器数据错误。
     * @since 5.0
     * @version 1.0
     */
    SENSOR_DATA_ERROR = 5,
};

/**
 * @brief 相机设备资源开销，用于{@link ICameraDevice::GetResourceCost}。
 */
struct CameraDeviceResourceCost {
    /**
     * @brief 当前Camera设备使用总的资源，范围为[0, 100]。
     */
    unsigned int resourceCost_;

    /**
     * @brief 当前相机设备打开时无法打开的相机设备ID列表。
     */
    String[] conflictingDevices_;
};

/**
 * @brief 相机媒体流类型。
 * @since 5.0
 * @version 1.0
 */
enum MediaStreamType {
    /**
     * 视频媒体流。
     * @since 5.0
     * @version 1.0
     */
    MEDIA_STREAM_TYPE_VIDEO = 0,
    /**
     * metadata媒体流。
     * @since 5.0
     * @version 1.0
     */
    MEDIA_STREAM_TYPE_METADATA = 1,
    /**
     * 维测媒体流。
     * @since 5.0
     * @version 1.0
     */
    MEDIA_STREAM_TYPE_MAKER = 2,
};

/**
 * @brief 流描述信息，使用于{@link IVideoProcessSession::Prepare}。
 * @since 5.0
 * @version 1.0
 */
struct StreamDescription {
    /**
     * 流id。
     * @since 5.0
     * @version 1.0
     */
    int streamId;
    /**
     * 媒体流类型。
     * @since 5.0
     * @version 1.0
     */
    enum MediaStreamType type;
    /**
     * pixel格式。
     * @since 5.0
     * @version 1.0
     */
    int pixelFormat;
    /**
     * 图片宽。
     * @since 5.0
     * @version 1.0
     */
    int width;
    /**
     * 图片高。
     * @since 5.0
     * @version 1.0
     */
    int height;
    /**
     * 图片色域。
     * @since 5.0
     * @version 1.0
     */
    int dataspace;
};

/**
 * @brief 拍照结束信息，使用于{@link IStreamOperatorCallback::OnCaptureEndedExt}。
 * @since 5.0
 * @version 1.0
 */
struct CaptureEndedInfoExt {
    /**
     * 流id。
     * @since 5.0
     * @version 1.0
     */
    int streamId_;
    /**
     * 帧数。
     * @since 5.0
     * @version 1.0
     */
    int frameCount_;
    /**
     * 是否使能二阶段视频增强处理。
     * @since 5.0
     * @version 1.0
     */
    boolean isDeferredVideoEnhancementAvailable_;
    /**
     * 视频id。
     * @since 5.0
     * @version 1.0
     */
    String videoId_;
};

/**
 * @brief 图片流信息，使用于{@link IImageProcessCallback::OnProcessDoneExt}。
 * @since 5.0
 * @version 1.0
 */
struct ImageBufferInfoExt {
    /**
     * @brief metadata信息。
     * @since 5.0
     * @version 1.0
     */
    MapDataSequenceable metadata;

    /**
     * @brief 图片处理信息。
     * @since 5.0
     * @version 1.0
     */
    BufferHandleSequenceable imageHandle;

    /**
     * @brief gainMap是否上报。
     * @since 5.0
     * @version 1.0
     */
    boolean isGainMapValid;

    /**
     * @brief gainMap信息。
     * @since 5.0
     * @version 1.0
     */
    BufferHandleSequenceable gainMapHandle;

    /**
     * @brief depthmap是否上报。
     * @since 5.0
     * @version 1.0
     */
    boolean isDepthMapValid;

    /**
     * @brief depthmap处理信息。
     * @since 5.0
     * @version 1.0
     */
    BufferHandleSequenceable depthMapHandle;

    /**
     * @brief 人像模式是否上报unrefocusImage。
     * @since 5.0
     * @version 1.0
     */
    boolean isUnrefocusImageValid;

    /**
     * @brief unrefocusImage处理信息。
     * @since 5.0
     * @version 1.0
     */
    BufferHandleSequenceable unrefocusImageHandle;

    /**
     * @brief 是否上报高位深度线性图像。
     * @since 5.0
     * @version 1.0
     */
    boolean isHighBitDepthLinearImageValid;

    /**
     * @brief 高位深度线性图像处理信息。
     * @since 5.0
     * @version 1.0
     */
    BufferHandleSequenceable highBitDepthLinearImageHandle;

    /**
     * @brief 是否上报exif信息。
     * @since 5.0
     * @version 1.0
     */
    boolean isExifValid;

    /**
     * @brief exif处理信息。
     * @since 5.0
     * @version 1.0
     */
    BufferHandleSequenceable exifHandle;

    /**
     * @brief 维测信息是否上报。
     * @since 5.0
     * @version 1.0
     */
    boolean isMakerInfoValid;

    /**
     * @brief 维测处理信息。
     * @since 5.0
     * @version 1.0
     */
    BufferHandleSequenceable makerInfoHandle;
};

/**
 * @brief 流数据的编码类型。
 * @since 5.0
 * @version 1.0
 */
enum EncodeType : ohos.hdi.camera.v1_0.EncodeType {
     /**
      * HEIC编码格式。
      */
     ENCODE_TYPE_HEIC = 4,
};

/**
 * @brief 流错误类型，用于流错误类型{@link CaptureErrorInfo}。
 * @since 3.2
 * @version 1.1
 */
enum StreamError : ohos.hdi.camera.v1_0.StreamError {
    /**
     * 当传感器温度高于阈值，cameraHal将会停止上报画中画流并且通过OnCaptureError回调上报高温错误码。
     *
     * @since 5.0
     * @version 1.0
     */
    HIGH_TEMPERATURE_ERROR = 2,
};
/** @} */