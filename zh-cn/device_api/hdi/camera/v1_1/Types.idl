/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Camera
 * @{
 *
 * @brief Camera模块接口定义。
 *
 * Camera模块涉及相机设备的操作、流的操作、离线流的操作和各种回调等。
 *
 * @since 4.0
 * @version 1.1
 */

/**
 * @file Types.idl
 *
 * @brief Camera模块HDI接口使用的数据类型。
 *
 * 模块包路径：ohos.hdi.camera.v1_1
 *
 * 引用：ohos.hdi.camera.v1_0.Types
 *
 * @since 4.0
 * @version 1.1
 */


package ohos.hdi.camera.v1_1;
sequenceable ohos.hdi.camera.v1_0.BufferProducerSequenceable;

import ohos.hdi.camera.v1_0.Types;

/**
 * @brief 扩展流信息。
 *
 * @since 4.0
 * @version 1.1
 */
enum ExtendedStreamInfoType {
    /**
     * 快速缩略图的扩展流信息.
     */
    EXTENDED_STREAM_INFO_QUICK_THUMBNAIL = 0,
};

/**
 * @brief 扩展流信息。
 *
 * @since 4.0
 * @version 1.1
 */ 
struct ExtendedStreamInfo {
    /**
     * 扩展流信息类型。
     */
    enum ExtendedStreamInfoType type;

    /**
     * 图像宽度。
     */
    int width;

    /**
     * 图像高度。
     */
    int height;

    /**
     * 图像格式。
     */
    int format;

    /**
     * 图像颜色空间。
     */
    int dataspace;

    /**
     * 图形提供的生产者句柄，用于快速缩略图。
     */
    BufferProducerSequenceable bufferQueue;
};

/**
 * @brief 流信息，用于创建流时传入相关的配置参数。
 *
 * @since 4.0
 * @version 1.1
 */
struct StreamInfo_V1_1 {
    /**
     * 流信息的上一版本。
     */
    struct StreamInfo v1_0;

    /**
     * 可选扩展流信息。
     */
    struct ExtendedStreamInfo[] extendedStreamInfos;
};

/**
 * @brief 预启动配置信息，用于{@link Prelaunch}。
 *
 * @since 4.0
 * @version 1.1
 */
struct PrelaunchConfig {
    /**
     * Camera ID，相机设备唯一标识。
     */
    String cameraId;

    /**
     * 流预启动模式中使用的流信息，目前可以忽略。
     */
    struct StreamInfo_V1_1[] streamInfos_V1_1;

    /**
     * 表示预启动配置信息。
     */
    unsigned char[] setting;
};

/**
 * @brief 流的使用模式。
 *
 * @since 4.0
 * @version 1.1
 */
enum OperationMode_V1_1 {
    /**
     * 普通模式, 支持拍照和录像场景。
     */
    NORMAL = 0,

    /**
     * 拍照模式, 专用于拍照场景。
     * 如果执行此模式, 不应再执行普通模式。
     */
    CAPTURE = 1,

    /**
     * 录像模式, 专用于录像场景。
     * 如果执行此模式, 不应再执行普通模式。
     */
    VIDEO = 2,

    /**
     * 人像模式, 专用于人像场景。
     */
    PORTRAIT = 3,

    /**
     * 夜景模式, 专用于夜间拍照场景。
     */
    NIGHT = 4,

    /**
     * 专业模式, 专用于专业拍照场景。
     */
    PROFESSIONAL = 5,

    /**
     * 慢动作模式, 专用于捕捉慢动作。
     */
    SLOW_MOTION = 6,
};
/** @} */