/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 /**
 * @addtogroup IntelligentVoiceEngine
 * @{
 *
 * @brief IntelligentVoiceEngine模块向上层服务提供了统一接口。
 *
 * 上层服务开发人员可根据IntelligentVoiceEngine模块提供的向上统一接口获取如下能力：创建销毁唤醒算法引擎、启动停止唤醒算法引擎、写语音数据、读文件、回调函数注册等。
 *
 * @since 4.0
 * @version 1.0
 */

/**
 * @file IIntellVoiceEngineAdapter.idl
 *
 * @brief IntelligentVoiceEngine模块智能语音引擎适配器接口，包括设置回调、加载唤醒算法引擎、卸载唤醒算法引擎、设置唤醒算法参数、获取唤醒算法参数、启动唤醒算法引擎、停止唤醒算法引擎、读写数据等。
 *
 * 模块包路径：ohos.hdi.intelligent_voice.engine.v1_0
 *
 * 引用：
 * - ohos.hdi.intelligent_voice.engine.v1_0.IntellVoiceEngineTypes
 * - ohos.hdi.intelligent_voice.engine.v1_0.IIntellVoiceEngineCallback
 *
 * @since 4.0
 * @version 1.0
 */

package ohos.hdi.intelligent_voice.engine.v1_0;

import ohos.hdi.intelligent_voice.engine.v1_0.IntellVoiceEngineTypes;
import ohos.hdi.intelligent_voice.engine.v1_0.IIntellVoiceEngineCallback;

 /**
 * @brief IntelligentVoiceEngine模块向上层服务提供了智能语音引擎适配器接口。
 *
 * 上层服务开发人员可根据IntelligentVoiceEngine模块提供的向上智能语音引擎适配器接口实现设置回调、加载唤醒算法引擎、卸载唤醒算法引擎、设置唤醒算法参数、获取唤醒算法参数、启动唤醒算法引擎、停止唤醒算法引擎、读写数据等功能。
 *
 * @since 4.0
 * @version 1.0
 */
interface IIntellVoiceEngineAdapter {
    /**
     * @brief 上层服务设置回调接口。
     *
     * @param engineCallback 回调接口，具体参考{@link IIntellVoiceEngineCallback}。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 4.0
     * @version 1.0
     */
    SetCallback([in] IIntellVoiceEngineCallback engineCallback);
    /**
     * @brief 加载唤醒算法引擎。
     *
     * @param info 智能语音唤醒算法引擎适配器信息，具体参考{@link IntellVoiceEngineAdapterInfo}。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 4.0
     * @version 1.0
     */
    Attach([in] struct IntellVoiceEngineAdapterInfo info);
    /**
     * @brief 卸载唤醒算法引擎。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 4.0
     * @version 1.0
     */
    Detach();
    /**
     * @brief 设置唤醒算法参数。
     *
     * @param keyValueList 键值对列表，键值对的格式为"key=value"，多个键值对之间通过分号分割，key和value的具体值由开发者自定义。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 4.0
     * @version 1.0
     */
    SetParameter([in] String keyValueList);
    /**
     * @brief 获取唤醒算法参数。
     *
     * @param keyList 键列表，多个键之间通过分号分割，key和value的具体值由开发者自定义。
     * @param valueList 返回值列表，多个返回值之间通过分号分割。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 4.0
     * @version 1.0
     */
    GetParameter([in] String keyList, [out] String valueList);
    /**
     * @brief 启动唤醒算法引擎。
     *
     * @param info 启动信息，具体参考{@link StartInfo}。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 4.0
     * @version 1.0
     */
    Start([in] struct StartInfo info);
    /**
     * @brief 停止唤醒算法引擎。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 4.0
     * @version 1.0
     */
    Stop();
    /**
     * @brief 写语音数据。
     *
     * @param buffer 语音数据，语音数据大小由开发者指定，默认是20ms语音数据。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 4.0
     * @version 1.0
     */
    WriteAudio([in] List <unsigned char> buffer);
    /**
     * @brief 读数据。
     *
     * @param type 数据类型，具体参考{@link ContentType}。
     * @param buffer 数据内容。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 4.0
     * @version 1.0
     */
    Read([in] enum ContentType type, [out] Ashmem buffer);
}
/** @} */