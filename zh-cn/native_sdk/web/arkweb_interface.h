/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Web
 * @{
 *
 * @brief 提供ArkWeb在Native侧的能力，如网页刷新、执行JavaScript、注册回调等。
 * @since 12
 */
/**
 * @file arkweb_interface.h
 *
 * @brief 提供ArkWeb在Native侧获取API的接口，及基础Native API类型。
 * @library libohweb.so
 * @syscap SystemCapability.Web.Webview.Core
 * @since 12
 */

#ifndef ARKWEB_INTERFACE_H
#define ARKWEB_INTERFACE_H

#include "arkweb_type.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 定义基础Native API类型。
 *
 * @since 12
 */
typedef struct {
    /** 结构体对应的大小。 */
    size_t size;
} ArkWeb_AnyNativeAPI;

/**
 * @brief 定义Native API的类型枚举。
 *
 * @since 12
 */
typedef enum {
    /** component相关API类型。 */
    ARKWEB_NATIVE_COMPONENT,
    /** controller相关API类型。 */
    ARKWEB_NATIVE_CONTROLLER,
    /** webMessagePort相关API类型。 */
    ARKWEB_NATIVE_WEB_MESSAGE_PORT,
    /** webMessage相关API类型。 */
    ARKWEB_NATIVE_WEB_MESSAGE,
    /** cookieManager相关API类型。 */
    ARKWEB_NATIVE_COOKIE_MANAGER,
    /**
     * @brief JavaScriptValue相关接口类型。
     *
     * @since 16
     */
    ARKWEB_NATIVE_JAVASCRIPT_VALUE,
} ArkWeb_NativeAPIVariantKind;

/**
 * @brief 根据传入的API类型，获取对应的Native API结构体。
 * @param type ArkWeb支持的Native API类型。
 * @return 根据传入的API类型，返回对应的Native API结构体指针，结构体第一个成员为当前结构体的大小。
 *
 * @syscap SystemCapability.Web.Webview.Core
 * @since 12
 */
ArkWeb_AnyNativeAPI* OH_ArkWeb_GetNativeAPI(ArkWeb_NativeAPIVariantKind type);

#ifdef __cplusplus
};
#endif
#endif // ARKWEB_INTERFACE_H
/** @} */