/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Preferences
 * @{
 *
 * @brief 首选项模块（Preferences）提供Key-Value键值型数据（后续简称KV数据）的处理接口，实现对轻量级KV数据的查询、修改和持久化功能。
 *
 * @syscap SystemCapability.DistributedDataManager.Preferences.Core
 *
 * @since 13
 */

/**
 * @file oh_preferences_value.h
 *
 * @brief 提供访问Preferences值（PreferencesValue）对象的接口、枚举类型与数据结构。
 *
 * 引用文件<database/preferences/oh_preferences_value.h>
 * @library libohpreferences.so
 * @syscap SystemCapability.DistributedDataManager.Preferences.Core
 *
 * @since 13
 */

#ifndef OH_PREFERENCES_VALUE_H
#define OH_PREFERENCES_VALUE_H

#include <cstdint>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 定义PreferencesValue的数据类型。
 *
 * @since 13
 */
typedef enum Preference_ValueType {
    /**
     * @brief 空类型
     */
    PREFERENCE_TYPE_NULL = 0,
    /**
     * @brief 整型类型
     */
    PREFERENCE_TYPE_INT,
    /**
     * @brief 布尔类型
     */
    PREFERENCE_TYPE_BOOL,
    /**
     * @brief 字符串类型
     */
    PREFERENCE_TYPE_STRING,
    /**
     * @brief 结束类型
     */
    PREFERENCE_TYPE_BUTT
} Preference_ValueType;

/**
 * @brief 定义Preferences使用的KV数据对象类型。
 *
 * @since 13
 */
typedef struct OH_PreferencesPair OH_PreferencesPair;

/**
 * @brief 定义PreferencesValue对象类型。
 *
 * @since 13
 */
typedef struct OH_PreferencesValue OH_PreferencesValue;

/**
 * @brief 获取KV数据中索引对应数据的键。
 *
 * @param pairs 目标KV数据{@link OH_PreferencesPair}的指针。
 * @param index 目标KV数据{@link OH_PreferencesPair}的索引值。
 * @return 如果操作成功，返回获取到的键的指针。操作失败或传参不合法返回空指针。
 * @see OH_PreferencesPair
 * @since 13
 */
const char *OH_PreferencesPair_GetKey(const OH_PreferencesPair *pairs, uint32_t index);

/**
 * @brief 获取KV数据数组中索引对应的值。
 *
 * @param pairs 目标KV数据{@link OH_PreferencesPair}的指针。
 * @param index 目标KV数据{@link OH_PreferencesPair}的索引值。
 * @return 如果操作成功，返回获取到的值对象的指针。操作失败或传参不合法返回空指针。
 * @see OH_PreferencesValue
 * @since 13
 */
const OH_PreferencesValue *OH_PreferencesPair_GetPreferencesValue(const OH_PreferencesPair *pairs, uint32_t index);

/**
 * @brief 获取PreferencesValue对象的数据类型。
 *
 * @param object PreferencesValue对象{@link OH_PreferencesValue}的指针。
 * @return 返回获取到的数据类型枚举。若返回数据类型枚举为PREFERENCE_TYPE_NULL，代表传参不合法。
 * @see OH_PreferencesValue
 * @since 13
 */
Preference_ValueType OH_PreferencesValue_GetValueType(const OH_PreferencesValue *object);

/**
 * @brief 从PreferencesValue对象{@link OH_PreferencesValue}中获取一个整型值。
 *
 * @param object PreferencesValue对象{@link OH_PreferencesValue}的指针。
 * @param value 该参数作为出参使用，表示指向获取到的整型值的指针。
 * @return 返回执行的错误码。
 *         若错误码为PREFERENCES_OK，表示操作成功。
 *         若错误码为PREFERENCES_ERROR_INVALID_PARAM，表示参数不合法。
 *         若错误码为PREFERENCES_ERROR_STORAGE，表示存储异常。
 *         若错误码为PREFERENCES_ERROR_MALLOC，表示内存分配失败。
 * @see OH_PreferencesValue
 * @see OH_Preferences_ErrCode
 * @since 13
 */
int OH_PreferencesValue_GetInt(const OH_PreferencesValue *object, int *value);

/**
 * @brief 从PreferencesValue对象{@link OH_PreferencesValue}中获取一个布尔值。
 *
 * @param object PreferencesValue对象{@link OH_PreferencesValue}的指针。
 * @param value 该参数作为出参使用，表示指向获取到的布尔值的指针。
 * @return 返回执行的错误码。
 *         若错误码为PREFERENCES_OK，表示操作成功。
 *         若错误码为PREFERENCES_ERROR_INVALID_PARAM，表示参数不合法。
 *         若错误码为PREFERENCES_ERROR_STORAGE，表示存储异常。
 *         若错误码为PREFERENCES_ERROR_MALLOC，表示内存分配失败。
 * @see OH_PreferencesValue
 * @see OH_Preferences_ErrCode
 * @since 13
 */
int OH_PreferencesValue_GetBool(const OH_PreferencesValue *object, bool *value);

/**
 * @brief 从PreferencesValue对象{@link OH_PreferencesValue}中获取字符串。
 *
 * @param object PreferencesValue对象{@link OH_PreferencesValue}的指针。
 * @param value 该参数作为出参使用，表示指向获取到的字符串的二级指针，使用完毕后需要调用释放函数{@link OH_Preferences_FreeString}释放内存。
 * @param valueLen 该参数作为出参使用，表示指向获取到的字符串长度的指针。
 * @return 返回执行的错误码。
 *         若错误码为PREFERENCES_OK，表示操作成功。
 *         若错误码为PREFERENCES_ERROR_INVALID_PARAM，表示参数不合法。
 *         若错误码为PREFERENCES_ERROR_STORAGE，表示存储异常。
 *         若错误码为PREFERENCES_ERROR_MALLOC，表示内存分配失败。
 * @see OH_PreferencesValue
 * @see OH_Preferences_ErrCode
 * @since 13
 */
int OH_PreferencesValue_GetString(const OH_PreferencesValue *object, char **value, uint32_t *valueLen);
#ifdef __cplusplus
};
#endif

/** @} */
#endif // OH_PREFERENCES_VALUE_H