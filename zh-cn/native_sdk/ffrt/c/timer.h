/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup FFRT
 * @{
 *
 * @brief FFRT（Function Flow运行时）是支持Function Flow编程模型的软件运行时库，用于调度执行开发者基于Function Flow编程模型开发的应用。
 *
 *
 * @syscap SystemCapability.Resourceschedule.Ffrt.Core
 *
 * @since 12
 */

 /**
 * @file timer.h
 *
 * @brief 声明定时器的C接口。
 *
 * @syscap SystemCapability.Resourceschedule.Ffrt.Core
 * @since 12
 */
#ifndef FFRT_API_C_TIMER_H
#define FFRT_API_C_TIMER_H
#include <stdbool.h>

#include "type_def.h"

/**
 * @brief 启动计时器。
 * 
 * @param qos qos等级。
 * @param timeout 超时时间。
 * @param data 超时后回调函数的入参。
 * @param cb 超时执行的回调函数。
 * @param repeat 是否重复执行该定时器（该功能暂未支持）。
 * @return 返回定时器句柄。
 * @since 12
*/
FFRT_C_API ffrt_timer_t ffrt_timer_start(ffrt_qos_t qos, uint64_t timeout, void* data, ffrt_timer_cb cb, bool repeat);

/**
 * @brief 关闭计时器。
 * 
 * @param qos qos等级。
 * @param handle 定时器句柄。
 * @return 0 代表返回成功，其余是失败。
 * @since 12
*/
FFRT_C_API int ffrt_timer_stop(ffrt_qos_t qos, ffrt_timer_t handle);
#endif
