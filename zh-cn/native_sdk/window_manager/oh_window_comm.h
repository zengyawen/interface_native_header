/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup WindowManager_NativeModule
 * @{
 *
 *
 * @brief 提供应用窗口的管理能力。
 *
 * @since 12
 */

/**
 * @file oh_window_comm.h
 *
 * @brief 提供窗口的公共枚举、公共定义等。
 *
 * @syscap SystemCapability.Window.SessionManager
 * 引用文件：<window_manager/oh_window_comm.h>
 * @library libnative_window_manager.so
 * @since 12
 */
#ifndef OH_WINDOW_COMM_H
#define OH_WINDOW_COMM_H

#include "stdint.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 窗口管理接口返回状态码枚举。
 *
 * @since 12
 */
typedef enum WindowManager_ErrorCode {
    /** 成功。 */
    OK = 0,
    /** 非法窗口ID。 */
    INVAILD_WINDOW_ID = 1000,
    /** 服务异常。 */
    SERVICE_ERROR = 2000,
} WindowManager_ErrorCode;

#ifdef __cplusplus
}
#endif

#endif // OH_WINDOW_COMM_H
/** @} */