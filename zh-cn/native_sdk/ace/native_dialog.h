/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup ArkUI_NativeModule
 * @{
 *
 * @brief 提供ArkUI在Native侧的UI能力，如UI组件创建销毁、树节点操作，属性设置，事件监听等。
 *
 * @since 12
 */

/**
 * @file native_dialog.h
 *
 * @brief 提供ArkUI在Native侧的自定义弹窗接口定义集合。
 *
 * @library libace_ndk.z.so
 * @syscap SystemCapability.ArkUI.ArkUI.Full
 * @since 12
 */

#ifndef ARKUI_NATIVE_DIALOG_H
#define ARKUI_NATIVE_DIALOG_H

#include "native_type.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
* @brief 弹窗关闭的触发方式。
*
* @since 12
*/
typedef enum {
    /** 系统定义的返回操作、键盘ESC触发。*/
    DIALOG_DISMISS_BACK_PRESS = 0,
    /** 点击遮障层触发。*/
    DIALOG_DISMISS_TOUCH_OUTSIDE,
    /** 点击关闭按钮。*/
    DIALOG_DISMISS_CLOSE_BUTTON,
    /** 下拉关闭。*/
    DIALOG_DISMISS_SLIDE_DOWN,
} ArkUI_DismissReason;

/**
* @brief 弹窗关闭的回调函数。
*
* @since 12
*/
typedef bool (*ArkUI_OnWillDismissEvent)(int32_t reason);

/**
 * @brief 定义弹窗关闭事件对象。
 *
 * @since 12
 */
typedef struct ArkUI_DialogDismissEvent ArkUI_DialogDismissEvent;

/**
 * @brief ArkUI提供的Native侧自定义弹窗接口集合。
 *
 * @version 1
 * @since 12
 */
typedef struct {
    /**
    * @brief 创建自定义弹窗并返回指向自定义弹窗的指针。
    *
    * @note create方法需要在调用show方法之前调用。
    * @return 返回指向自定义弹窗的指针，如创建失败，返回空指针。
    */
    ArkUI_NativeDialogHandle (*create)();
    /**
    * @brief 销毁自定义弹窗。
    *
    * @param handle 指向自定义弹窗控制器的指针。
    */
    void (*dispose)(ArkUI_NativeDialogHandle handle);
    /**
    * @brief 挂载自定义弹窗内容。
    *
    * @note setContent方法需要在调用show方法之前调用。
    * @param handle 指向自定义弹窗控制器的指针。
    * @param content 弹窗内容根节点指针。
    * @return 错误码。
    *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
    *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
    */
    int32_t (*setContent)(ArkUI_NativeDialogHandle handle, ArkUI_NodeHandle content);
    /**
    * @brief 卸载自定义弹窗内容。
    *
    * @note removeContent方法需要在调用show方法之前调用。
    * @param handle 指向自定义弹窗控制器的指针。
    * @return 错误码。
    *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
    *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
    */
    int32_t (*removeContent)(ArkUI_NativeDialogHandle handle);
    /**
    * @brief 为自定义弹窗设置对齐方式。
    *
    * @note setContentAlignment方法需要在调用show方法之前调用。
    * @param handle 指向自定义弹窗控制器的指针。
    * @param alignment 对齐方式，参数类型{@Link ArkUI_Alignment}。
    * @param offsetX 弹窗的水平偏移量，浮点型。
    * @param offsetY 弹窗的垂直偏移量，浮点型。
    * @return 错误码。
    *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
    *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
    */
    int32_t (*setContentAlignment)(ArkUI_NativeDialogHandle handle, int32_t alignment, float offsetX, float offsetY);
    /**
    * @brief 重置setContentAlignment方法设置的属性，使用系统默认的对齐方式。
    *
    * @note resetContentAlignment方法需要在调用show方法之前调用。
    * @param handle 指向自定义弹窗控制器的指针。
    * @return 错误码。
    *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
    *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
    */
    int32_t (*resetContentAlignment)(ArkUI_NativeDialogHandle handle);
    /**
    * @brief 设置自定义弹窗是否开启模态样式的弹窗。
    *
    * @note setModalMode方法需要在调用show方法之前调用。
    * @param handle 指向自定义弹窗控制器的指针。
    * @param isModal 设置是否开启模态窗口，模态窗口有蒙层，非模态窗口无蒙层，为true时开启模态窗口。
    * @return 错误码。
    *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
    *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
    */
    int32_t (*setModalMode)(ArkUI_NativeDialogHandle handle, bool isModal);
    /**
    * @brief 设置自定义弹窗是否允许点击遮罩层退出。
    *
    * @note setAutoCancel方法需要在调用show方法之前调用。
    * @param handle 指向自定义弹窗控制器的指针。
    * @param autoCancel 设置是否允许点击遮罩层退出，true表示关闭弹窗，false表示不关闭弹窗。
    * @return 错误码。
    *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
    *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
    */
    int32_t (*setAutoCancel)(ArkUI_NativeDialogHandle handle, bool autoCancel);
    /**
    * @brief 设置自定义弹窗遮罩属性。
    *
    * @note setMask方法需要在调用show方法之前调用。
    * @param handle 指向自定义弹窗控制器的指针。
    * @param maskColor 设置遮罩颜色，0xargb格式。
    * @param maskRect 遮蔽层区域范围的指针，遮蔽层区域内的事件不透传，在遮蔽层区域外的事件透传。参数类型{@Link ArkUI_Rect}。
    * @return 错误码。
    *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
    *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
    */
    int32_t (*setMask)(ArkUI_NativeDialogHandle handle, uint32_t maskColor, const ArkUI_Rect* maskRect);
    /**
    * @brief 设置弹窗背景色。
    *
    * @note setBackgroundColor方法需要在调用show方法之前调用。
    * @param handle 指向自定义弹窗控制器的指针。
    * @param backgroundColor 设置弹窗背景颜色，0xargb格式。
    * @return 错误码。
    *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
    *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
    */
    int32_t (*setBackgroundColor)(ArkUI_NativeDialogHandle handle, uint32_t backgroundColor);
    /**
    * @brief 设置弹窗背板圆角半径。
    *
    * @note setCornerRadius方法需要在调用show方法之前调用。
    * @param handle 指向自定义弹窗控制器的指针。
    * @param topLeft 设置弹窗背板左上角圆角半径。
    * @param topRight 设置弹窗背板右上角圆角半径。
    * @param bottomLeft 设置弹窗背板左下圆角半径。
    * @param bottomRight 设置弹窗背板右下角圆角半径。
    * @return 错误码。
    *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
    *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
    */
    int32_t (*setCornerRadius)(ArkUI_NativeDialogHandle handle, float topLeft, float topRight,
        float bottomLeft, float bottomRight);
    /**
    * @brief 弹窗宽度占栅格宽度的个数。
    *
    * @note setGridColumnCount方法需要在调用show方法之前调用。
    * @param handle 指向自定义弹窗控制器的指针。
    * @param gridCount 默认为按照窗口大小自适应，最大栅格数为系统最大栅格数。
    * @return 错误码。
    *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
    *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
    */
    int32_t (*setGridColumnCount)(ArkUI_NativeDialogHandle handle, int32_t gridCount);
    /**
    * @brief 弹窗容器样式是否自定义。
    *
    * @note enableCustomStyle方法需要在调用show方法之前调用。
    * @param handle 指向自定义弹窗控制器的指针。
    * @param enableCustomStyle true:宽度自适应子节点，圆角为0，弹窗背景色透明；false:高度自适应子节点，宽度由栅格系统定义, 圆角半径24vp。
    * @return 错误码。
    *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
    *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
    */
    int32_t (*enableCustomStyle)(ArkUI_NativeDialogHandle handle, bool enableCustomStyle);
    /**
    * @brief 弹窗容器是否使用自定义弹窗动画。
    *
    * @note enableCustomAnimation方法需要在调用show方法之前调用。
    * @param handle 指向自定义弹窗控制器的指针。
    * @param enableCustomAnimation true:使用自定义动画，关闭系统默认动画；false:使用系统默认动画。
    * @return 错误码。
    *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
    *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
    */
    int32_t (*enableCustomAnimation)(ArkUI_NativeDialogHandle handle, bool enableCustomAnimation);
    /**
    * @brief 当触发系统定义的返回操作、键盘ESC关闭交互操作时，如果注册该回调函数，则不会立刻关闭弹窗，是否关闭由用户自行决定。
    *
    * @note registerOnWillDismiss方法需要在调用show方法之前调用。
    * @param handle 指向自定义弹窗控制器的指针。
    * @param eventHandler 弹窗关闭的回调函数 参数类型{@Link OnWillDismissEvent}。
    * @return 错误码。
    *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
    *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
    */
    int32_t (*registerOnWillDismiss)(ArkUI_NativeDialogHandle handle, ArkUI_OnWillDismissEvent eventHandler);
    /**
    * @brief 显示自定义弹窗。
    *
    * @param handle 指向自定义弹窗控制器的指针。
    * @param showInSubWindow 是否在子窗口显示弹窗。
    * @return 错误码。
    *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
    *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
    */
    int32_t (*show)(ArkUI_NativeDialogHandle handle, bool showInSubWindow);
    /**
    * @brief 关闭自定义弹窗，如已关闭，则不生效。
    *
    * @param handle 指向自定义弹窗控制器的指针。
    * @return 错误码。
    *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
    *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
    */
    int32_t (*close)(ArkUI_NativeDialogHandle handle);
    /**
    * @brief 注册系统关闭自定义弹窗的监听事件。
    *
    * @param handle 指向自定义弹窗控制器的指针。
    * @param userData 用户自定义数据指针。
    * @param callback 监听自定义弹窗关闭的回调事件。
    * @return 错误码。
    *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
    *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
    */
    int32_t (*registerOnWillDismissWithUserData)(
        ArkUI_NativeDialogHandle handle, void* userData, void (*callback)(ArkUI_DialogDismissEvent* event));
} ArkUI_NativeDialogAPI_1;

/**
 * @brief ArkUI提供的Native侧自定义弹窗接口集合。
 *
 * @version 2
 * @since 16
 */
typedef struct {
    ArkUI_NativeDialogAPI_1 nativeDialogAPI1;
    /**
     * @brief 指定自定义弹窗避让系统键盘的距离。
     *
	 * @note setKeyboardAvoidDistance方法需要在调用show方法之前调用。
     * @param handle 指向自定义弹窗控制器的指针。
     * @param distance 避让键盘的距离，默认为vp。
     * @param unit 避让距离的单位，参数类型{@link ArkUI_LengthMetricUnit}。
     * @return 错误码。
     *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
     *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
     * @since 16
     */
    int32_t (*setKeyboardAvoidDistance)(ArkUI_NativeDialogHandle handle, float distance, ArkUI_LengthMetricUnit unit);
} ArkUI_NativeDialogAPI_2;

/**
 * @brief 设置是否需要屏蔽系统关闭弹窗行为，true表示屏蔽系统行为不关闭弹窗，false表示不屏蔽。
 *
 * @param event 弹窗关闭事件对象指针。
 * @param shouldBlockDismiss 实现需要屏蔽系统关闭弹窗行为。
 * @since 12
 */
void OH_ArkUI_DialogDismissEvent_SetShouldBlockDismiss(ArkUI_DialogDismissEvent* event, bool shouldBlockDismiss);

/**
 * @brief 获取弹窗关闭事件对象中的用户自定义数据指针。
 *
 * @param event 弹窗关闭事件对象指针。
 *
 * @return 用户自定义数据指针。
 * @since 12
 */
void* OH_ArkUI_DialogDismissEvent_GetUserData(ArkUI_DialogDismissEvent* event);

/**
 * @brief 获取交互式关闭事件指针中的关闭原因。
 *
 * @param event 弹窗关闭事件对象指针。
 *
 * @return 关闭原因，异常情况返回-1。
 *         {@link DIALOG_DISMISS_BACK_PRESS} 对应点击三键back、左滑/右滑、键盘ESC关闭。
 *         {@link DIALOG_DISMISS_TOUCH_OUTSIDE} 点击遮障层时。
 *         {@link DIALOG_DISMISS_CLOSE_BUTTON} 点击关闭按钮。
 *         {@link DIALOG_DISMISS_SLIDE_DOWN} 下拉关闭。
 * @since 12
 */
int32_t OH_ArkUI_DialogDismissEvent_GetDismissReason(ArkUI_DialogDismissEvent* event);

#ifdef __cplusplus
};
#endif

#endif // ARKUI_NATIVE_DIALOG_H
/** @} */