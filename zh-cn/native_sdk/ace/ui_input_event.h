/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup ArkUI_EventModule
 * @{
 *
 * @brief 在Native端提供ArkUI的UI输入事件能力。
 *
 * @since 12
 */

/**
 * @file ui_input_event.h
 *
 * @brief 提供ArkUI在Native侧的事件定义。
 *
 * @library libace_ndk.z.so
 * @syscap SystemCapability.ArkUI.ArkUI.Full
 * @since 12
 */

#ifndef UI_INPUT_EVENT
#define UI_INPUT_EVENT

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief UI输入事件定义。
 *
 * @since 12
 */
typedef struct ArkUI_UIInputEvent ArkUI_UIInputEvent;

/**
 * @brief UI输入事件类型定义。
 *
 * @since 12
 */
typedef enum {
    ARKUI_UIINPUTEVENT_TYPE_UNKNOWN = 0,
    ARKUI_UIINPUTEVENT_TYPE_TOUCH = 1,
    ARKUI_UIINPUTEVENT_TYPE_AXIS = 2,
    ARKUI_UIINPUTEVENT_TYPE_MOUSE = 3,
} ArkUI_UIInputEvent_Type;

/**
 * @brief 定义输入事件的Action Code。
 *
 * @since 12
 */
enum {
    /** 触摸取消。 */
    UI_TOUCH_EVENT_ACTION_CANCEL = 0,
    /** 触摸按下。 */
    UI_TOUCH_EVENT_ACTION_DOWN = 1,
    /** 触摸移动。 */
    UI_TOUCH_EVENT_ACTION_MOVE = 2,
    /** 触摸抬起。 */
    UI_TOUCH_EVENT_ACTION_UP = 3,
};

/**
 * @brief 产生输入事件的工具类型定义。
 *
 * @since 12
 */
enum {
    /** 不支持的工具类型。 */
    UI_INPUT_EVENT_TOOL_TYPE_UNKNOWN = 0,

    /** 手指。 */
    UI_INPUT_EVENT_TOOL_TYPE_FINGER = 1,

    /** 笔。 */
    UI_INPUT_EVENT_TOOL_TYPE_PEN = 2,

    /** 鼠标。 */
    UI_INPUT_EVENT_TOOL_TYPE_MOUSE = 3,

    /** 触控板。 */
    UI_INPUT_EVENT_TOOL_TYPE_TOUCHPAD = 4,

    /** 操纵杆。 */
    UI_INPUT_EVENT_TOOL_TYPE_JOYSTICK = 5,
};

/**
 * @brief 产生输入事件的来源类型定义。
 *
 * @since 12
 */
enum {
    /** 不支持的来源类型。 */
    UI_INPUT_EVENT_SOURCE_TYPE_UNKNOWN = 0,
    /** 鼠标。 */
    UI_INPUT_EVENTT_SOURCE_TYPE_MOUSE = 1,
    /** 触摸屏。 */
    UI_INPUT_EVENTT_SOURCE_TYPE_TOUCH_SCREEN = 2,
};

/**
 * @brief 定义触摸测试类型的枚举值。
 *
 * @since 12
 */
typedef enum {
    /** 默认触摸测试效果，自身和子节点都响应触摸测试，但会阻塞兄弟节点的触摸测试。 */
    HTMDEFAULT = 0,

    /** 自身响应触摸测试，阻塞子节点和兄弟节点的触摸测试。 */
    HTMBLOCK,

    /** 自身和子节点都响应触摸测试，不会阻塞兄弟节点的触摸测试。 */
    HTMTRANSPARENT,

    /** 自身不响应触摸测试，不会阻塞子节点和兄弟节点的触摸测试。 */
    HTMNONE,
} HitTestMode;

/**
 * @brief 定义鼠标事件的Action Code。
 *
 * @since 12
 */
enum {
    /** 无效行为 */
    UI_MOUSE_EVENT_ACTION_UNKNOWN = 0,
    /** 鼠标按键按下。 */
    UI_MOUSE_EVENT_ACTION_PRESS = 1,
    /** 鼠标按键松开。 */
    UI_MOUSE_EVENT_ACTION_RELEASE = 2,
    /** 鼠标移动。 */
    UI_MOUSE_EVENT_ACTION_MOVE = 3,
    /** 鼠标按键被取消。
     * @since 16
    */
    UI_MOUSE_EVENT_ACTION_CANCEL = 13,
};

/**
 * @brief 定义鼠标事件的按键类型。
 *
 * @since 12
 */
enum {
    /** 无按键。 */
    UI_MOUSE_EVENT_BUTTON_NONE = 0,
    /** 鼠标左键。 */
    UI_MOUSE_EVENT_BUTTON_LEFT = 1,
    /** 鼠标右键。 */
    UI_MOUSE_EVENT_BUTTON_RIGHT = 2,
    /** 鼠标中键。 */
    UI_MOUSE_EVENT_BUTTON_MIDDLE = 3,
    /** 鼠标左侧后退键。 */
    UI_MOUSE_EVENT_BUTTON_BACK = 4,
    /** 鼠标左侧前进键。 */
    UI_MOUSE_EVENT_BUTTON_FORWARD = 5,
};

/**
 * @brief 定义modifier按键。
 *
 * @since 12
 */
typedef enum {
    /** Ctrl. */
    ARKUI_MODIFIER_KEY_CTRL = 1 << 0,
    /** Shift. */
    ARKUI_MODIFIER_KEY_SHIFT = 1 << 1,
    /** Alt. */
    ARKUI_MODIFIER_KEY_ALT = 1 << 2,
    /** Fn. */
    ARKUI_MODIFIER_KEY_FN = 1 << 3,
} ArkUI_ModifierKeyName;

/**
 * @brief 定义焦点轴事件的轴类型。
 *
 * @since 15
 */
enum {
    /** ABS_X. */
    UI_FOCUS_AXIS_EVENT_ABS_X = 0,
    /** ABS_Y. */
    UI_FOCUS_AXIS_EVENT_ABS_Y = 1,
    /** ABS_Z. */
    UI_FOCUS_AXIS_EVENT_ABS_Z = 2,
    /** ABS_RZ. */
    UI_FOCUS_AXIS_EVENT_ABS_RZ = 3,
    /** ABS_GAS. */
    UI_FOCUS_AXIS_EVENT_ABS_GAS = 4,
    /** ABS_BRAKE. */
    UI_FOCUS_AXIS_EVENT_ABS_BRAKE = 5,
    /** ABS_HAT0X. */
    UI_FOCUS_AXIS_EVENT_ABS_HAT0X = 6,
    /** ABS_HAT0Y. */
    UI_FOCUS_AXIS_EVENT_ABS_HAT0Y = 7,
};

/**
 * @brief 定义轴事件的操作类型。
 *
 * @since 16
 */
enum {
    /** 轴事件异常。 */
    UI_AXIS_EVENT_ACTION_NONE = 0,
    /** 轴事件开始。 */
    UI_AXIS_EVENT_ACTION_BEGIN = 1,
    /** 轴事件更新。 */
    UI_AXIS_EVENT_ACTION_UPDATE = 2,
    /** 轴事件结束。 */
    UI_AXIS_EVENT_ACTION_END = 3,
    /** 轴事件取消。 */
    UI_AXIS_EVENT_ACTION_CANCEL = 4,
};

/**
 * @brief 获取UI输入事件的类型。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @return 返回当前UI输入事件的类型，如果参数异常则返回0。
 * @since 12
 */
int32_t OH_ArkUI_UIInputEvent_GetType(const ArkUI_UIInputEvent* event);

/**
 * @brief 获取UI输入事件的操作类型。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @return 返回当前UI输入事件的操作类型，如果参数异常则返回0。
 * @since 12
 */
int32_t OH_ArkUI_UIInputEvent_GetAction(const ArkUI_UIInputEvent* event);

/**
 * @brief 获取产生UI输入事件的来源类型。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @return 返回产生当前UI输入事件的来源类型。
 * @since 12
 */
int32_t OH_ArkUI_UIInputEvent_GetSourceType(const ArkUI_UIInputEvent* event);

/**
 * @brief 获取产生UI输入事件的工具类型。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @return 返回产生当前UI输入事件的工具类型。
 * @since 12
 */
int32_t OH_ArkUI_UIInputEvent_GetToolType(const ArkUI_UIInputEvent* event);

/**
 * @brief 获取UI输入事件发生的时间。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @return 返回UI输入事件发生的时间，如果参数异常则返回0。
 * @since 12
 */
int64_t OH_ArkUI_UIInputEvent_GetEventTime(const ArkUI_UIInputEvent* event);

/**
 * @brief 从带有指向性的输入事件（如触摸事件、鼠标事件、轴事件）中获取多点触控的接触点数量。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @return 返回当前带有指向性的输入事件的接触点数量。
 * @since 12
 */
uint32_t OH_ArkUI_PointerEvent_GetPointerCount(const ArkUI_UIInputEvent* event);

/**
 * @brief 从带有指向性的输入事件（如触摸事件、鼠标事件、轴事件）中获取多点触控的接触点标识。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @param pointerIndex 表示多点触控数据列表中的序号。
 * @return 返回特定接触点标识。
 * @since 12
 */
int32_t OH_ArkUI_PointerEvent_GetPointerId(const ArkUI_UIInputEvent* event, uint32_t pointerIndex);

/**
 * @brief 获取当前触摸事件触发的id。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @param pointerIndex 表示多点触控数据列表中的序号。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 * @since 16
 */
int32_t OH_ArkUI_PointerEvent_GetChangedPointerId(const ArkUI_UIInputEvent* event, uint32_t* pointerIndex);

/**
 * @brief 从带有指向性的输入事件（如触摸事件、鼠标事件、轴事件）中获取相对于当前组件左上角的X坐标。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @return 返回当前带有指向性的输入事件相对于当前组件左上角的X坐标，如果参数异常则返回0.0f。
 * @since 12
 */
float OH_ArkUI_PointerEvent_GetX(const ArkUI_UIInputEvent* event);

/**
 * @brief 从带有指向性的输入事件（如触摸事件、鼠标事件、轴事件）中获取特定接触点相对于当前组件左上角的X坐标。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @param pointerIndex 表示多点触控数据列表中的序号。
 * @return 返回当前带有指向性的输入事件相对于当前组件左上角的X坐标，如果参数异常则返回0.0f。
 * @since 12
 */
float OH_ArkUI_PointerEvent_GetXByIndex(const ArkUI_UIInputEvent* event, uint32_t pointerIndex);

/**
 * @brief 从带有指向性的输入事件（如触摸事件、鼠标事件、轴事件）中获取相对于当前组件左上角的Y坐标。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @return 返回当前带有指向性的输入事件相对于当前组件左上角的Y坐标，如果参数异常则返回0.0f。
 * @since 12
 */
float OH_ArkUI_PointerEvent_GetY(const ArkUI_UIInputEvent* event);

/**
 * @brief 从带有指向性的输入事件（如触摸事件、鼠标事件、轴事件）中获取特定接触点相对于当前组件左上角的Y坐标。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @param pointerIndex 表示多点触控数据列表中的序号。
 * @return 返回当前带有指向性的输入事件相对于当前组件左上角的Y坐标，如果参数异常则返回0.0f。
 * @since 12
 */
float OH_ArkUI_PointerEvent_GetYByIndex(const ArkUI_UIInputEvent* event, uint32_t pointerIndex);

/**
 * @brief 从带有指向性的输入事件（如触摸事件、鼠标事件、轴事件）中获取相对于当前应用窗口左上角的X坐标。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @return 返回当前带有指向性的输入事件相对于当前应用窗口左上角的X坐标，如果参数异常则返回0.0f。
 * @since 12
 */
float OH_ArkUI_PointerEvent_GetWindowX(const ArkUI_UIInputEvent* event);

/**
 * @brief 从带有指向性的输入事件（如触摸事件、鼠标事件、轴事件）中获取特定接触点相对于当前应用窗口左上角的X坐标。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @param pointerIndex 表示多点触控数据列表中的序号。
 * @return 返回当前带有指向性的输入事件相对于当前应用窗口左上角的X坐标，如果参数异常则返回0.0f。
 * @since 12
 */
float OH_ArkUI_PointerEvent_GetWindowXByIndex(const ArkUI_UIInputEvent* event, uint32_t pointerIndex);

/**
 * @brief 从带有指向性的输入事件（如触摸事件、鼠标事件、轴事件）中获取相对于当前应用窗口左上角的Y坐标。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @return 返回当前带有指向性的输入事件相对于当前应用窗口左上角的Y坐标，如果参数异常则返回0.0f。
 * @since 12
 */
float OH_ArkUI_PointerEvent_GetWindowY(const ArkUI_UIInputEvent* event);

/**
 * @brief 从带有指向性的输入事件（如触摸事件、鼠标事件、轴事件）中获取特定接触点相对于当前应用窗口左上角的Y坐标。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @param pointerIndex 表示多点触控数据列表中的序号。
 * @return 返回当前带有指向性的输入事件相对于当前应用窗口左上角的Y坐标，如果参数异常则返回0.0f。
 * @since 12
 */
float OH_ArkUI_PointerEvent_GetWindowYByIndex(const ArkUI_UIInputEvent* event, uint32_t pointerIndex);

/**
 * @brief 从带有指向性的输入事件（如触摸事件、鼠标事件、轴事件）中获取相对于当前屏幕左上角的X坐标。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @return 返回当前带有指向性的输入事件相对于当前屏幕左上角的X坐标，如果参数异常则返回0.0f。
 * @since 12
 */
float OH_ArkUI_PointerEvent_GetDisplayX(const ArkUI_UIInputEvent* event);

/**
 * @brief 从带有指向性的输入事件（如触摸事件、鼠标事件、轴事件）中获取特定接触点相对于当前屏幕左上角的X坐标。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @param pointerIndex 表示多点触控数据列表中的序号。
 * @return 返回当前带有指向性的输入事件相对于当前屏幕左上角的X坐标，如果参数异常则返回0.0f。
 * @since 12
 */
float OH_ArkUI_PointerEvent_GetDisplayXByIndex(const ArkUI_UIInputEvent* event, uint32_t pointerIndex);

/**
 * @brief 从带有指向性的输入事件（如触摸事件、鼠标事件、轴事件）中获取相对于当前屏幕左上角的Y坐标。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @return 返回当前带有指向性的输入事件相对于当前屏幕左上角的Y坐标，如果参数异常则返回0.0f。
 * @since 12
 */
float OH_ArkUI_PointerEvent_GetDisplayY(const ArkUI_UIInputEvent* event);

/**
 * @brief 从带有指向性的输入事件（如触摸事件、鼠标事件、轴事件）中获取特定接触点相对于当前屏幕左上角的Y坐标。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @param pointerIndex 表示多点触控数据列表中的序号。
 * @return 返回当前带有指向性的输入事件相对于当前屏幕左上角的Y坐标，如果参数异常则返回0.0f。
 * @since 12
 */
float OH_ArkUI_PointerEvent_GetDisplayYByIndex(const ArkUI_UIInputEvent* event, uint32_t pointerIndex);

/**
 * @brief 从带有指向性的输入事件（如触摸事件）中获取触屏压力。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @param pointerIndex 表示多点触控数据列表中的序号。
 * @return 返回当前带有指向性的输入事件产生的触屏压力，如果参数异常则返回0.0f。
 * @since 12
 */
float OH_ArkUI_PointerEvent_GetPressure(const ArkUI_UIInputEvent* event, uint32_t pointerIndex);

/**
 * @brief 从带有指向性的输入事件（如触摸事件）中获取相对YZ平面的角度，取值的范围[-90, 90]，其中正值是向右倾斜。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @param pointerIndex 表示多点触控数据列表中的序号。
 * @return 返回当前带有指向性的输入事件中相对YZ平面的角度。
 * @since 12
 */
float OH_ArkUI_PointerEvent_GetTiltX(const ArkUI_UIInputEvent* event, uint32_t pointerIndex);

/**
 * @brief 从带有指向性的输入事件（如触摸事件）中获取相对XZ平面的角度，值的范围[-90, 90]，其中正值是向下倾斜。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @param pointerIndex 表示多点触控数据列表中的序号。
 * @return 返回当前带有指向性的输入事件中相对XZ平面的角度。
 * @since 12
 */
float OH_ArkUI_PointerEvent_GetTiltY(const ArkUI_UIInputEvent* event, uint32_t pointerIndex);

/**
 * @brief 从带有指向性的输入事件（如触摸事件）中获取触屏区域的宽度。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @param pointerIndex 表示多点触控数据列表中的序号。
 * @return 返回当前带有指向性的输入事件中触屏区域的宽度。
 * @since 12
 */
float OH_ArkUI_PointerEvent_GetTouchAreaWidth(const ArkUI_UIInputEvent* event, uint32_t pointerIndex);

/**
 * @brief 从带有指向性的输入事件（如触摸事件）中获取触屏区域的高度。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @param pointerIndex 表示多点触控数据列表中的序号。
 * @return 返回当前带有指向性的输入事件中触屏区域的高度。
 * @since 12
 */
float OH_ArkUI_PointerEvent_GetTouchAreaHeight(const ArkUI_UIInputEvent* event, uint32_t pointerIndex);

/**
 * @brief 从带有指向性的输入事件（如触摸事件、鼠标事件、轴事件）中获取历史事件数量。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @return 返回当前历史事件数量。
 * @since 12
 */
uint32_t OH_ArkUI_PointerEvent_GetHistorySize(const ArkUI_UIInputEvent* event);

/**
 * @brief 从带有指向性的输入事件（如触摸事件、鼠标事件、轴事件）中获取历史事件发生的时间。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @param historyIndex 表示历史事件数据列表的序号。
 * @return 返回UI输入事件发生的时间，如果参数异常则返回0。
 * @since 12
 */
int64_t OH_ArkUI_PointerEvent_GetHistoryEventTime(const ArkUI_UIInputEvent* event, uint32_t historyIndex);

/**
 * @brief 从带有指向性的输入事件（如触摸事件、鼠标事件、轴事件）中获取特定历史事件中多点触控的接触点数量。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @param historyIndex 表示历史事件数据列表的序号。
 * @return 特定历史事件中多点触控的接触点数量。
 * @since 12
 */
uint32_t OH_ArkUI_PointerEvent_GetHistoryPointerCount(const ArkUI_UIInputEvent* event, uint32_t historyIndex);

/**
 * @brief 从带有指向性的输入事件（如触摸事件、鼠标事件、轴事件）中获取多点触控的接触点标识。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @param pointerIndex 表示多点触控数据列表的序号。
 * @param historyIndex 表示历史事件数据列表的序号。
 * @return 返回特定历史事件中的特定接触点标识。
 * @since 12
 */
int32_t OH_ArkUI_PointerEvent_GetHistoryPointerId(
    const ArkUI_UIInputEvent* event, uint32_t pointerIndex, uint32_t historyIndex);

/**
 * @brief 从带有指向性的输入事件（如触摸事件、鼠标事件、轴事件）中获取特定历史事件中特定接触点相对于当前组件左上角的X坐标。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @param pointerIndex 表示多点触控数据列表的序号。
 * @param historyIndex 表示历史事件数据列表的序号。
 * @return 返回当前带有指向性的输入事件相对于当前组件左上角的X坐标，如果参数异常则返回0.0f。
 * @since 12
 */
float OH_ArkUI_PointerEvent_GetHistoryX(const ArkUI_UIInputEvent* event, uint32_t pointerIndex, uint32_t historyIndex);

/**
 * @brief 从带有指向性的输入事件（如触摸事件、鼠标事件、轴事件）中获取特定历史事件中特定接触点相对于当前组件左上角的Y坐标。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @param pointerIndex 表示多点触控数据列表的序号。
 * @param historyIndex 表示历史事件数据列表的序号。
 * @return 返回当前带有指向性的输入事件相对于当前组件左上角的Y坐标，如果参数异常则返回0.0f。
 * @since 12
 */
float OH_ArkUI_PointerEvent_GetHistoryY(const ArkUI_UIInputEvent* event, uint32_t pointerIndex, uint32_t historyIndex);

/**
 * @brief 从带有指向性的输入事件（如触摸事件、鼠标事件、轴事件）中获取特定历史事件中特定接触点相对于当前应用窗口左上角的X坐标。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @param pointerIndex 表示多点触控数据列表的序号。
 * @param historyIndex 表示历史事件数据列表的序号。
 * @return 返回当前带有指向性的输入事件相对于当前应用窗口左上角的X坐标，如果参数异常则返回0.0f。
 * @since 12
 */
float OH_ArkUI_PointerEvent_GetHistoryWindowX(
    const ArkUI_UIInputEvent* event, uint32_t pointerIndex, uint32_t historyIndex);

/**
 * @brief 从带有指向性的输入事件（如触摸事件、鼠标事件、轴事件）中获取特定历史事件中特定接触点相对于当前应用窗口左上角的Y坐标。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @param pointerIndex 表示多点触控数据列表的序号。
 * @param historyIndex 表示历史事件数据列表的序号。
 * @return 返回当前带有指向性的输入事件相对于当前应用窗口左上角的Y坐标，如果参数异常则返回0.0f。
 * @since 12
 */
float OH_ArkUI_PointerEvent_GetHistoryWindowY(
    const ArkUI_UIInputEvent* event, uint32_t pointerIndex, uint32_t historyIndex);

/**
 * @brief 从带有指向性的输入事件（如触摸事件、鼠标事件、轴事件）中获取特定历史事件中特定接触点相对于当前屏幕左上角的X坐标。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @param pointerIndex 表示多点触控数据列表的序号。
 * @param historyIndex 表示历史事件数据列表的序号。
 * @return 返回当前带有指向性的输入事件相对于当前屏幕左上角的X坐标，如果参数异常则返回0.0f。
 * @since 12
 */
float OH_ArkUI_PointerEvent_GetHistoryDisplayX(
    const ArkUI_UIInputEvent* event, uint32_t pointerIndex, uint32_t historyIndex);

/**
 * @brief 从带有指向性的输入事件（如触摸事件、鼠标事件、轴事件）中获取特定历史事件中特定接触点相对于当前屏幕左上角的Y坐标。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @param pointerIndex 表示多点触控数据列表的序号。
 * @param historyIndex 表示历史事件数据列表的序号。
 * @return 返回当前带有指向性的输入事件相对于当前屏幕左上角的Y坐标，如果参数异常则返回0.0f。
 * @since 12
 */
float OH_ArkUI_PointerEvent_GetHistoryDisplayY(
    const ArkUI_UIInputEvent* event, uint32_t pointerIndex, uint32_t historyIndex);

/**
 * @brief 从带有指向性的输入事件（如触摸事件）中获取特定历史事件中的触屏压力。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @param pointerIndex 表示多点触控数据列表中的序号。
 * @param historyIndex 表示历史事件数据列表的序号。
 * @return 返回当前带有指向性的输入事件产生的触屏压力，如果参数异常则返回0.0f。
 * @since 12
 */
float OH_ArkUI_PointerEvent_GetHistoryPressure(
    const ArkUI_UIInputEvent* event, uint32_t pointerIndex, uint32_t historyIndex);

/**
 * @brief 从带有指向性的输入事件（如触摸事件）中获取特定历史事件中的相对YZ平面的角度，取值的范围[-90, 90]，其中正值是向右倾斜。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @param pointerIndex 表示多点触控数据列表中的序号。
 * @param historyIndex 表示历史事件数据列表的序号。
 * @return 返回当前带有指向性的输入事件中相对YZ平面的角度。
 * @since 12
 */
float OH_ArkUI_PointerEvent_GetHistoryTiltX(
    const ArkUI_UIInputEvent* event, uint32_t pointerIndex, uint32_t historyIndex);

/**
 * @brief 从带有指向性的输入事件（如触摸事件）中获取特定历史事件中的相对XZ平面的角度，值的范围[-90, 90]，其中正值是向下倾斜。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @param pointerIndex 表示多点触控数据列表中的序号。
 * @param historyIndex 表示历史事件数据列表的序号。
 * @return 返回当前带有指向性的输入事件中相对XZ平面的角度。
 * @since 12
 */
float OH_ArkUI_PointerEvent_GetHistoryTiltY(
    const ArkUI_UIInputEvent* event, uint32_t pointerIndex, uint32_t historyIndex);

/**
 * @brief 从带有指向性的输入事件（如触摸事件）中获取特定历史事件中的触屏区域的宽度。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @param pointerIndex 表示多点触控数据列表中的序号。
 * @param historyIndex 表示历史事件数据列表的序号。
 * @return 返回当前带有指向性的输入事件中触屏区域的宽度。
 * @since 12
 */
float OH_ArkUI_PointerEvent_GetHistoryTouchAreaWidth(
    const ArkUI_UIInputEvent* event, uint32_t pointerIndex, uint32_t historyIndex);

/**
 * @brief 从带有指向性的输入事件（如触摸事件）中获取特定历史事件中的触屏区域的高度。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @param pointerIndex 表示多点触控数据列表中的序号。
 * @param historyIndex 表示历史事件数据列表的序号。
 * @return 返回当前带有指向性的输入事件中触屏区域的高度。
 * @since 12
 */
float OH_ArkUI_PointerEvent_GetHistoryTouchAreaHeight(
    const ArkUI_UIInputEvent* event, uint32_t pointerIndex, uint32_t historyIndex);

/**
 * @brief 获取当前轴事件的垂直滚动轴的值。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @return 返回当前轴事件的垂直滚动轴的值，如果参数异常则返回0.0。
 * @since 12
 */
double OH_ArkUI_AxisEvent_GetVerticalAxisValue(const ArkUI_UIInputEvent* event);

/**
 * @brief 获取当前轴事件的水平滚动轴的值。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @return 返回当前轴事件的水平滚动轴的值，如果参数异常则返回0.0。
 * @since 12
 */
double OH_ArkUI_AxisEvent_GetHorizontalAxisValue(const ArkUI_UIInputEvent* event);

/**
 * @brief 获取当前轴事件的捏合轴缩放的值。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @return 返回当前轴事件的捏合轴缩放的值，如果参数异常则返回0.0。
 * @since 12
 */
double OH_ArkUI_AxisEvent_GetPinchAxisScaleValue(const ArkUI_UIInputEvent* event);

/**
 * @brief 获取当前轴事件的操作类型。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @return 返回当前轴事件的操作类型。
 * @since 16
 */
int32_t OH_ArkUI_AxisEvent_GetAxisAction(const ArkUI_UIInputEvent* event);

/**
 * @brief 配置HitTest模式。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @param mode 指定HitTest模式，参数类型{@link HitTestMode}。
 * @return 返回执行的状态代码。
 * @since 12
 */
int32_t OH_ArkUI_PointerEvent_SetInterceptHitTestMode(const ArkUI_UIInputEvent* event, HitTestMode mode);

/**
 * @brief 获取鼠标事件的按键类型的值。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @return 返回鼠标按键类型，1为左键，2为右键，3为中键，4为后退键，5为前进键。
 * @since 12
 */
int32_t OH_ArkUI_MouseEvent_GetMouseButton(const ArkUI_UIInputEvent* event);

/**
 * @brief 获取鼠标事件的鼠标动作类型的值。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @return 返回鼠标动作类型，1表示按键按下，2表示按键松开，3表示鼠标移动。
 * @since 12
 */
int32_t OH_ArkUI_MouseEvent_GetMouseAction(const ArkUI_UIInputEvent* event);

/**
 * @brief 设置是否阻止事件冒泡。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @param stopPropagation 表示是否阻止事件冒泡。
 * @return 返回执行的状态代码。返回0表示设置成功，如果返回401，表示返回失败，可能的原因是参数异常，例如event是一个空指针。
 * @since 12
 */
int32_t OH_ArkUI_PointerEvent_SetStopPropagation(const ArkUI_UIInputEvent* event, bool stopPropagation);

/**
 * @brief 获取当前按键的输入设备ID。
 *
 * @param event ArkUI_UIInputEvent事件指针。
 * @return 当前按键的输入设备ID。
 * @since 14
 */
int32_t OH_ArkUI_UIInputEvent_GetDeviceId(const ArkUI_UIInputEvent* event);

/**
 * @brief 获取所有按下的按键，当前只支持按键事件。
 *
 * @param event ArkUI_UIInputEvent事件指针。
 * @param pressedKeyCodes 输出参数，表示所有按下键的数组，指向的内存空间需要调用者申请。
 * @param length 作为输入参数表示传入的pressedKeyCodes数组长度，作为输出参数表示实际按下按键的个数。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_BUFFER_SIZE_NOT_ENOUGH} 内存分配不足。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 * @since 14
 */
int32_t OH_ArkUI_UIInputEvent_GetPressedKeys(
    const ArkUI_UIInputEvent* event, int32_t* pressedKeyCodes, int32_t* length);

/**
 * @brief 获取焦点轴事件的轴值。
 *
 * @param event ArkUI_UIInputEvent事件指针。
 * @param axis 焦点轴事件的轴类型。
 * @return 焦点轴事件的轴值，如果参数异常则返回0.0。
 * @since 15
 */
double OH_ArkUI_FocusAxisEvent_GetAxisValue(const ArkUI_UIInputEvent* event, int32_t axis);

/**
 * @brief 设置是否阻止焦点轴事件冒泡。
 *
 * @param event 表示指向当前UI输入事件的指针。
 * @param stopPropagation 表示是否阻止事件冒泡。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 参数异常。
 * @since 15
 */
int32_t OH_ArkUI_FocusAxisEvent_SetStopPropagation(const ArkUI_UIInputEvent* event, bool stopPropagation);

/**
 * @brief 获取UI输入事件的功能键按压状态。
 *
 * @param event ArkUI_UIInputEvent事件指针。
 * @param keys 返回当前处于按下状态的modifier key组合，应用可通过位运算进行判断。
 * @return 错误码。
 *         {@link ARKUI_ERROR_CODE_NO_ERROR} 成功。
 *         {@link ARKUI_ERROR_CODE_PARAM_INVALID} 函数参数异常。
 * @since 16
 */
int32_t OH_ArkUI_UIInputEvent_GetModifierKeyStates(const ArkUI_UIInputEvent* event, uint64_t* keys);

/**
* @brief 获取事件命中的组件的宽度。
*
* @param event 指向ArkUI_UIInputEvent对象的指针。
* @return 返回事件命中的组件的宽度；如果发生任何参数错误，则返回 0.0f。
* @since 16
*/
float OH_ArkUI_UIInputEvent_GetEventTargetWidth(const ArkUI_UIInputEvent* event);

/**
* @brief 获取事件命中的组件的高度。
*
* @param event 指向ArkUI_UIInputEvent对象的指针。
* @return 返回事件命中的组件的高度；如果发生任何参数错误，则返回 0.0f。
* @since 16
*/
float OH_ArkUI_UIInputEvent_GetEventTargetHeight(const ArkUI_UIInputEvent* event);

/**
* @brief 获取事件命中的组件的X坐标。
*
* @param event 指向ArkUI_UIInputEvent对象的指针。
* @return 返回事件命中的组件的X坐标；如果发生任何参数错误，则返回 0.0f。
* @since 16
*/
float OH_ArkUI_UIInputEvent_GetEventTargetPositionX(const ArkUI_UIInputEvent* event);

/**
* @brief 获取事件命中的组件的Y坐标。
*
* @param event 指向ArkUI_UIInputEvent对象的指针。
* @return 返回事件命中的组件的Y坐标；如果发生任何参数错误，则返回 0.0f。
* @since 16
*/
float OH_ArkUI_UIInputEvent_GetEventTargetPositionY(const ArkUI_UIInputEvent* event);

/**
* @brief 获取事件命中的组件的全局X坐标。
*
* @param event 指向ArkUI_UIInputEvent对象的指针。
* @return 返回事件命中的组件的全局X坐标；如果发生任何参数错误，则返回 0.0。
* @since 16
*/
float OH_ArkUI_UIInputEvent_GetEventTargetGlobalPositionX(const ArkUI_UIInputEvent* event);

/**
* @brief 获取事件命中的组件的全局Y坐标。
*
* @param event 指向ArkUI_UIInputEvent对象的指针。
* @return 返回事件命中的组件的全局Y坐标；如果发生任何参数错误，则返回 0.0f。
* @since 16
*/
float OH_ArkUI_UIInputEvent_GetEventTargetGlobalPositionY(const ArkUI_UIInputEvent* event);

 * @brief 获取特定触摸点的按压时间。
 *
 * @param event ArkUI_UIInputEvent事件指针。
 * @param pointerIndex 指示多点触控数据列表中目标触控点的索引。
 * @return 返回特定触摸点的按下时间；如果发生任何参数错误，则返回0。
 * @since 16
 */
int64_t OH_ArkUI_PointerEvent_GetPressedTimeByIndex(const ArkUI_UIInputEvent* event, uint32_t pointerIndex);

/**
 * @brief 获取特定触摸点的压力。
 *
 * @param event ArkUI_UIInputEvent事件指针。
 * @param pointerIndex 指示多点触控数据列表中目标触控点的索引。
 * @return 返回特定触摸点的压力；如果发生任何参数错误，则返回0.0f。
 * @since 16
 */
float OH_ArkUI_PointerEvent_GetPressureByIndex(const ArkUI_UIInputEvent* event, uint32_t pointerIndex);

/**
 * @brief 获取X轴相对于前一个上报的鼠标事件的鼠标指针位置的偏移量。当鼠标指针位于屏幕边缘时，该值可能小于两次上报的X坐标的差。
 * @param event ArkUI_UIInputEvent事件指针。
 * @return 返回相对于前一个上报的鼠标事件的鼠标指针位置的X轴偏移量；如果发生任何参数错误，则返回0.0f。
 * @since 16
 */
float OH_ArkUI_MouseEvent_GetRawDeltaX(const ArkUI_UIInputEvent* event);

/**
 * @brief 获取相对于前一个上报的鼠标事件的鼠标指针位置的Y轴偏移量。当鼠标指针位于屏幕边缘时，该值可能小于两次上报的Y坐标的差。
 * @param event ArkUI_UIInputEvent事件指针。
 * @return 返回相对于前一个上报的鼠标事件的鼠标指针位置的Y轴偏移量；如果发生任何参数错误，则返回0.0f。
 * @since 16
 */
float OH_ArkUI_MouseEvent_GetRawDeltaY(const ArkUI_UIInputEvent* event);

/**
 * @brief 从鼠标事件中获取按下的按钮。
 *
 * @param event ArkUI_UIInputEvent事件指针。
 * @param pressedButtons 指示按下按钮的列表。需要先创建一个int数组，用来储存按下的按钮。
 * @param length 指示列表数组的总长度。
 * @return 返回结果代码。
 *         如果操作成功，则返回{@linkARKUI_ERROR_CODE_NO_ERROR}。
 *         如果给定的缓冲区不够，则返回{@linkARKUI_ERROR_CODE_BUFFER_SIZE_ERROR}。
 * @since 16
 */
int32_t OH_ArkUI_MouseEvent_GetPressedButtons(const ArkUI_UIInputEvent* event, int32_t* pressedButtons, int32_t length);

/**
 * @brief 获取发生UI输入事件的屏幕ID。
 *
 * @param event ArkUI_UIInputEvent事件指针。
 * @return 返回屏幕ID；如果发生任何参数错误，则返回0。
 * @since 16
 */
int32_t OH_ArkUI_UIInputEvent_GetTargetDisplayId(const ArkUI_UIInputEvent* event);

/**
* @brief 获取鼠标是否悬浮在当前组件上
*
* @param event ArkUI_UIInputEvent事件指针。
* @return 如果鼠标悬浮在当前组件上则返回true。
*         如果鼠标没有悬浮在当前组件上，返回false
* @since 16
*/
bool OH_ArkUI_HoverEvent_IsHovered(const ArkUI_UIInputEvent* event);
#ifdef __cplusplus
};
#endif

#endif // UI_INPUT_EVENT
/** @} */
