/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef C_INCLUDE_EFFECT_TYPES_H
#define C_INCLUDE_EFFECT_TYPES_H

/**
 * @addtogroup effectKit
 * @{
 *
 * @brief 提供处理图像的一些基础能力，包括对当前图像的亮度调节、模糊化、灰度调节等。
 *
 * @since 12
 * @version 1.0
 */

/**
 * @file effect_types.h
 *
 * @brief 声明滤镜效果的数据类型。
 *
 * 引用文件<native_effect/effect_types.h>
 * @library libnative_effect.so
 * @syscap SystemCapability.Multimedia.Image.Core
 * @since 12
 */

#include <stdint.h>
#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 滤镜结构体，用来生成滤镜位图。
 *
 * @since 12
 */
typedef struct OH_Filter {
    /** 定义存储图像滤镜对象指针的容器 */
    std::vector<sk_sp<SkImageFilter> > skFilters_;
}

/**
 * @brief 定义一个位图。
 *
 * @since 12
 */
typedef struct OH_PixelmapNative {
    /** 指向pixelMap对象的智能指针 */
    std::shared_ptr<OHOS::Media::PixelMap> pixelMap;
};

/**
 * @brief 定义一个用来创建滤镜效果的矩阵。
 *
 * @since 12
 */
struct OH_Filter_ColorMatrix {
    /** 自定义颜色矩阵，值是一个5*4的数组 */
    float val[20];
};

/**
 * @brief 定义滤镜效果的状态码。
 *
 * @since 12
 */
typedef enum EffectErrorCode {
    /** 成功 */
    EFFECT_SUCCESS = 0,
    /** 无效的参数 */
    EFFECT_BAD_PARAMETER = 401,
    /** 不支持的操作 */
    EFFECT_UNSUPPORTED_OPERATION = 7600201,
    /** 未知错误 */
    EFFECT_UNKNOWN_ERROR = 7600901,
} EffectErrorCode;

/**
 * @brief 定义着色器效果平铺模式的枚举。
 *
 * @since 14
 */
typedef enum {
    /** 如果着色器效果超出其原始边界，剩余区域使用着色器的边缘颜色填充。 */
    CLAMP = 0,
    /** 在水平和垂直方向上重复着色器效果。 */
    REPEAT,
    /** 在水平和垂直方向上重复着色器效果，交替镜像图像，以便相邻图像始终接合。*/
    MIRROR,
    /** 仅在其原始边界内渲染着色器效果。 */
    DECAL,
} EffectTileMode;

#ifdef __cplusplus
}
#endif
/** @} */
#endif