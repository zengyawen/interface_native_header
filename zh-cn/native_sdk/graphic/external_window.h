/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef NDK_INCLUDE_EXTERNAL_NATIVE_WINDOW_H_
#define NDK_INCLUDE_EXTERNAL_NATIVE_WINDOW_H_

/**
 * @addtogroup NativeWindow
 * @{
 *
 * @brief NativeWindow模块提供图像buffer轮转功能，可用来和egl对接。
 * 开发者作为图像buffer的生产者，生产buffer并通过NativeWindow传递buffer供消费端去读取。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @since 8
 * @version 1.0
 */

/**
 * @file external_window.h
 *
 * @brief 定义获取和使用NativeWindow的相关函数。
 *
 * 引用文件<native_window/external_window.h>
 * @library libnative_window.so
 * @since 8
 * @version 1.0
 */

#include <stdint.h>
#include "buffer_handle.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 提供对IPC序列化对象的访问功能。
 *
 * @since 12
 * @version 1.0
 */
typedef struct OHIPCParcel OHIPCParcel;

/**
 * @brief NativeWindow结构体。
 * @since 8
 */
struct NativeWindow;

/**
 * @brief NativeWindowBuffer结构体。
 * @since 8
 */
struct NativeWindowBuffer;

/**
 * @brief 提供对OHNativeWindow的访问功能。
 * @since 8
 */
typedef struct NativeWindow OHNativeWindow;

/**
 * @brief 提供对OHNativeWindowBuffer的访问功能。
 * @since 8
 */
typedef struct NativeWindowBuffer OHNativeWindowBuffer;

/**
 * @brief 表示本地窗口OHNativeWindow需要更新内容的矩形区域（脏区）。
 * @since 8
 */
typedef struct Region {
    /** 如果rects是空指针nullptr， 默认Buffer大小为脏区 */
    struct Rect {
        int32_t x; /**< 矩形框起始x坐标 */
        int32_t y; /**< 矩形框起始y坐标 */
        uint32_t w; /**< 矩形框宽度 */
        uint32_t h; /**< 矩形框高度 */
    } *rects;
    /** 如果rectNumber为0，默认Buffer大小为脏区 */
    int32_t rectNumber;
}Region;

/**
 * @brief 接口错误码说明（仅用于查询）。
 * @since 12
 */
typedef enum OHNativeErrorCode {
    /** 成功 */
    NATIVE_ERROR_OK = 0,
    /**
     * 内存操作错误
     * @since 15
     */
    NATIVE_ERROR_MEM_OPERATION_ERROR = 30001000,
    /** 入参无效 */
    NATIVE_ERROR_INVALID_ARGUMENTS = 40001000,
    /** 无权限操作 */
    NATIVE_ERROR_NO_PERMISSION = 40301000,
    /** 无空闲可用的buffer */
    NATIVE_ERROR_NO_BUFFER = 40601000,
    /** 消费端不存在 */
    NATIVE_ERROR_NO_CONSUMER = 41202000,
    /** 未初始化 */
    NATIVE_ERROR_NOT_INIT = 41203000,
    /** 消费端已经被连接 */
    NATIVE_ERROR_CONSUMER_CONNECTED = 41206000,
    /** buffer状态不符合预期 */
    NATIVE_ERROR_BUFFER_STATE_INVALID = 41207000,
    /** buffer已在缓存队列中 */
    NATIVE_ERROR_BUFFER_IN_CACHE = 41208000,
    /** 队列已满 */
    NATIVE_ERROR_BUFFER_QUEUE_FULL = 41209000,
    /** buffer不在缓存队列中 */
    NATIVE_ERROR_BUFFER_NOT_IN_CACHE = 41210000,
    /** 消费端已经被断开连接 */
    NATIVE_ERROR_CONSUMER_DISCONNECTED = 41211000,
    /** 消费端未注册listener回调函数 */
    NATIVE_ERROR_CONSUMER_NO_LISTENER_REGISTERED = 41212000,
    /** 当前设备或平台不支持 */
    NATIVE_ERROR_UNSUPPORTED = 50102000,
    /** 未知错误，请查看日志 */
    NATIVE_ERROR_UNKNOWN = 50002000,
    /** HDI接口调用失败 */
    NATIVE_ERROR_HDI_ERROR = 50007000,
    /** 跨进程通信失败 */
    NATIVE_ERROR_BINDER_ERROR = 50401000,
    /** egl环境状态异常 */
    NATIVE_ERROR_EGL_STATE_UNKNOWN = 60001000,
    /** egl接口调用失败 */
    NATIVE_ERROR_EGL_API_FAILED = 60002000,
} OHNativeErrorCode;

/**
 * @brief OH_NativeWindow_NativeWindowHandleOpt函数中的操作码。
 * @since 8
 */
typedef enum NativeWindowOperation {
    /**
     * 设置本地窗口缓冲区几何图形，
     * 函数中的可变参数是
     * [输入] int32_t width，[输入] int32_t height。
     */
    SET_BUFFER_GEOMETRY,
    /**
     * 获取本地窗口缓冲区几何图形，
     * 函数中的可变参数是
     * [输出] int32_t *height， [输出] int32_t *width。
     */
    GET_BUFFER_GEOMETRY,
    /**
     * 获取本地窗口缓冲区格式，
     * 函数中的可变参数是
     * [输出] int32_t *format， 取值具体可见{@link OH_NativeBuffer_Format}枚举值。
     */
    GET_FORMAT,
    /**
     * 设置本地窗口缓冲区格式，
     * 函数中的可变参数是
     * [输入] int32_t format， 取值具体可见{@link OH_NativeBuffer_Format}枚举值。
     */
    SET_FORMAT,
    /**
     * 获取本地窗口读写方式，
     * 函数中的可变参数是
     * [输出] uint64_t *usage， 取值具体可见{@link OH_NativeBuffer_Usage}枚举值。
     */
    GET_USAGE,
    /**
     * 设置本地窗口缓冲区读写方式，
     * 函数中的可变参数是
     * [输入] uint64_t usage， 取值具体可见{@link OH_NativeBuffer_Usage}枚举值。
     */
    SET_USAGE,
    /**
     * 设置本地窗口缓冲区步幅，
     * 函数中的可变参数是
     * [输入] int32_t stride。
     */
    SET_STRIDE,
    /**
     * 获取本地窗口缓冲区步幅，
     * 函数中的可变参数是
     * [输出] int32_t *stride。
     */
    GET_STRIDE,
    /**
     * 设置本地窗口缓冲区交换间隔，
     * 函数中的可变参数是
     * [输入] int32_t interval。
     */
    SET_SWAP_INTERVAL,
    /**
     * 获取本地窗口缓冲区交换间隔，
     * 函数中的可变参数是
     * [输出] int32_t *interval。
     */
    GET_SWAP_INTERVAL,
    /**
     * 设置请求本地窗口请求缓冲区的超时等待时间，
     * 未手动设置时默认值为3000毫秒，
     * 函数中的可变参数是
     * [输入] int32_t timeout, 单位为毫秒。
     */
    SET_TIMEOUT,
    /**
     * 获取请求本地窗口请求缓冲区的超时等待时间，
     * 未手动设置时默认值为3000毫秒，
     * 函数中的可变参数是
     * [输出] int32_t *timeout，单位为毫秒。
     */
    GET_TIMEOUT,
    /**
     * 设置本地窗口缓冲区色彩空间，
     * 函数中的可变参数是
     * [输入] int32_t colorGamut， 取值具体可见{@link OH_NativeBuffer_ColorGamut}枚举值。
     */
    SET_COLOR_GAMUT,
    /**
     * 获取本地窗口缓冲区色彩空间，
     * 函数中的可变参数是
     * [输出] int32_t *colorGamut， 取值具体可见{@link OH_NativeBuffer_ColorGamut}枚举值。
     */
    GET_COLOR_GAMUT,
    /**
     * 设置本地窗口缓冲区变换，
     * 函数中的可变参数是
     * [输入] int32_t transform， 取值具体可见{@link OH_NativeBuffer_TransformType}枚举值。
     */
    SET_TRANSFORM,
    /**
     * 获取本地窗口缓冲区变换，
     * 函数中的可变参数是
     * [输出] int32_t *transform， 取值具体可见{@link OH_NativeBuffer_TransformType}枚举值。
     */
    GET_TRANSFORM,
    /**
     * 设置本地窗口缓冲区UI时间戳，
     * 函数中的可变参数是
     * [输入] uint64_t uiTimestamp。
     */
    SET_UI_TIMESTAMP,
    /**
     * 获取内存队列大小,
     * 函数中的可变参数是
     * [输出] int32_t *size.
     * @since 12
     */
    GET_BUFFERQUEUE_SIZE,
    /**
     * 设置本地窗口内容来源,
     * 函数中的可变参数是
     * [输入] int32_t sourceType， 取值具体可见{@link OHSurfaceSource}枚举值。
     * @since 12
     */
    SET_SOURCE_TYPE,
    /**
     * 获取本地窗口内容来源,
     * 函数中的可变参数是
     * [输出] int32_t *sourceType， 取值具体可见{@link OHSurfaceSource}枚举值。
     * @since 12
     */
    GET_SOURCE_TYPE,
    /**
     * 设置本地窗口应用框架名称,
     * 函数中的可变参数是
     * [输入] char* frameworkType， 最大支持64字节。
     * @since 12
     */
    SET_APP_FRAMEWORK_TYPE,
    /**
     * 获取本地窗口应用框架名称,
     * 函数中的可变参数是
     * [输出] char* frameworkType。
     * @since 12
     */
    GET_APP_FRAMEWORK_TYPE,
    /**
     * 设置HDR白点亮度,
     * 函数中的可变参数是
     * [输入] float brightness. 取值范围为[0.0f, 1.0f].
     * @since 12
     */
    SET_HDR_WHITE_POINT_BRIGHTNESS,
    /**
     * 设置SDR白点亮度,
     * 函数中的可变参数是
     * [输入] float brightness. 取值范围为[0.0f, 1.0f].
     * @since 12
     */
    SET_SDR_WHITE_POINT_BRIGHTNESS,
    /**
     * 设置本地窗口缓冲区期望上屏时间的时间戳。
     * 当且仅当RenderService为本地窗口的消费者时，该时间戳生效。
     * 本操作执行后需要配合调用{@link OH_NativeWindow_NativeWindowFlushBuffer}生效。
     * 生产者下一次放入队列的buffer，达到该期望上屏时间后，才会被RenderService消费并上屏。
     * 如果buffer队列中存在多个生产者放入的buffer，都设置了desiredPresentTimestamp并已达到期望上屏时间，则较早入队的buffer将被消费者丢弃回队列。
     * 如果期望上屏时间大于消费者提供的时间 1 秒以上，则该期望上屏时间戳将被忽略。
     * 函数中的可变参数是
     * [输入] int64_t desiredPresentTimestamp，取值范围大于0，应由std::chrono::steady_clock标准库时钟生成，且单位为纳秒.
     * @since 13
     */
    SET_DESIRED_PRESENT_TIMESTAMP = 24,
} NativeWindowOperation;

/**
 * @brief 缩放模式 Scaling Mode。
 * @since 9
 * @deprecated 从API version 10开始废弃。
 * @useinstead {@link OHScalingModeV2}
 */
typedef enum {
    /**
     * 在接收到窗口大小的缓冲区之前，不可以更新窗口内容。
     */
    OH_SCALING_MODE_FREEZE = 0,
    /**
     * 缓冲区在二维中缩放以匹配窗口大小。
     */
    OH_SCALING_MODE_SCALE_TO_WINDOW,
    /**
     * 缓冲区被统一缩放，使得缓冲区的较小尺寸与窗口大小匹配。
     */
    OH_SCALING_MODE_SCALE_CROP,
    /**
     * 窗口被裁剪为缓冲区裁剪矩形的大小，裁剪矩形之外的像素被视为完全透明。
     */
    OH_SCALING_MODE_NO_SCALE_CROP,
} OHScalingMode;

/**
 * @brief 渲染缩放模式枚举。
 * @since 12
 */
typedef enum OHScalingModeV2 {
    /**
     * 冻结窗口，在接收到和窗口大小相等的缓冲区之前，窗口内容不进行更新。
     */
    OH_SCALING_MODE_FREEZE_V2 = 0,
    /**
     * 缓冲区进行拉伸缩放以匹配窗口大小。
     */
    OH_SCALING_MODE_SCALE_TO_WINDOW_V2,
    /**
     * 缓冲区按原比例缩放，使得缓冲区的较小边与窗口匹配，
     * 较长边超出窗口部分被视为透明。
     */
    OH_SCALING_MODE_SCALE_CROP_V2,
    /**
     * 按窗口大小将缓冲区裁剪，裁剪矩形之外的像素被视为完全透明。
     */
    OH_SCALING_MODE_NO_SCALE_CROP_V2,
    /**
     * 缓冲区按原比例缩放。优先显示所有缓冲区内容。
     * 如果比例与窗口比例不同，用背景颜色填充窗口的未填充区域。
     * 开发板和模拟器不支持该模式。
     */
    OH_SCALING_MODE_SCALE_FIT_V2,
} OHScalingModeV2;

/**
 * @brief 枚举HDR元数据关键字。
 * @since 9
 * @deprecated 从API version 10开始废弃，不再提供替代接口。
 */
typedef enum {
    OH_METAKEY_RED_PRIMARY_X = 0, /**< 红基色X坐标。 */
    OH_METAKEY_RED_PRIMARY_Y = 1, /**<  红基色Y坐标。*/
    OH_METAKEY_GREEN_PRIMARY_X = 2, /**<  绿基色X坐标。 */
    OH_METAKEY_GREEN_PRIMARY_Y = 3, /**<  绿基色Y坐标。 */
    OH_METAKEY_BLUE_PRIMARY_X = 4, /**<  蓝基色X坐标。 */
    OH_METAKEY_BLUE_PRIMARY_Y = 5, /**<  蓝基色Y坐标。 */
    OH_METAKEY_WHITE_PRIMARY_X = 6, /**<  白点X坐标。 */
    OH_METAKEY_WHITE_PRIMARY_Y = 7, /**<  白点Y坐标。 */
    OH_METAKEY_MAX_LUMINANCE = 8, /**<  最大的光亮度。 */
    OH_METAKEY_MIN_LUMINANCE = 9, /**<  最小的光亮度。 */
    OH_METAKEY_MAX_CONTENT_LIGHT_LEVEL = 10, /**<  最大的内容亮度水平。 */
    OH_METAKEY_MAX_FRAME_AVERAGE_LIGHT_LEVEL = 11, /**<  最大的帧平均亮度水平。 */
    OH_METAKEY_HDR10_PLUS = 12, /**<  HDR10 Plus。 */
    OH_METAKEY_HDR_VIVID = 13, /**<  Vivid。 */
} OHHDRMetadataKey;

/**
 * @brief HDR元数据结构体定义。
 * @since 9
 * @deprecated 从API version 10开始废弃，不再提供替代接口。
 */
typedef struct {
    OHHDRMetadataKey key; /**<  HDR元数据关键字 */
    float value; /**<  关键字对应的值 */
} OHHDRMetaData;

/**
 * @brief 扩展数据句柄结构体定义。
 * @since 9
 * @deprecated 从API version 10开始废弃，不再提供替代接口。
 */
typedef struct {
    int32_t fd; /**<  句柄 Fd， -1代表不支持 */
    uint32_t reserveInts; /**<  Reserve数组的个数 */
    int32_t reserve[0]; /**<  Reserve数组 */
} OHExtDataHandle;

/**
 * @brief 本地窗口内容来源类型枚举。
 * @since 12
 */
typedef enum OHSurfaceSource {
    /*
     * 窗口内容默认来源。
     */
    OH_SURFACE_SOURCE_DEFAULT = 0,
    /*
     * 窗口内容来自于UI。
     */
    OH_SURFACE_SOURCE_UI,
    /*
     * 窗口内容来自于游戏。
     */
    OH_SURFACE_SOURCE_GAME,
    /*
     * 窗口内容来自于相机。
     */
    OH_SURFACE_SOURCE_CAMERA,
    /*
     * 窗口内容来自于视频。
     */
    OH_SURFACE_SOURCE_VIDEO,
} OHSurfaceSource;

/**
 * @brief 创建OHNativeWindow实例，每次调用都会产生一个新的OHNativeWindow实例。
 * 说明：此接口不可用，可通过<b>OH_NativeImage_AcquireNativeWindow</b>创建，或通过XComponent创建。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param pSurface 一个指向生产者ProduceSurface的指针，类型为sptr<OHOS::Surface>。
 * @return 返回一个指针，指向OHNativeWindow的结构体实例。
 * @since 8
 * @version 1.0
 * @deprecated 从API version 12开始废弃，不再提供替代接口。
 */
OHNativeWindow* OH_NativeWindow_CreateNativeWindow(void* pSurface);

/**
 * @brief 将OHNativeWindow对象的引用计数减1，当引用计数为0的时候，该OHNativeWindow对象会被析构掉。\n
 * 本接口为非线程安全类型接口。\n
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param window 一个OHNativeWindow的结构体实例的指针。
 * @since 8
 * @version 1.0
 */
void OH_NativeWindow_DestroyNativeWindow(OHNativeWindow* window);

/**
 * @brief 创建OHNativeWindowBuffer实例，每次调用都会产生一个新的OHNativeWindowBuffer实例。
 * 说明：此接口不可用，使用<b>OH_NativeWindow_CreateNativeWindowBufferFromNativeBuffer</b>替代。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param pSurfaceBuffer 一个指向生产者buffer的指针，类型为sptr<OHOS::SurfaceBuffer>。
 * @return 返回一个指针，指向OHNativeWindowBuffer的结构体实例。
 * @since 8
 * @version 1.0
 * @deprecated 从API version 12开始废弃。
 * @useinstead {@link OH_NativeWindow_CreateNativeWindowBufferFromNativeBuffer}
 */
OHNativeWindowBuffer* OH_NativeWindow_CreateNativeWindowBufferFromSurfaceBuffer(void* pSurfaceBuffer);

/**
 * @brief 创建OHNativeWindowBuffer实例，每次调用都会产生一个新的OHNativeWindowBuffer实例。\n
 * 本接口需要与{@link OH_NativeWindow_DestroyNativeWindowBuffer}接口配合使用，否则会存在内存泄露。\n
 * 本接口为非线程安全类型接口。\n
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param nativeBuffer 一个指向OH_NativeBuffer的指针。
 * @return 返回一个指针，指向OHNativeWindowBuffer的结构体实例。
 * @since 11
 * @version 1.0
 */
OHNativeWindowBuffer* OH_NativeWindow_CreateNativeWindowBufferFromNativeBuffer(OH_NativeBuffer* nativeBuffer);

/**
 * @brief 将OHNativeWindowBuffer对象的引用计数减1，当引用计数为0的时候，该OHNativeWindowBuffer对象会被析构掉。\n
 * 本接口为非线程安全类型接口。\n
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param buffer 一个OHNativeWindowBuffer的结构体实例的指针。
 * @since 8
 * @version 1.0
 */
void OH_NativeWindow_DestroyNativeWindowBuffer(OHNativeWindowBuffer* buffer);

/**
 * @brief 通过OHNativeWindow对象申请一块OHNativeWindowBuffer，用以内容生产。\n
 * 在调用本接口前，需要通过{@link SET_BUFFER_GEOMETRY}对<b>OHNativeWindow</b>设置宽高。\n
 * 本接口需要与{@link OH_NativeWindow_NativeWindowFlushBuffer}接口配合使用，否则内存会耗尽。\n
 * 当fenceFd使用完，用户需要将其close。\n
 * 本接口为非线程安全类型接口。\n
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param window 一个OHNativeWindow的结构体实例的指针。
 * @param buffer 一个OHNativeWindowBuffer的结构体实例的二级指针。
 * @param fenceFd 一个文件描述符句柄。
 * @return 返回值为0表示执行成功，其他返回值可参考{@link OHNativeErrorCode}。
 * @since 8
 * @version 1.0
 */
int32_t OH_NativeWindow_NativeWindowRequestBuffer(OHNativeWindow *window,
    OHNativeWindowBuffer **buffer, int *fenceFd);

/**
 * @brief 通过OHNativeWindow将生产好内容的OHNativeWindowBuffer放回到Buffer队列中，用以内容消费。\n
 * 系统会将fenFd关闭，无需用户close。\n
 * 本接口为非线程安全类型接口。\n
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param window 一个OHNativeWindow的结构体实例的指针。
 * @param buffer 一个OHNativeWindowBuffer的结构体实例的指针。
 * @param fenceFd 一个文件描述符句柄，用以同步时序。
 * @param region 表示一块脏区域，该区域有内容更新。
 * @return 返回值为0表示执行成功，其他返回值可参考{@link OHNativeErrorCode}。
 * @since 8
 * @version 1.0
 */
int32_t OH_NativeWindow_NativeWindowFlushBuffer(OHNativeWindow *window, OHNativeWindowBuffer *buffer,
    int fenceFd, Region region);

/**
 * @brief 从OHNativeWindow获取上次送回到buffer队列中的OHNativeWindowBuffer。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param window 一个OHNativeWindow的结构体实例的指针。
 * @param buffer 一个OHNativeWindowBuffer结构体指针的指针。
 * @param fenceFd 一个文件描述符的指针。
 * @param matrix 表示检索到的4*4变换矩阵。
 * @return 返回值为0表示执行成功，其他返回值可参考{@link OHNativeErrorCode}。
 * @since 11
 * @version 1.0
 * @deprecated 从API version 12开始废弃。
 * @useinstead {@link OH_NativeWindow_GetLastFlushedBufferV2}
 */
int32_t OH_NativeWindow_GetLastFlushedBuffer(OHNativeWindow *window, OHNativeWindowBuffer **buffer,
    int *fenceFd, float matrix[16]);

/**
 * @brief 通过OHNativeWindow将之前申请出来的OHNativeWindowBuffer返还到Buffer队列中，供下次再申请。\n
 * 本接口为非线程安全类型接口。\n
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param window 一个OHNativeWindow的结构体实例的指针。
 * @param buffer 一个OHNativeWindowBuffer的结构体实例的指针。
 * @return 返回值为0表示执行成功，其他返回值可参考{@link OHNativeErrorCode}。
 * @since 8
 * @version 1.0
 */
int32_t OH_NativeWindow_NativeWindowAbortBuffer(OHNativeWindow *window, OHNativeWindowBuffer *buffer);

/**
 * @brief 设置/获取OHNativeWindow的属性，包括设置/获取宽高、内容格式等。\n
 * 本接口为非线程安全类型接口。\n
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param window 一个OHNativeWindow的结构体实例的指针。
 * @param code 表示操作码，详见{@link NativeWindowOperation}。
 * @param ... 可变参数，必须与操作码对应的数据类型保持一致，且入参数量严格按照操作码提示传入，否则会存在未定义行为。
 * @return 返回值为0表示执行成功，其他返回值可参考{@link OHNativeErrorCode}。
 * @since 8
 * @version 1.0
 */
int32_t OH_NativeWindow_NativeWindowHandleOpt(OHNativeWindow *window, int code, ...);

/**
 * @brief 通过OHNativeWindowBuffer获取该buffer的BufferHandle指针。\n
 * 本接口为非线程安全类型接口。\n
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param buffer 一个OHNativeWindowBuffer的结构体实例的指针。
 * @return BufferHandle 返回一个指针，指向BufferHandle的结构体实例。
 * @since 8
 * @version 1.0
 */
BufferHandle *OH_NativeWindow_GetBufferHandleFromNative(OHNativeWindowBuffer *buffer);

/**
 * @brief 增加一个NativeObject的引用计数。\n
 * 本接口需要与{@link OH_NativeWindow_NativeObjectUnreference}接口配合使用，否则会存在内存泄露。\n
 * 本接口为非线程安全类型接口。\n
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param obj 一个OHNativeWindow或者OHNativeWindowBuffer的结构体实例的指针。
 * @return 返回值为0表示执行成功，其他返回值可参考{@link OHNativeErrorCode}。
 * @since 8
 * @version 1.0
 */
int32_t OH_NativeWindow_NativeObjectReference(void *obj);

/**
 * @brief 减少一个NativeObject的引用计数，当引用计数减少为0时，该NativeObject将被析构掉。\n
 * 本接口为非线程安全类型接口。\n
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param obj 一个OHNativeWindow或者OHNativeWindowBuffer的结构体实例的指针。
 * @return 返回值为0表示执行成功，其他返回值可参考{@link OHNativeErrorCode}。
 * @since 8
 * @version 1.0
 */
int32_t OH_NativeWindow_NativeObjectUnreference(void *obj);

/**
 * @brief 获取NativeObject的MagicId。\n
 * 本接口为非线程安全类型接口。\n
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param obj 一个OHNativeWindow或者OHNativeWindowBuffer的结构体实例的指针。
 * @return MagicId 返回值为魔鬼数字，每个NativeObject唯一。
 * @since 8
 * @version 1.0
 */
int32_t OH_NativeWindow_GetNativeObjectMagic(void *obj);

/**
 * @brief 设置OHNativeWindow的ScalingMode。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param window 一个OHNativeWindow的结构体实例的指针。
 * @param sequence 生产缓冲区的序列。
 * @param scalingMode 枚举值OHScalingMode。
 * @return 返回值为0表示执行成功。
 * @since 9
 * @version 1.0
 * @deprecated 从API version 10开始废弃。
 * @useinstead {@link OH_NativeWindow_NativeWindowSetScalingModeV2}
 */
int32_t OH_NativeWindow_NativeWindowSetScalingMode(OHNativeWindow *window, uint32_t sequence,
                                                   OHScalingMode scalingMode);

/**
 * @brief 设置OHNativeWindow的元数据。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param window 一个OHNativeWindow的结构体实例的指针。
 * @param sequence 生产缓冲区的序列。
 * @param size OHHDRMetaData数组的大小。
 * @param metaDate 指向OHHDRMetaData数组的指针。
 * @return 返回值为0表示执行成功。
 * @since 9
 * @version 1.0
 * @deprecated 从API version 10开始废弃，不再提供替代接口。
 */
int32_t OH_NativeWindow_NativeWindowSetMetaData(OHNativeWindow *window, uint32_t sequence, int32_t size,
                                                const OHHDRMetaData *metaData);

/**
 * @brief 设置OHNativeWindow的元数据集。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param window 一个OHNativeWindow的结构体实例的指针。
 * @param sequence 生产缓冲区的序列。
 * @param key 枚举值OHHDRMetadataKey。
 * @param size uint8_t向量的大小。
 * @param metaDate 指向uint8_t向量的指针。
 * @return 返回值为0表示执行成功。
 * @since 9
 * @version 1.0
 * @deprecated 从API version 10开始废弃，不再提供替代接口。
 */
int32_t OH_NativeWindow_NativeWindowSetMetaDataSet(OHNativeWindow *window, uint32_t sequence, OHHDRMetadataKey key,
                                                   int32_t size, const uint8_t *metaData);

/**
 * @brief 设置OHNativeWindow的TunnelHandle。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param window 一个OHNativeWindow的结构体实例的指针。
 * @param handle 指向OHExtDataHandle的指针。
 * @return 返回值为0表示执行成功。
 * @since 9
 * @version 1.0
 * @deprecated 从API version 10开始废弃，不再提供替代接口。
 */
int32_t OH_NativeWindow_NativeWindowSetTunnelHandle(OHNativeWindow *window, const OHExtDataHandle *handle);

/**
 * @brief 将OHNativeWindowBuffer添加进OHNativeWindow中。\n
 * 本接口需要与{@link OH_NativeWindow_NativeWindowDetachBuffer}接口配合使用，否则会存在内存管理混乱问题。\n
 * 本接口为非线程安全类型接口。\n
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param window 一个OHNativeWindow的结构体实例的指针。
 * @param buffer 一个OHNativeWindowBuffer的结构体实例的指针。
 * @return 返回值为0表示执行成功，其他返回值可参考{@link OHNativeErrorCode}。
 * @since 12
 * @version 1.0
 */
int32_t OH_NativeWindow_NativeWindowAttachBuffer(OHNativeWindow *window, OHNativeWindowBuffer *buffer);

/**
 * @brief 将OHNativeWindowBuffer从OHNativeWindow中分离。\n
 * 本接口为非线程安全类型接口。\n
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param window 一个OHNativeWindow的结构体实例的指针。
 * @param buffer 一个OHNativeWindowBuffer的结构体实例的指针。
 * @return 返回值为0表示执行成功，其他返回值可参考{@link OHNativeErrorCode}。
 * @since 12
 * @version 1.0
 */
int32_t OH_NativeWindow_NativeWindowDetachBuffer(OHNativeWindow *window, OHNativeWindowBuffer *buffer);

/**
 * @brief 通过OHNativeWindow获取对应的surfaceId。\n
 * 本接口为非线程安全类型接口。\n
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param window 一个OHNativeWindow的结构体实例的指针。
 * @param surfaceId 一个surface对应ID的指针。
 * @return 返回值为0表示执行成功，其他返回值可参考{@link OHNativeErrorCode}。
 * @since 12
 * @version 1.0
 */
int32_t OH_NativeWindow_GetSurfaceId(OHNativeWindow *window, uint64_t *surfaceId);

/**
 * @brief 通过surfaceId创建对应的OHNativeWindow。\n
 * 本接口需要与{@link OH_NativeWindow_DestroyNativeWindow}接口配合使用，否则会存在内存泄露。\n
 * 如果存在并发释放<b>OHNativeWindow<\b>的情况，需要通过{@link OH_NativeWindow_NativeObjectReference}和
 * {@link OH_NativeWindow_NativeObjectUnreference}对<b>OHNativeWindow<\b>进行引用计数加一和减一。\n
 * 通过surfaceId获取的surface需要是在本进程中创建的，不能跨进程获取surface。\n
 * 本接口为非线程安全类型接口。\n
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param surfaceId 一个surface对应的ID。
 * @param window 一个OHNativeWindow的结构体实例的二级指针。
 * @return 返回值为0表示执行成功，其他返回值可参考{@link OHNativeErrorCode}。
 * @since 12
 * @version 1.0
 */
int32_t OH_NativeWindow_CreateNativeWindowFromSurfaceId(uint64_t surfaceId, OHNativeWindow **window);

/**
 * @brief 设置OHNativeWindow的渲染缩放模式。\n
 * 本接口为非线程安全类型接口。\n
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param window 一个OHNativeWindow的结构体实例的指针。
 * @param scalingMode 一个OHScalingModeV2类型的枚举值。
 * @return 返回值为0表示执行成功，其他返回值可参考{@link OHNativeErrorCode}。
 * @since 12
 * @version 1.0
 */
int32_t OH_NativeWindow_NativeWindowSetScalingModeV2(OHNativeWindow* window, OHScalingModeV2 scalingMode);

/**
 * @brief 从OHNativeWindow获取上次送回到buffer队列中的OHNativeWindowBuffer,
 * 与OH_NativeWindow_GetLastFlushedBuffer的差异在于matrix不同。\n
 * 本接口需要与{@link OH_NativeWindow_NativeObjectUnreference}接口配合使用，否则会存在内存泄露。\n
 * 本接口为非线程安全类型接口。\n
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param window 一个OHNativeWindow的结构体实例的指针。
 * @param buffer 一个OHNativeWindowBuffer结构体指针的指针。
 * @param fenceFd 一个文件描述符的指针。
 * @param matrix 表示检索到的4*4变换矩阵。
 * @return 返回值为0表示执行成功，其他返回值可参考{@link OHNativeErrorCode}。
 * @since 12
 * @version 1.0
 */
int32_t OH_NativeWindow_GetLastFlushedBufferV2(OHNativeWindow *window, OHNativeWindowBuffer **buffer,
    int *fenceFd, float matrix[16]);

/**
 * @brief 提前缓存一帧buffer，且缓存的这一帧延迟一帧上屏显示，以此抵消后续一次超长帧丢帧。\n
 * 本接口为非线程安全类型接口。\n
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param window 一个{@link OHNativeWindow}的结构体实例的指针。
 * @since 12
 * @version 1.0
 */
void OH_NativeWindow_SetBufferHold(OHNativeWindow *window);

/**
 * @brief 将窗口对象写入IPC序列化对象中。\n
 * 本接口为非线程安全类型接口。\n
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param window 一个指向{@link OHNativeWindow}的结构体实例的指针。
 * @param parcel 一个指向{@link OHIPCParcel}的结构体实例的指针。
 * @return 返回值为0表示执行成功，其他返回值可参考{@link OHNativeErrorCode}。
 * @since 12
 * @version 1.0
 */
int32_t OH_NativeWindow_WriteToParcel(OHNativeWindow *window, OHIPCParcel *parcel);

/**
 * @brief 从IPC序列化对象中读取窗口对象。\n
 * 本接口为非线程安全类型接口。\n
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param parcel 一个指向{@link OHIPCParcel}的结构体实例的指针。
 * @param window 一个指向{@link OHNativeWindow}的结构体实例的二级指针。
 * @return 返回值为0表示执行成功，其他返回值可参考{@link OHNativeErrorCode}。
 * @since 12
 * @version 1.0
 */
int32_t OH_NativeWindow_ReadFromParcel(OHIPCParcel *parcel, OHNativeWindow **window);

/**
 * @brief 为OHNativeWindow设置颜色空间属性。\n
 * 本接口为非线程安全类型接口。\n
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param window 一个指向{@link OHNativeWindow}的结构体实例的指针。
 * @param colorSpace 为OHNativeWindow设置的颜色空间，其值从{@link OH_NativeBuffer_ColorSpace}获取。
 * @return 返回值为0表示执行成功，其他返回值可参考{@link OHNativeErrorCode}。
 * @since 12
 * @version 1.0
 */
int32_t OH_NativeWindow_SetColorSpace(OHNativeWindow *window, OH_NativeBuffer_ColorSpace colorSpace);

/**
 * @brief 获取OHNativeWindow颜色空间属性。\n
 * 本接口为非线程安全类型接口。\n
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param window 一个指向{@link OHNativeWindow}的结构体实例的指针。
 * @param colorSpace 为OHNativeWindow设置的颜色空间，其值从{@link OH_NativeBuffer_ColorSpace}获取。
 * @return 返回值为0表示执行成功，其他返回值可参考{@link OHNativeErrorCode}。
 * @since 12
 * @version 1.0
 */
int32_t OH_NativeWindow_GetColorSpace(OHNativeWindow *window, OH_NativeBuffer_ColorSpace *colorSpace);

/**
 * @brief 为OHNativeWindow设置元数据属性值。\n
 * 本接口为非线程安全类型接口。\n
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param window 一个指向{@link OHNativeWindow}的结构体实例的指针。
 * @param metadataKey OHNativeWindow的元数据类型，其值从{@link OH_NativeBuffer_MetadataKey}获取。
 * @param size uint8_t向量的大小，其取值范围见{@link OH_NativeBuffer_MetadataKey}。
 * @param metaDate 指向uint8_t向量的指针。
 * @return 返回值为0表示执行成功，其他返回值可参考{@link OHNativeErrorCode}。
 * @since 12
 * @version 1.0
 */
int32_t OH_NativeWindow_SetMetadataValue(OHNativeWindow *window, OH_NativeBuffer_MetadataKey metadataKey,
    int32_t size, uint8_t *metaData);

/**
 * @brief 获取OHNativeWindow元数据属性值。\n
 * 本接口为非线程安全类型接口。\n
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeWindow
 * @param window 一个指向{@link OHNativeWindow}的结构体实例的指针。
 * @param metadataKey OHNativeWindow的元数据类型，其值从{@link OH_NativeBuffer_MetadataKey}获取。
 * @param size uint8_t向量的大小，其取值范围见{@link OH_NativeBuffer_MetadataKey}。
 * @param metaDate 指向uint8_t向量的二级指针。
 * @return 返回值为0表示执行成功，其他返回值可参考{@link OHNativeErrorCode}。
 * @since 12
 * @version 1.0
 */
int32_t OH_NativeWindow_GetMetadataValue(OHNativeWindow *window, OH_NativeBuffer_MetadataKey metadataKey,
    int32_t *size, uint8_t **metaData);
#ifdef __cplusplus
}
#endif

/** @} */
#endif