/*
 * Copyright (C) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup image
 * @{
 *
 * @brief 提供用于图像数据编码的native接口。
 *
 * image模块的图像数据编码部分。\n
 * 可用于将像素数据信息编码到缓冲区或文件中。
 *
 * @since 11
 * @version 4.1
 */

/**
 * @file image_packer_mdk.h
 *
 * @brief 声明用于将图像编码到缓冲区或文件的api。
 *
 * 可用于将像素数据编码到目标缓冲区或文件中。\n
 *
 * 编码过程如下：\n
 * 通过OH_ImagePacker_Create方法创建编码器实例对象。\n
 * 然后通过OH_ImagePacker_InitNative将编码器实例对象转换为编码器原生实例对象。\n
 * 接下来用OH_ImagePacker_PackToData或者OH_ImagePacker_PackToFile将源以特定的编码选项编码进目标区域。\n
 * 最后通过OH_ImagePacker_Release释放编码器实例对象。\n
 *
 * @library libimage_packer_ndk.z.so
 * @syscap SystemCapability.Multimedia.Image
 * @since 11
 * @version 4.1
 */

#ifndef INTERFACES_KITS_NATIVE_INCLUDE_IMAGE_PACKER_MDK_H_
#define INTERFACES_KITS_NATIVE_INCLUDE_IMAGE_PACKER_MDK_H_
#include "napi/native_api.h"
#include "image_mdk_common.h"

#ifdef __cplusplus
extern "C" {
#endif

struct ImagePacker_Native_;

/**
 * @brief 为编码器方法定义native层编码器对象。
 *
 * @since 11
 * @version 4.1
 */
typedef struct ImagePacker_Native_ ImagePacker_Native;

/**
 * @brief 定义图像编码选项信息。
 *
 * @since 11
 * @version 4.1
 */
struct ImagePacker_Opts_ {
    /** 编码格式 */
    const char* format;
    /** 编码质量 */
    int quality;
};

/**
 * @brief 定义图像编码选项的别名。
 *
 * @since 11
 * @version 4.1
 */
typedef struct ImagePacker_Opts_ ImagePacker_Opts;

/**
 * @brief 获取JavaScript native层API ImagePacker对象。
 *
 * @param env 表明JNI环境的指针。
 * @param res 表明JavaScript native层API ImagePacker对象的指针。
 * @return 如果操作成功则返回{@link IRNdkErrCode}IMAGE_RESULT_SUCCESS；如果参数无效则返回IMAGE_RESULT_INVALID_PARAMETER。
 *
 * @Syscap SystemCapability.Multimedia.Image
 * @since 11
 * @version 4.1
 */
int32_t OH_ImagePacker_Create(napi_env env, napi_value *res);

/**
 * @brief 从输入JavaScript native层API ImagePacker对象中，转换成ImagePacker_Native值。
 *
 * @param env 表明JNI环境的指针。
 * @param packer 表明JavaScript native层API ImagePacker对象。
 * @return 如果操作成功则返回{@link ImagePacker_Native}指针，否则返回空指针。
 * @see OH_ImagePacker_Release
 * @since 11
 * @version 4.1
 */
ImagePacker_Native* OH_ImagePacker_InitNative(napi_env env, napi_value packer);

/**
 * @brief 通过一个给定的选项ImagePacker_Opts结构体，将输入JavaScript native层API PixelMap对象或者ImageSource对象编码并输出\n
 * 到指定的缓存区outData中。
 *
 * @param native 表明指向native层{@link ImagePacker}的指针。
 * @param source 表明待编码JavaScript native层API PixelMap对象或者ImageSource对象。
 * @param opts 表明位图编码的选项，查看{@link ImagePacker_Opts}。
 * @param outData 输出的指定缓存区。
 * @param size 输出的指定缓存区大小。
 * @return 如果操作成功返回{@link IRNdkErrCode} OHOS_IMAGE_RESULT_SUCCESS；\n
  * returns 如果参数无效返回{@link IRNdkErrCode} IMAGE_RESULT_INVALID_PARAMETER；\n
  * returns 如果输出缓冲区异常返回{@link IRNdkErrCode} ERR_IMAGE_DATA_ABNORMAL；\n
  * returns 如果格式不匹配返回{@link IRNdkErrCode} ERR_IMAGE_MISMATCHED_FORMAT；\n
  * returns 如果malloc内部缓冲区错误返回{@link IRNdkErrCode} ERR_IMAGE_MALLOC_ABNORMAL；\n
  * returns 如果init编解码器内部错误返回{@link IRNdkErrCode} ERR_IMAGE_DECODE_ABNORMAL；\n
  * returns 如果编码器在编码过程中出现错误返回{@link IRNdkErrCode} ERR_IMAGE_ENCODE_FAILED。\n
 * @see OH_ImagePacker_PackToFile
 * @since 11
 * @version 4.1
 */
int32_t OH_ImagePacker_PackToData(ImagePacker_Native* native, napi_value source,
    ImagePacker_Opts* opts, uint8_t* outData, size_t* size);

/**
 * @brief 通过一个给定的选项ImagePacker_Opts结构体，将输入JavaScript native层API PixelMap对象或者ImageSource对象编码并输出\n
 * 到指定的文件中。
 *
 * @param native 表明指向native层{@link ImagePacker}的指针。
 * @param source 表明待编码JavaScript native层API PixelMap对象或者ImageSource对象。
 * @param opts 表明位图编码的选项，查看{@link ImagePacker_Opts}。
 * @param fd 输出的指定文件描述符。
 * @return 如果操作成功返回{@link IRNdkErrCode} OHOS_IMAGE_RESULT_SUCCESS；\n
  * returns 如果参数无效返回{@link IRNdkErrCode} IMAGE_RESULT_INVALID_PARAMETER；\n
  * returns 如果输出缓冲区异常返回{@link IRNdkErrCode} ERR_IMAGE_DATA_ABNORMAL；\n
  * returns 如果格式不匹配返回{@link IRNdkErrCode} ERR_IMAGE_MISMATCHED_FORMAT；\n
  * returns 如果malloc内部缓冲区错误返回{@link IRNdkErrCode} ERR_IMAGE_MALLOC_ABNORMAL；\n
  * returns 如果init编解码器内部错误返回{@link IRNdkErrCode} ERR_IMAGE_DECODE_ABNORMAL；\n
  * returns 如果编码器在编码过程中出现错误返回{@link IRNdkErrCode} ERR_IMAGE_ENCODE_FAILED。\n
 * @see OH_ImagePacker_PackToData
 * @since 11
 * @version 4.1
 */
int32_t OH_ImagePacker_PackToFile(ImagePacker_Native* native, napi_value source,
    ImagePacker_Opts* opts, int fd);


/**
 * @brief 释放native层编码器对象{@link ImagePacker_Native}。
 * 注: 此API不用于释放JavaScript原生API ImagePacker对象，它用于释放native层对象{@link ImagePacker_Native}。\n
 * 通过调用{@link OH_ImagePacker_InitNative}解析。
 *
 * @param native 表明native层{@link ImagePacker_Native}值的指针。
 * @return 如果操作成功则返回{@link IRNdkErrCode} IMAGE_RESULT_SUCCESS。
 * @see OH_ImagePacker_InitNative
 * @since 11
 * @version 4.1
 */
int32_t OH_ImagePacker_Release(ImagePacker_Native* native);
#ifdef __cplusplus
};
#endif
/** @} */
#endif // INTERFACES_KITS_NATIVE_INCLUDE_IMAGE_PACKER_MDK_H_