/*
 * Copyright (C) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Image_NativeModule
 * @{
 *
 * @brief 提供图片编解码能力。
 *
 * @since 12
 */

/**
 * @file pixelmap_native.h
 *
 * @brief 访问Pixelmap的API。
 *
 * @library libpixelmap.so
 * @Syscap SystemCapability.Multimedia.Image.Core
 * @since 12
 */

#ifndef INTERFACES_KITS_NATIVE_INCLUDE_IMAGE_PIXELMAP_NATIVE_H_
#define INTERFACES_KITS_NATIVE_INCLUDE_IMAGE_PIXELMAP_NATIVE_H_
#include "image_common.h"
#include "napi/native_api.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Pixelmap结构体类型，用于执行Pixelmap相关操作。
 *
 * @since 12
 */
struct OH_PixelmapNative;
typedef struct OH_PixelmapNative OH_PixelmapNative;

/**
 * @brief NativeBuffer结构体类型，用于执行NativeBuffer相关操作。
 *
 * @since 12
 */
struct OH_NativeBuffer;
typedef struct OH_NativeBuffer OH_NativeBuffer;

/**
 * @brief NativeColorSpaceManager结构体类型，用于执行NativeColorSpaceManager相关操作。
 *
 * @since 13
 */
typedef struct OH_NativeColorSpaceManager OH_NativeColorSpaceManager;

/**
 * @brief Pixelmap透明度类型。
 *
 * @since 12
 */
typedef enum {
    /*
    * 未知格式
    */
    PIXELMAP_ALPHA_TYPE_UNKNOWN = 0,
     /*
    * 不透明的格式
    */
    PIXELMAP_ALPHA_TYPE_OPAQUE = 1,
     /*
    * 预乘透明度格式
    */
    PIXELMAP_ALPHA_TYPE_PREMULTIPLIED = 2,
}PIXELMAP_ALPHA_TYPE;

typedef enum {
    /*
    * 未知格式
    */
    PIXEL_FORMAT_UNKNOWN = 0,
    /*
    * RGB_565格式
    */
    PIXEL_FORMAT_RGB_565 = 2,
    /*
    * RGBA_8888格式
    */
    PIXEL_FORMAT_RGBA_8888 = 3,
    /*
    * BGRA_8888格式
    */
    PIXEL_FORMAT_BGRA_8888 = 4,
    /*
    * RGB_888格式
    */
    PIXEL_FORMAT_RGB_888 = 5,
    /*
    * ALPHA_8格式
    */
    PIXEL_FORMAT_ALPHA_8 = 6,
    /*
    * RGBA_F16格式
    */
    PIXEL_FORMAT_RGBA_F16 = 7,
    /*
    * NV21格式
    */
    PIXEL_FORMAT_NV21 = 8,
    /*
    * NV12格式
    */
    PIXEL_FORMAT_NV12 = 9,
} PIXEL_FORMAT;

/**
 * @brief Pixelmap缩放时采用的缩放算法。
 *
 * @since 12
 */
typedef enum {
    /**
     * 最近邻缩放算法。
     */
    OH_PixelmapNative_AntiAliasing_NONE = 0,
    /**
     * 双线性缩放算法。
     */
    OH_PixelmapNative_AntiAliasing_LOW = 1,
    /**
     * 双线性缩放算法，同时开启mipmap。
     */
    OH_PixelmapNative_AntiAliasing_MEDIUM = 2,
    /**
     * cubic缩放算法。
     */
    OH_PixelmapNative_AntiAliasing_HIGH = 3,
} OH_PixelmapNative_AntiAliasingLevel;

/**
 * @brief Pixelmap使用的HDR相关元数据信息的关键字，用于{@link OH_PixelmapNative_SetMetadata}及{@link OH_PixelmapNative_GetMetadata}。
 *
 * @since 12
 */
typedef enum {
    /**
     * Pixelmap使用的元数据类型。
     */
    HDR_METADATA_TYPE = 0,
    /**
     * 静态元数据。
     */
    HDR_STATIC_METADATA = 1,
    /**
     * 动态元数据。
     */
    HDR_DYNAMIC_METADATA = 2,
    /**
     * Gainmap使用的元数据。
     */
    HDR_GAINMAP_METADATA = 3,
} OH_Pixelmap_HdrMetadataKey;

/**
 * @brief HDR_METADATA_TYPE关键字对应的值。
 *
 * @since 12
 */
typedef enum {
    /**
     * 无元数据内容。
     */
    HDR_METADATA_TYPE_NONE = 0,
    /**
     * 表示用于基础图的元数据。
     */
    HDR_METADATA_TYPE_BASE = 1,
    /**
     * 表示用于Gainmap图的元数据。
     */
    HDR_METADATA_TYPE_GAINMAP = 2,
    /**
     * 表示用于合成后HDR图的元数据。
     */
    HDR_METADATA_TYPE_ALTERNATE = 3,
} OH_Pixelmap_HdrMetadataType;

/**
 * @brief HDR_STATIC_METADATA关键字对应的静态元数据值。
 *
 * @since 12
 */
typedef struct OH_Pixelmap_HdrStaticMetadata {
    /**
     * 归一化后显示设备三基色的X坐标，数组的长度为3，以0.00002为单位，范围[0.0, 1.0]。
     */
    float displayPrimariesX[3];
    /**
     * 归一化后显示设备三基色的Y坐标，数组的长度为3，以0.00002为单位，范围[0.0, 1.0]。
     */
    float displayPrimariesY[3];
    /**
     * 归一化后白点值的X坐标，以0.00002为单位，范围[0.0, 1.0]。
     */
    float whitePointX;
    /**
     * 归一化后白点值的Y坐标，以0.00002为单位，范围[0.0, 1.0]。
     */
    float whitePointY;
    /**
     * 图像主监视器最大亮度。以1为单位，最大值为65535。
     */
    float maxLuminance;
    /**
     * 图像主监视器最小亮度。以0.0001为单位，最大值6.55535。
     */
    float minLuminance;
    /**
     * 显示内容的最大亮度。以1为单位，最大值为65535。
     */
    float maxContentLightLevel;
    /**
     * 显示内容的最大平均亮度，以1为单位，最大值为65535。
     */
    float maxFrameAverageLightLevel;
} OH_Pixelmap_HdrStaticMetadata;

/**
 * @brief DR_DYNAMIC_METADATA关键字对应的动态元数据值。
 *
 * @since 12
 */
typedef struct OH_Pixelmap_HdrDynamicMetadata {
    /**
     * 动态元数据的值。
     */
    uint8_t* data;
    /**
     * 动态元数据值的长度。
     */
    uint32_t length;
} OH_Pixelmap_HdrDynamicMetadata;

/**
 * @brief HDR_GAINMAP_METADATA关键字对应的gainmap相关元数据值，参考ISO 21496-1。
 *
 * @since 12
 */
typedef struct OH_Pixelmap_HdrGainmapMetadata {
    /**
     * 元数据编写器使用的版本。
     */
    uint16_t writerVersion;
    /**
     * 元数据解析需要理解的最小版本。
     */
    uint16_t miniVersion;
    /**
     * Gainmap的颜色通道数，值为3时RGB通道的元数据值不同，值为1时各通道元数据值相同。
     */
    uint8_t gainmapChannelNum;
    /**
     * 是否使用基础图的色彩空间，参考ISO 21496-1。
     */
    bool useBaseColorFlag;
    /**
     * 基础图提亮比，参考ISO 21496-1。
     */
    float baseHeadroom;
    /**
     * 提取的可选择图像提亮比，参考ISO 21496-1。
     */
    float alternateHeadroom;
    /**
     * 增强图像的最大值，参考ISO 21496-1。
     */
    float gainmapMax[3];
    /**
     * 增强图像的最小值，参考ISO 21496-1。
     */
    float gainmapMin[3];
    /**
     * gamma值，参考ISO 21496-1。
     */
    float gamma[3];
    /**
     * 基础图的偏移，参考ISO 21496-1。
     */
    float baselineOffset[3];
    /**
     * 提取的可选择图像偏移量，参考ISO 21496-1。
     */
    float alternateOffset[3];
} OH_Pixelmap_HdrGainmapMetadata;

/**
 * @brief Pixelmap使用的HDR元数据值，和OH_Pixelmap_HdrMetadataKey关键字相对应。用于{@link OH_PixelmapNative_SetMetadata}及{@link OH_PixelmapNative_GetMetadata}，有相应{@link OH_Pixelmap_HdrMetadataKey}关键字作为入参时，设置或获取到本结构体中相对应的元数据类型的值。
 *
 * @since 12
 */
typedef struct OH_Pixelmap_HdrMetadataValue {
    /**
     * HDR_METADATA_TYPE关键字对应的具体值。
     */
    OH_Pixelmap_HdrMetadataType type;
    /**
     * HDR_STATIC_METADATA关键字对应的具体值。
     */
    OH_Pixelmap_HdrStaticMetadata staticMetadata;
    /**
     * HDR_DYNAMIC_METADATA关键字对应的具体值。
     */
    OH_Pixelmap_HdrDynamicMetadata dynamicMetadata;
    /**
     * HDR_GAINMAP_METADATA关键字对应的具体值。
     */
    OH_Pixelmap_HdrGainmapMetadata gainmapMetadata;
} OH_Pixelmap_HdrMetadataValue;

/**
 * @brief 初始化参数结构体。
 *
 * @since 12
 */
struct OH_Pixelmap_InitializationOptions;
typedef struct OH_Pixelmap_InitializationOptions OH_Pixelmap_InitializationOptions;

/**
 * @brief 创建OH_Pixelmap_InitializationOptions指针。
 *
 * @param options 被创建的OH_Pixelmap_InitializationOptions指针。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_PixelmapInitializationOptions_Create(OH_Pixelmap_InitializationOptions **options);

/**
 * @brief 获取图片宽。
 *
 * @param options 被操作的OH_Pixelmap_InitializationOptions指针。
 * @param width 图片的宽，单位：像素。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_PixelmapInitializationOptions_GetWidth(OH_Pixelmap_InitializationOptions *options,
    uint32_t *width);

/**
 * @brief 设置图片宽。
 *
 * @param options 被操作的OH_Pixelmap_InitializationOptions指针。
 * @param width 图片的宽，单位：像素。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_PixelmapInitializationOptions_SetWidth(OH_Pixelmap_InitializationOptions *options,
    uint32_t width);

/**
 * @brief 获取图片高。
 *
 * @param options 被操作的OH_Pixelmap_InitializationOptions指针。
 * @param height 图片的高，单位：像素。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_PixelmapInitializationOptions_GetHeight(OH_Pixelmap_InitializationOptions *options,
    uint32_t *height);

/**
 * @brief 设置图片高。
 *
 * @param options 被操作的OH_Pixelmap_InitializationOptions指针。
 * @param height 图片的高，单位：像素。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_PixelmapInitializationOptions_SetHeight(OH_Pixelmap_InitializationOptions *options,
    uint32_t height);

/**
 * @brief 获取像素格式。
 *
 * @param options 被操作的OH_Pixelmap_InitializationOptions指针。
 * @param pixelFormat 像素格式{@link PIXEL_FORMAT}。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_PixelmapInitializationOptions_GetPixelFormat(OH_Pixelmap_InitializationOptions *options,
    int32_t *pixelFormat);

/**
 * @brief 设置像素格式。
 *
 * @param options 被操作的OH_Pixelmap_InitializationOptions指针。
 * @param pixelFormat 像素格式{@link PIXEL_FORMAT}。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_PixelmapInitializationOptions_SetPixelFormat(OH_Pixelmap_InitializationOptions *options,
    int32_t pixelFormat);

/**
 * @brief 获取源像素格式。
 * @param options 被操作的OH_Pixelmap_InitializationOptions指针。
 * @param srcpixelFormat 像素格式{@link PIXEL_FORMAT}。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_PixelmapInitializationOptions_GetSrcPixelFormat(OH_Pixelmap_InitializationOptions *options,
    int32_t *srcpixelFormat);

/**
 * @brief 设置源像素格式。
 *
 * @param options 被操作的OH_Pixelmap_InitializationOptions指针。
 * @param srcpixelFormat 源像素格式{@link PIXEL_FORMAT}。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_PixelmapInitializationOptions_SetSrcPixelFormat(OH_Pixelmap_InitializationOptions *options,
    int32_t srcpixelFormat);

/**
 * @brief 获取行跨距。跨距， 图像每行占用的真实内存大小，单位为字节。跨距 = width * 单位像素字节数 + padding，padding为每行为内存对齐做的填充区域。
 *
 * @param options 被操作的OH_Pixelmap_InitializationOptions指针。
 * @param rowStride 跨距，单位：字节。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 如果options被释放返回 IMAGE_UNKNOWN_ERROR； 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_PixelmapInitializationOptions_GetRowStride(OH_Pixelmap_InitializationOptions *options,
    int32_t *rowStride);

/**
 * @brief 设置图像跨距。跨距， 图像每行占用的真实内存大小，单位为字节。跨距 = width * 单位像素字节数 + padding，padding为每行为内存对齐做的填充区域。
 *
 * @param options 被操作的OH_Pixelmap_InitializationOptions指针。
 * @param rowStride 跨距，单位：字节。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 如果options被释放返回 IMAGE_UNKNOWN_ERROR； 具体请参考 {@link Image_ErrorCode}。
 *
 * @since 12
 */
Image_ErrorCode OH_PixelmapInitializationOptions_SetRowStride(OH_Pixelmap_InitializationOptions *options,
    int32_t rowStride);

/**
 * @brief 获取透明度类型。
 *
 * @param options 被操作的OH_Pixelmap_InitializationOptions指针。
 * @param alphaType 透明度类型{@link PIXELMAP_ALPHA_TYPE}。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_PixelmapInitializationOptions_GetAlphaType(OH_Pixelmap_InitializationOptions *options,
    int32_t *alphaType);

/**
 * @brief 设置透明度类型。
 *
 * @param options 被操作的OH_Pixelmap_InitializationOptions指针。
 * @param alphaType 透明度类型{@link PIXELMAP_ALPHA_TYPE}。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_PixelmapInitializationOptions_SetAlphaType(OH_Pixelmap_InitializationOptions *options,
    int32_t alphaType);

/**
 * @brief 释放OH_Pixelmap_InitializationOptions指针。
 *
 * @param options 被释放的OH_Pixelmap_InitializationOptions指针。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_PixelmapInitializationOptions_Release(OH_Pixelmap_InitializationOptions *options);

/**
 * @brief 图像像素信息结构体。
 *
 * @since 12
 */
struct OH_Pixelmap_ImageInfo;
typedef struct OH_Pixelmap_ImageInfo OH_Pixelmap_ImageInfo;

/**
 * @brief 创建OH_Pixelmap_ImageInfo指针。
 *
 * @param info 被创建的OH_Pixelmap_ImageInfo指针。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_PixelmapImageInfo_Create(OH_Pixelmap_ImageInfo **info);

/**
 * @brief 获取图片宽。
 *
 * @param info 被操作的OH_Pixelmap_ImageInfo指针。
 * @param width 图片宽，单位：像素。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_PixelmapImageInfo_GetWidth(OH_Pixelmap_ImageInfo *info, uint32_t *width);

/**
 * @brief 获取图片高。
 *
 * @param info 被操作的OH_Pixelmap_ImageInfo指针。
 * @param height 图片高，单位：像素。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_PixelmapImageInfo_GetHeight(OH_Pixelmap_ImageInfo *info, uint32_t *height);

/**
 * @brief 获取行跨距。
 *
 * @param info 被操作的OH_Pixelmap_ImageInfo指针。
 * @param rowStride 跨距，内存中每行像素所占的空间。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_PixelmapImageInfo_GetRowStride(OH_Pixelmap_ImageInfo *info, uint32_t *rowStride);

/**
 * @brief 获取像素格式。
 *
 * @param info 被操作的OH_Pixelmap_ImageInfo指针。
 * @param pixelFormat 像素格式。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_PixelmapImageInfo_GetPixelFormat(OH_Pixelmap_ImageInfo *info, int32_t *pixelFormat);

/**
 * @brief 获取透明度类型。
 *
 * @param info 被操作的OH_Pixelmap_ImageInfo指针。
 * @param alphaType 透明度类型{@link PIXELMAP_ALPHA_TYPE}。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_PixelmapImageInfo_GetAlphaType(OH_Pixelmap_ImageInfo *info, int32_t *alphaType);

/**
 * @brief 获取Pixelmap是否为高动态范围的信息。
 *
 * @param info 被操作的OH_Pixelmap_ImageInfo指针。
 * @param isHdr 是否为hdr的布尔值。
 * @return 如果操作成功返回IMAGE_SUCCESS，参数校验错误返回IMAGE_BAD_PARAMETER。具体请参考{@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_PixelmapImageInfo_GetDynamicRange(OH_Pixelmap_ImageInfo *info, bool *isHdr);

/**
 * @brief 释放OH_Pixelmap_ImageInfo指针。
 *
 * @param info 被释放的OH_Pixelmap_ImageInfo指针。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_PixelmapImageInfo_Release(OH_Pixelmap_ImageInfo *info);

/**
 * @brief 通过属性创建PixelMap，默认采用BGRA_8888格式处理数据。
 *
 * @param data BGRA_8888格式的颜色数组。
 * @param dataLength 数组长度。
 * @param options 创建像素的属性。
 * @param pixelmap 被创建的OH_PixelmapNative对象指针。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 如果不支持的操作返回 IMAGE_UNSUPPORTED_OPERATION，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_PixelmapNative_CreatePixelmap(uint8_t *data, size_t dataLength,
    OH_Pixelmap_InitializationOptions *options, OH_PixelmapNative **pixelmap);

/**
 * @brief 将nativePixelMap对象转换为PixelMapnapi对象。
 *
 * @param env napi的环境指针。
 * @param pixelmapNative 被操作的OH_PixelmapNative指针。
 * @param pixelmapNapi 转换出来的PixelMapnapi对象指针。
 * @return 如果操作成功返回 IMAGE_SUCCESS，
 * 如果pixelmapNative为空返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_PixelmapNative_ConvertPixelmapNativeToNapi(napi_env env, OH_PixelmapNative *pixelmapNative,
    napi_value *pixelmapNapi);

/**
 * @brief 将PixelMapnapi对象转换为nativePixelMap对象。
 *
 * @param env napi的环境指针。
 * @param pixelmapNapi 需要转换的PixelMapnapi对象。
 * @param pixelmapNative 转换出的OH_PixelmapNative对象指针。
 * @return 如果操作成功返回 IMAGE_SUCCESS，
 * 如果pixelmapNative是nullptr，或者pixelmapNapi不是PixelMapNapi对象返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_PixelmapNative_ConvertPixelmapNativeFromNapi(napi_env env, napi_value pixelmapNapi,
    OH_PixelmapNative **pixelmapNative);

/**
 * @brief 读取图像像素数据，结果写入ArrayBuffer里，使用Promise形式返回。
 * 指定BGRA_8888格式创建pixelmap，读取的像素数据与原数据保持一致。
 *
 * @param pixelmap 被操作的OH_PixelmapNative指针。
 * @param destination 缓冲区，获取的图像像素数据写入到该内存区域内。
 * @param bufferSize 缓冲区大小。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 如果未知错误返回 IMAGE_UNKNOWN_ERROR，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_PixelmapNative_ReadPixels(OH_PixelmapNative *pixelmap, uint8_t *destination, size_t *bufferSize);

/**
 * @brief 读取缓冲区中的图片数据，结果写入PixelMap中
 *
 * @param pixelmap 被操作的OH_PixelmapNative指针。
 * @param source 图像像素数据。
 * @param bufferSize 图像像素数据长度。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 如果不支持的操作返回 IMAGE_UNSUPPORTED_OPERATION，如果未知错误返回 IMAGE_UNKNOWN_ERROR，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_PixelmapNative_WritePixels(OH_PixelmapNative *pixelmap, uint8_t *source, size_t bufferSize);

/**
 * @brief 从PixelMap中读取ARGB格式的数据。
 *
 * @param pixelmap 被操作的OH_PixelmapNative指针。
 * @param destination 缓冲区，获取的图像像素数据写入到该内存区域内。
 * @param bufferSize 缓冲区大小。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 如果PixelMap格式不支持读取ARGB数据，返回 IMAGE_UNSUPPORTED_CONVERSION，如果内存申请失败，返回 IMAGE_ALLOC_FAILED,
 * 如果内存数据拷贝、读取、操作失败，返回 IMAGE_COPY_FAILED。
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 13
 */
Image_ErrorCode OH_PixelmapNative_GetArgbPixels(OH_PixelmapNative *pixelmap, uint8_t *destination, size_t *bufferSize);

/**
 * @brief 将HDR的图像内容转换为SDR的图像内容。
 *
 * @param pixelmap 被操作的OH_PixelmapNative指针。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 如果不支持的操作返回 IMAGE_UNSUPPORTED_OPERATION，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_PixelmapNative_ToSdr(OH_PixelmapNative *pixelmap);

/**
 * @brief 获取图像像素信息。
 *
 * @param pixelmap 被操作的OH_PixelmapNative指针。
 * @param imageInfo 图像像素信息。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_PixelmapNative_GetImageInfo(OH_PixelmapNative *pixelmap, OH_Pixelmap_ImageInfo *imageInfo);

/**
 * @brief 通过设置透明比率来让PixelMap达到对应的透明效果
 *
 * @param pixelmap 被操作的OH_PixelmapNative指针。
 * @param rate 透明比率的值。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_PixelmapNative_Opacity(OH_PixelmapNative *pixelmap, float rate);

/**
 * @brief 根据输入的宽高对图片进行缩放。
 *
 * @param pixelmap 被操作的OH_PixelmapNative指针。
 * @param scaleX 宽度的缩放比例。
 * @param scaleY 高度的缩放比例。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_PixelmapNative_Scale(OH_PixelmapNative *pixelmap, float scaleX, float scaleY);

/**
 * @brief 根据指定的缩放算法和输入的宽高对图片进行缩放。
 *
 * @param pixelmap 被操作的OH_PixelmapNative指针。
 * @param scaleX 宽度的缩放比例。
 * @param scaleY 高度的缩放比例。
 * @param level 缩放算法。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 如果图片过大返回 IMAGE_TOO_LARGE，如果内存申请失败返回 IMAGE_ALLOC_FAILED，
 * 如果pixelmap已经被释放返回 IMAGE_UNKNOWN_ERROR，具体请参考 {@link Image_ErrorCode}。
 *
 * @since 12
 */
Image_ErrorCode OH_PixelmapNative_ScaleWithAntiAliasing(OH_PixelmapNative *pixelmap, float scaleX, float scaleY,
    OH_PixelmapNative_AntiAliasingLevel level);

/**
 * @brief 根据输入的宽高的缩放比例，创建一个新的缩放后的图片。
 *
 * @param srcpixelmap 被操作的OH_PixelmapNative指针，源pixelmap对象指针。
 * @param dstpixelmap 被操作的OH_PixelmapNative指针，目标pixelmap对象指针。
 * @param scaleX 宽度的缩放比例。
 * @param scaleY 高度的缩放比例。
 * @return 如果操作成功返回 {@link IMAGE_SUCCESS}
 *         如果参数错误返回 {@link IMAGE_BAD_PARAMETER}
 * @since 16
 */
Image_ErrorCode OH_PixelmapNative_CreateScaledPixelMap(OH_PixelmapNative *srcPixelmap, OH_PixelmapNative **dstPixelmap,
    float scaleX, float scaleY);

/**
 * @brief 根据指定的缩放算法和输入的宽高的缩放比例，创建一个新的缩放后的图片。
 *
 * @param srcpixelmap 被操作的OH_PixelmapNative指针，源pixelmap对象指针。
 * @param dstpixelmap 被操作的OH_PixelmapNative指针，目标pixelmap对象指针。
 * @param scaleX 宽度的缩放比例。
 * @param scaleY 高度的缩放比例。
 * @param level 缩放算法。
 * @return 如果操作成功返回 {@link IMAGE_SUCCESS}
 *         如果参数错误返回 {@link IMAGE_BAD_PARAMETER}
 *         如果图片过大返回 {@link IMAGE_TOO_LARGE}
 *         如果内存申请失败返回 {@link IMAGE_ALLOC_FAILED}
 * @since 16
 */
Image_ErrorCode OH_PixelmapNative_CreateScaledPixelMapWithAntiAliasing(OH_PixelmapNative *srcPixelmap,
    OH_PixelmapNative **dstPixelmap, float scaleX, float scaleY, OH_PixelmapNative_AntiAliasingLevel level);

/**
 * @brief 根据输入的坐标对图片进行位置变换。
 *
 * @param pixelmap 被操作的OH_PixelmapNative指针。
 * @param x 区域横坐标。
 * @param y 区域纵坐标。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_PixelmapNative_Translate(OH_PixelmapNative *pixelmap, float x, float y);

/**
 * @brief 根据输入的角度对图片进行旋转。
 *
 * @param pixelmap 被操作的OH_PixelmapNative指针。
 * @param angle 图片旋转的角度，单位为deg。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_PixelmapNative_Rotate(OH_PixelmapNative *pixelmap, float angle);

/**
 * @brief 根据输入的条件对图片进行翻转。
 *
 * @param pixelmap 被操作的OH_PixelmapNative指针。
 * @param shouldFilpHorizontally 是否水平翻转图像。
 * @param shouldFilpVertically 是否垂直翻转图像。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_PixelmapNative_Flip(OH_PixelmapNative *pixelmap, bool shouldFilpHorizontally, bool shouldFilpVertically);

/**
 * @brief 根据输入的尺寸对图片进行裁剪
 *
 * @param pixelmap 被操作的OH_PixelmapNative指针。
 * @param region 裁剪的尺寸。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_PixelmapNative_Crop(OH_PixelmapNative *pixelmap, Image_Region *region);

/**
 * @brief 释放OH_PixelmapNative指针。
 *
 * @param pixelmap 被释放的OH_PixelmapNative指针。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_PixelmapNative_Release(OH_PixelmapNative *pixelmap);

/**
 * @brief 将pixlemap的像素数据做预乘和非预乘之间的转换。
 *
 * @param srcpixelmap 被操作的OH_PixelmapNative指针, 源pixelmap对象指针。
 * @param dstpixelmap 被操作的OH_PixelmapNative指针, 目标pixelmap对象指针。
 * @param isPremul 转换方式，true为非预乘转预乘，false为预乘转非预乘。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_PixelmapNative_ConvertAlphaFormat(OH_PixelmapNative* srcpixelmap,
    OH_PixelmapNative* dstpixelmap, const bool isPremul);

/**
 * @brief 利用OH_Pixelmap_InitializationOptions创建空的pixelmap对象，内存数据为0。
 *
 * @param options 创建像素的属性。
 * @param pixelmap 被创建的OH_PixelmapNative对象指针。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_PixelmapNative_CreateEmptyPixelmap(OH_Pixelmap_InitializationOptions *options,
    OH_PixelmapNative **pixelmap);

/**
 * @brief 从DMA内存的PixelMap中，获取NativeBuffer对象。
 *
 * @param pixelmap 要获取NativeBuffer的源PixelMap。
 * @param nativeBuffer 被创建的NativeBuffer对象指针。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 如果不是DMA内存返回IMAGE_DMA_NOT_EXIST，如果DMA内存操作失败返回IMAGE_DMA_OPERATION_FAILED。
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 12
 */
Image_ErrorCode OH_PixelmapNative_GetNativeBuffer(OH_PixelmapNative *pixelmap, OH_NativeBuffer **nativeBuffer);

/**
 * @brief 获取元数据。
 *
 * @param pixelmap 被操作的OH_PixelmapNative指针。
 * @param key 元数据的关键字，参见{@link OH_Pixelmap_HdrMetadataKey}。
 * @param value 元数据的值，参见{@link OH_Pixelmap_HdrMetadataValue}。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 如果不存在DMA内存返回 IMAGE_DMA_NOT_EXIST，如果内存拷贝失败返回 IMAGE_COPY_FAILED，
 * 具体请参考 {@link Image_ErrorCode}。
 *
 * @since 12
 */
Image_ErrorCode OH_PixelmapNative_GetMetadata(OH_PixelmapNative *pixelmap, OH_Pixelmap_HdrMetadataKey key,
    OH_Pixelmap_HdrMetadataValue **value);

/**
 * @brief 设置元数据。
 *
 * @param pixelmap 被操作的OH_PixelmapNative指针。
 * @param key 元数据的关键字，参见{@link OH_Pixelmap_HdrMetadataKey}。
 * @param value 元数据的值，参见{@link OH_Pixelmap_HdrMetadataValue}。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER，
 * 如果不存在DMA内存返回 IMAGE_DMA_NOT_EXIST，如果内存拷贝失败返回 IMAGE_COPY_FAILED，
 * 具体请参考 {@link Image_ErrorCode}。
 *
 * @since 12
 */
Image_ErrorCode OH_PixelmapNative_SetMetadata(OH_PixelmapNative *pixelmap, OH_Pixelmap_HdrMetadataKey key,
    OH_Pixelmap_HdrMetadataValue *value);

/**
 * @brief 设置NativeColorSpaceManager对象。
 *
 * @param pixelmap 要设置NativeColorSpaceManager的目标PixelMap。
 * @param colorSpaceNative 要设置的NativeColorSpaceManager对象。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER。
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 13
 */
Image_ErrorCode OH_PixelmapNative_SetColorSpaceNative(OH_PixelmapNative *pixelmap,
    OH_NativeColorSpaceManager *colorSpaceNative);

/**
 * @brief 获取NativeColorSpaceManager对象。
 *
 * @param pixelmap 获取到NativeColorSpaceManager的源PixelMap。
 * @param colorSpaceNative 获取到的NativeColorSpaceManager对象。
 * @return 如果操作成功返回 IMAGE_SUCCESS，如果参数错误返回 IMAGE_BAD_PARAMETER。
 * 具体请参考 {@link Image_ErrorCode}。
 * @since 13
 */
Image_ErrorCode OH_PixelmapNative_GetColorSpaceNative(OH_PixelmapNative *pixelmap,
    OH_NativeColorSpaceManager **colorSpaceNative);

/**
 * @brief 设置pixelMap内存名字。
 *
 * @param pixelmap 被操作的OH_PixelmapNative指针。
 * @param name 需要被设置的PixelMap内存名称。
 * @param size 需要被设置的PixelMap内存名称的字节大小。
 * @return 如果操作成功返回的是IMAGE_SUCCESS，如果名字长度超出31位或者小于1位会返回IMAGE_BAD_PARAMETER。
 * 如果既不是DMA内存也不是ASHMEM内存返回IMAGE_UNSUPPORTED_MEMORY_FORMAT。
 *
 * @since 13
 */
Image_ErrorCode OH_PixelmapNative_SetMemoryName(OH_PixelmapNative *pixelmap, char *name, size_t *size);

/**
 * @brief 获取Pixelmap中所有像素所占用的总字节数，不包含内存填充。
 *
 * @param pixelmap 被操作的Pixelmap指针。
 * @param byteCount 获取的总字节数。
 * @return 如果操作成功则返回{@link IMAGE_SUCCESS}，
 *         如果pixelmap或byteCount参数无效则返回{@link IMAGE_BAD_PARAMETER}。
 *         具体请参考 {@link Image_ErrorCode}。
 * @since 16
 */
Image_ErrorCode OH_PixelmapNative_GetByteCount(OH_PixelmapNative *pixelmap, uint32_t *byteCount);

/**
 * @brief 获取Pixelmap用于储存像素数据的内存字节数。
 *
 * @param pixelmap 被操作的Pixelmap指针。
 * @param allocationByteCount 获取的内存字节数。
 * @return 如果操作成功则返回{@link IMAGE_SUCCESS}，
 *         如果pixelmap或allocationByteCount参数无效则返回{@link IMAGE_BAD_PARAMETER}。
 *         具体请参考 {@link Image_ErrorCode}。
 * @since 16
 */
Image_ErrorCode OH_PixelmapNative_GetAllocationByteCount(OH_PixelmapNative *pixelmap, uint32_t *allocationByteCount);

/**
 * @brief 获取Pixelmap像素数据的内存地址，并锁定这块内存。\n
 *        当该内存被锁定时，任何修改或释放该Pixelmap的像素数据的操作均会失败或无效。
 *
 * @param pixelmap 被操作的Pixelmap指针。
 * @param addr Pixelmap内存地址的双指针。
 * @return 如果操作成功则返回{@link IMAGE_SUCCESS}，
 *         如果pixelmap或addr参数无效则返回{@link IMAGE_BAD_PARAMETER}，
 *         如果内存锁定失败则返回{@link IMAGE_LOCK_UNLOCK_FAILED}。
 *         具体请参考 {@link Image_ErrorCode}。
 * @since 16
 */
Image_ErrorCode OH_PixelmapNative_AccessPixels(OH_PixelmapNative *pixelmap, void **addr);

/**
 * @brief 释放Pixelmap像素数据的内存锁。\n
 *        该函数需要与{@link OH_PixelmapNative_AccessPixels}匹配使用。
 *
 * @param pixelmap 被操作的Pixelmap指针。
 * @return 如果操作成功则返回{@link IMAGE_SUCCESS}，
 *         如果pixelmap参数无效则返回{@link IMAGE_BAD_PARAMETER}，
 *         如果内存解锁失败则返回{@link IMAGE_LOCK_UNLOCK_FAILED}。
 *         具体请参考 {@link Image_ErrorCode}。
 * @since 16
 */
Image_ErrorCode OH_PixelmapNative_UnaccessPixels(OH_PixelmapNative *pixelmap);

#ifdef __cplusplus
};
#endif
/** @} */
#endif // INTERFACES_KITS_NATIVE_INCLUDE_IMAGE_PIXELMAP_NATIVE_H_
