/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef NATIVE_NET_CONN_API_H
#define NATIVE_NET_CONN_API_H

/**
 * @addtogroup NetConnection
 * @{
 *
 * @brief 为网络管理数据网络连接模块提供C接口。
 *
 * @since 11
 * @version 1.0
 */

/**
 * @file net_connection.h
 *
 * @brief 为网络管理数据网络连接模块提供C接口.
 *
 * @syscap SystemCapability.Communication.NetManager.Core
 * @library libnet_connection.so
 * @since 11
 * @version 1.0
 */

#include <netdb.h>

#include "net_connection_type.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 查询是否有默认激活的数据网络.
 *
 * @param hasDefaultNet 是否有默认网络.
 * @return 0 - 成功. 201 - 缺少权限.
 *         401 - 参数错误. 2100002 - 无法连接到服务.
 *         2100003 - 内部错误.
 * @permission ohos.permission.GET_NETWORK_INFO
 * @syscap SystemCapability.Communication.NetManager.Core
 * @since 11
 * @version 1.0
 */
int32_t OH_NetConn_HasDefaultNet(int32_t *hasDefaultNet);

/**
 * @brief 获取激活的默认的数据网络.
 *
 * @param netHandle 存放网络ID.
 * @return 0 - 成功. 201 - 缺少权限.
 *         401 - 参数错误. 2100002 - 无法连接到服务.
 *         2100003 - 内部错误.
 * @permission ohos.permission.GET_NETWORK_INFO
 * @syscap SystemCapability.Communication.NetManager.Core
 * @since 11
 * @version 1.0
 */
int32_t OH_NetConn_GetDefaultNet(NetConn_NetHandle *netHandle);

/**
 * @brief 查询默认数据网络是否记流量.
 *
 * @param isMetered 是否激活.
 * @return 0 - 成功. 201 - 缺少权限.
 *         401 - 参数错误. 2100002 - 无法连接到服务.
 *         2100003 - 内部错误.
 * @permission ohos.permission.GET_NETWORK_INFO
 * @syscap SystemCapability.Communication.NetManager.Core
 * @since 11
 * @version 1.0
 */
int32_t OH_NetConn_IsDefaultNetMetered(int32_t *isMetered);

/**
 * @brief 查询某个数据网络的链路信息.
 *
 * @param nethandle 存放网络ID.
 * @param prop 存放链路信息.
 * @return 0 - 成功. 201 - 缺少权限.
 *         401 - 参数错误. 2100002 - 无法连接到服务.
 *         2100003 - 内部错误.
 * @permission ohos.permission.GET_NETWORK_INFO
 * @syscap SystemCapability.Communication.NetManager.Core
 * @since 11
 * @version 1.0
 */
int32_t OH_NetConn_GetConnectionProperties(NetConn_NetHandle *netHandle, NetConn_ConnectionProperties *prop);

/**
 * @brief 查询某个网络的能力集.
 *
 * @param netHandle 存放网络ID.
 * @param netCapacities 存放能力集.
 * @return 0 - 成功. 201 - 缺少权限.
 *         401 - 参数错误. 2100002 - 无法连接到服务.
 *         2100003 - 内部错误.
 * @permission ohos.permission.GET_NETWORK_INFO
 * @syscap SystemCapability.Communication.NetManager.Core
 * @since 11
 * @version 1.0
 */
int32_t OH_NetConn_GetNetCapabilities(NetConn_NetHandle *netHandle, NetConn_NetCapabilities *netCapacities);

/**
 * @brief 查询默认的网络代理.
 *
 * @param httpProxy 存放代理配置信息.
 * @return 0 - 成功. 201 - 缺少权限.
 *         401 - 参数错误. 2100002 - 无法连接到服务.
 *         2100003 - 内部错误.
 * @syscap SystemCapability.Communication.NetManager.Core
 * @since 11
 * @version 1.0
 */
int32_t OH_NetConn_GetDefaultHttpProxy(NetConn_HttpProxy *httpProxy);

/**
 * @brief 通过netId获取DNS结果.
 *
 * @param host 所需查询的host名.
 * @param serv 服务名.
 * @param hint 指向addrinfo结构体的指针.
 * @param res 存放DNS查询结果，以链表形式返回.
 * @param netId DNS查询netId 为0是使用默认netid查询.
 * @return 0 - 成功. 201 - 缺少权限.
 *         401 - 参数错误. 2100002 - 无法连接到服务.
 *         2100003 - 内部错误.
 * @permission ohos.permission.INTERNET
 * @syscap SystemCapability.Communication.NetManager.Core
 * @since 11
 * @version 1.0
 */
int32_t OH_NetConn_GetAddrInfo(char *host, char *serv, struct addrinfo *hint, struct addrinfo **res, int32_t netId);

/**
 * @brief 释放DNS结果.
 *
 * @param res DNS查询结果链表头.
 * @return 0 - 成功. 201 - 缺少权限.
 *         401 - 参数错误. 2100002 - 无法连接到服务.
 *         2100003 - 内部错误.
 * @permission ohos.permission.INTERNET
 * @syscap SystemCapability.Communication.NetManager.Core
 * @since 11
 * @version 1.0
 */
int32_t OH_NetConn_FreeDnsResult(struct addrinfo *res);

/**
 * @brief 查询所有激活的数据网络.
 *
 * @param netHandleList 网络信息列表.
 * @return 0 - 成功. 201 - 缺少权限.
 *         401 - 参数错误. 2100002 - 无法连接到服务.
 *         2100003 - 内部错误.
 * @permission ohos.permission.GET_NETWORK_INFO
 * @syscap SystemCapability.Communication.NetManager.Core
 * @since 11
 * @version 1.0
 */
int32_t OH_NetConn_GetAllNets(NetConn_NetHandleList *netHandleList);

/**
 * @brief 注册自定义 DNS 解析器.
 *
 * @param resolver 指向自定义 DNS 解析器的指针.
 * @return 0 - 成功. 201 - 缺少权限.
 *         401 - 参数错误. 2100002 - 无法连接到服务.
 *         2100003 - 内部错误.
 * @permission ohos.permission.INTERNET
 * @syscap SystemCapability.Communication.NetManager.Core
 * @since 11
 * @version 1.0
 */
int32_t OHOS_NetConn_RegisterDnsResolver(OH_NetConn_CustomDnsResolver resolver);

/**
 * @brief 取消注册自定义 DNS 解析器.
 *
 * @return 0 - 成功. 201 - 缺少权限.
 *         401 - 参数错误. 2100002 - 无法连接到服务.
 *         2100003 - 内部错误.
 * @permission ohos.permission.INTERNET
 * @syscap SystemCapability.Communication.NetManager.Core
 * @since 11
 * @version 1.0
 */
int32_t OHOS_NetConn_UnregisterDnsResolver(void);

/**
 * @brief 将套接字与指定的网络进行绑定.
 *
 * @param socketFd 套接字文件描述符.
 * @param netHandle 存放网络ID.
 * @return 0 - 成功.
 *         401 - 参数错误.
 *         2100002 - 无法连接到服务.
 *         2100003 - 内部错误.
 * @syscap SystemCapability.Communication.NetManager.Core
 * @since 12
 * @version 1.0
 */
int32_t OH_NetConn_BindSocket(int32_t socketFd, NetConn_NetHandle *netHandle);

/**
 * @brief 为当前应用设置http代理配置信息.
 *
 * @param httpProxy 需要设置的http代理配置信息.
 * @return 0 - 成功.
 *         401 - 参数错误.
 * @syscap SystemCapability.Communication.NetManager.Core
 * @since 12
 * @version 1.0
 */
int32_t OH_NetConn_SetAppHttpProxy(NetConn_HttpProxy *httpProxy);

/**
 * @brief 注册监听应用http代理变化的回调.
 *
 * @param appHttpProxyChange 需要注册的监听回调.
 * @param callbackId 回调注册后生成的id, 关联已注册的回调.
 * @return 0 - 成功.
 *         401 - 参数错误.
 * @syscap SystemCapability.Communication.NetManager.Core
 * @since 12
 * @version 1.0
 */
int32_t OH_NetConn_RegisterAppHttpProxyCallback(OH_NetConn_AppHttpProxyChange appHttpProxyChange, uint32_t *callbackId);

/**
 * @brief 注销监听应用http代理变化的回调.
 *
 * @param callbackId 需要被注销的回调所对应的id.
 * @syscap SystemCapability.Communication.NetManager.Core
 * @since 12
 * @version 1.0
 */
void OH_NetConn_UnregisterAppHttpProxyCallback(uint32_t callbackId);

/**
 * @brief 注册监听网络状态变化的回调.
 *
 * @param netSpecifier 网络特征集.
 * @param callback 注册的回调函数集合.
 * @param timeout 超时时间，单位为毫秒，为0时表示无限等待.
 * @param callbackId 出参，对应本次注册成功的回调.
 * @return 0 - 成功.
 *         201 - 缺少权限.
 *         401 - 参数错误.
 *         2100002 - 无法连接到服务.
 *         2100003 - 内部错误.
 *         2101008 - 回调已注册.
 *         2101022 - 请求数超出了允许的最大值.
 * @permission ohos.permission.GET_NETWORK_INFO
 * @syscap SystemCapability.Communication.NetManager.Core
 * @since 12
 * @version 1.0
 */
int32_t OH_NetConn_RegisterNetConnCallback(NetConn_NetSpecifier *specifier, NetConn_NetConnCallback *netConnCallback,
                                           uint32_t timeout, uint32_t *callbackId);

/**
 * @brief 注册监听默认网络状态变化的回调.
 *
 * @param callback 注册的回调函数集合.
 * @param callbackId 出参，对应本次注册成功的回调.
 * @return 0 - 成功.
 *         201 - 缺少权限.
 *         401 - 参数错误.
 *         2100002 - 无法连接到服务.
 *         2100003 - 内部错误.
 *         2101008 - 回调已注册.
 *         2101022 - 请求数超出了允许的最大值.
 * @permission ohos.permission.GET_NETWORK_INFO
 * @syscap SystemCapability.Communication.NetManager.Core
 * @since 12
 * @version 1.0
 */
int32_t OH_NetConn_RegisterDefaultNetConnCallback(NetConn_NetConnCallback *netConnCallback, uint32_t *callbackId);

 /**
 * @brief 注销监听网络状态变化的回调
 *
 * @param callBackId 需要被注销的回调对应id.
 * @return 0 - 成功.
 *         201 - 缺少权限.
 *         401 - 参数错误.
 *         2100002 - 无法连接到服务.
 *         2100003 - 内部错误.
 *         2101007 - 回调不存在.
 * @permission ohos.permission.GET_NETWORK_INFO
 * @syscap SystemCapability.Communication.NetManager.Core
 * @since 12
 * @version 1.0
 */
int32_t OH_NetConn_UnregisterNetConnCallback(uint32_t callBackId);

#ifdef __cplusplus
}
#endif

/** @} */
#endif /* NATIVE_NET_CONN_API_H */
