/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef OHOS_INPUTMETHOD_TYPES_CAPI_H
#define OHOS_INPUTMETHOD_TYPES_CAPI_H
/**
 * @addtogroup InputMethod
 * @{
 *
 * @brief InputMethod模块提供方法来使用输入法和开发输入法。
 *
 * @since 12
 */

/**
 * @file inputmethod_types_capi.h
 *
 * @brief 提供了输入法相关的类型定义。
 *
 * @library libohinputmethod.so
 * @kit IMEKit
 * @syscap SystemCapability.MiscServices.InputMethodFramework
 * @since 12
 * @version 1.0
 */
#ifdef __cplusplus
extern "C"{
#endif /* __cplusplus */
/**
 * @brief 键盘状态。
 *
 * @since 12
 */
typedef enum InputMethod_KeyboardStatus {
    /**
     * 键盘状态为NONE。
     */
    IME_KEYBOARD_STATUS_NONE = 0,
    /**
     * 键盘状态为隐藏。
     */
    IME_KEYBOARD_STATUS_HIDE = 1,
    /**
     * 键盘状态为显示。
     */
    IME_KEYBOARD_STATUS_SHOW = 2,
} InputMethod_KeyboardStatus;

/**
 * @brief 回车键功能类型。
 *
 * @since 12
 */
typedef enum InputMethod_EnterKeyType {
    /**
     * 未指定。
     */
    IME_ENTER_KEY_UNSPECIFIED = 0,
    /**
     * NONE。
     */
    IME_ENTER_KEY_NONE = 1,
    /**
     * 前往。
     */
    IME_ENTER_KEY_GO = 2,
    /**
     * 搜索。
     */
    IME_ENTER_KEY_SEARCH = 3,
    /**
     * 发送。
     */
    IME_ENTER_KEY_SEND = 4,
    /**
     * 下一步。
     */
    IME_ENTER_KEY_NEXT = 5,
    /**
     * 完成。
     */
    IME_ENTER_KEY_DONE = 6,
    /**
     * 上一步。
     */
    IME_ENTER_KEY_PREVIOUS = 7,
    /**
     * 换行。
     */
    IME_ENTER_KEY_NEWLINE = 8,
} InputMethod_EnterKeyType;

/**
 * @brief 移动方向。
 *
 * @since 12
 */
typedef enum InputMethod_Direction {
    /**
     * NONE。
     */
    IME_DIRECTION_NONE = 0,
    /**
     * 向上。
     */
    IME_DIRECTION_UP = 1,
    /**
     * 向下。
     */
    IME_DIRECTION_DOWN = 2,
    /**
     * 向左。
     */
    IME_DIRECTION_LEFT = 3,
    /**
     * 向右。
     */
    IME_DIRECTION_RIGHT = 4,
} InputMethod_Direction;

/**
 * @brief 编辑框中文本的扩展编辑操作类型。
 *
 * @since 12
 */
typedef enum InputMethod_ExtendAction {
    /**
     * 全选。
     */
    IME_EXTEND_ACTION_SELECT_ALL = 0,
    /**
     * 剪切。
     */
    IME_EXTEND_ACTION_CUT = 3,
    /**
     * 赋值。
     */
    IME_EXTEND_ACTION_COPY = 4,
    /**
     * 粘贴。
     */
    IME_EXTEND_ACTION_PASTE = 5,
} InputMethod_ExtendAction;

/**
 * @brief 文本输入类型。
 *
 * @since 12
 */
typedef enum InputMethod_TextInputType {
    /**
     * NONE。
     */
    IME_TEXT_INPUT_TYPE_NONE = -1,
    /**
     * 文本类型。
     */
    IME_TEXT_INPUT_TYPE_TEXT = 0,
    /**
     * 多行类型。
     */
    IME_TEXT_INPUT_TYPE_MULTILINE = 1,
    /**
     * 数字类型。
     */
    IME_TEXT_INPUT_TYPE_NUMBER = 2,
    /**
     * 电话号码类型。
     */
    IME_TEXT_INPUT_TYPE_PHONE = 3,
    /**
     * 日期类型。
     */
    IME_TEXT_INPUT_TYPE_DATETIME = 4,
    /**
     * 邮箱地址类型。
     */
    IME_TEXT_INPUT_TYPE_EMAIL_ADDRESS = 5,
    /**
     * 链接类型。
     */
    IME_TEXT_INPUT_TYPE_URL = 6,
    /**
     * 密码类型。
     */
    IME_TEXT_INPUT_TYPE_VISIBLE_PASSWORD = 7,
    /**
     * 数字密码类型。
     */
    IME_TEXT_INPUT_TYPE_NUMBER_PASSWORD = 8,
    /**
     * 锁屏密码类型。
     */
    IME_TEXT_INPUT_TYPE_SCREEN_LOCK_PASSWORD = 9,
    /**
     * 用户名类型。
     */
    IME_TEXT_INPUT_TYPE_USER_NAME = 10,
    /**
     * 新密码类型。
     */
    IME_TEXT_INPUT_TYPE_NEW_PASSWORD = 11,
    /**
     * The text input type is NUMBER DECIMAL.
     */
    IME_TEXT_INPUT_TYPE_NUMBER_DECIMAL = 12,
} InputMethod_TextInputType;

/**
 * @brief 私有数据类型。
 *
 * @since 12
 */
typedef enum InputMethod_CommandValueType {
    /**
     * NONE。
     */
    IME_COMMAND_VALUE_TYPE_NONE = 0,
    /**
     * 字符串类型。
     */
    IME_COMMAND_VALUE_TYPE_STRING = 1,
    /**
     * 布尔类型。
     */
    IME_COMMAND_VALUE_TYPE_BOOL = 2,
    /**
     * 32位带符号整数类型。
     */
    IME_COMMAND_VALUE_TYPE_INT32 = 3,
} InputMethod_CommandValueType;

/**
 * @brief 输入法错误码。
 *
 * @since 12
 */
typedef enum InputMethod_ErrorCode {
    /**
     * 成功。
     */
    IME_ERR_OK = 0,

    /**
     * 查询失败。
     */
    IME_ERR_UNDEFINED = 1,
    /**
     * 参数检查失败。
     */
    IME_ERR_PARAMCHECK = 401,
    /**
     * 包管理异常。
     */
    IME_ERR_PACKAGEMANAGER = 12800001,
    /**
     * 输入法应用异常。
     */
    IME_ERR_IMENGINE = 12800002,
    /**
     * 输入框客户端异常。
     */
    IME_ERR_IMCLIENT = 12800003,
    /**
     * 配置固化失败。当保存配置失败时，会报此错误码。
     */
    IME_ERR_CONFIG_PERSIST = 12800005,
    /**
     * 输入法控制器异常。
     */
    IME_ERR_CONTROLLER = 12800006,
    /**
     * 输入法设置器异常。
     */
    IME_ERR_SETTINGS = 12800007,
    /**
     * 输入法管理服务异常。
     */
    IME_ERR_IMMS = 12800008,
    /**
     * 输入框未绑定。
     */
    IME_ERR_DETACHED = 12800009,
    /**
     * 空指针异常。
     */
    IME_ERR_NULL_POINTER = 12802000,
    /**
     * 查询失败。
     */
    IME_ERR_QUERY_FAILED = 12802001,
} InputMethod_ErrorCode;
#ifdef __cplusplus
}
#endif /* __cplusplus */
/** @} */
#endif // OHOS_INPUTMETHOD_TYPES_CAPI_H