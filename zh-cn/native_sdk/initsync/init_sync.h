/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup InitSync
 * @{
 * @brief 提供用于向Init进程通知事件的api。
 *
 * @since 10
 */

/**
 * @file init_sync.h
 * @kit BasicServicesKit
 * @brief 声明用于向Init进程通知事件的api。
 *
 * @syscap SystemCapability.Startup.SystemInfo
 * @since 10
 */

#ifndef BASE_STARTUP_INITLITE_NOTIFY_H
#define BASE_STARTUP_INITLITE_NOTIFY_H

#ifdef __cplusplus
#if __cplusplus
extern "C" {
#endif
#endif

#define EVENT1             0xf
#define EVENT1_WAITTIME    10000 // 10s = 10*1000 * 1 tick(1ms)

#define EVENT2             0xf0
/* 定义EVENT2_WAITTIME 0表示没有使用QS_STAGE2 */
#define EVENT2_WAITTIME    0

#define EVENT3             0xf00
/* 定义EVENT3_WAITTIME 0表示没有使用QS_STAGE3 */
#define EVENT3_WAITTIME    0

/**
 * @brief 通知事件到Init进程。
 * 所有进程都可以调用，通常由被监听进程调用。
 *
 * @param event：需要在监听器（init进程）和通知器之间达成一致。
 *
 * @return 失败时返回-1。
 */
extern int NotifyInit(unsigned long event);

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif
/** @} */
#endif // BASE_STARTUP_INITLITE_NOTIFY_H
