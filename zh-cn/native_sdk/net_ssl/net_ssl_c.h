/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef NET_SSL_C_H
#define NET_SSL_C_H

/**
 * @addtogroup netstack
 * @{
 *
 * @brief 为网络协议栈模块提供c接口
 *
 * @since 11
 * @version 1.0
 */

/**
 * @file net_ssl_c.h
 *
 * @brief 为SSL/TLS证书链校验模块定义C接口
 *
 * @library libnet_ssl.so
 * @syscap SystemCapability.Communication.NetStack
 * @since 11
 * @version 1.0
 */

#include "net_ssl_c_type.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief  对外暴露的证书链校验接口
 *
 * @param cert 用户传入的待校验证书
 * @param caCert   用户指定的证书，若为空则以系统预置证书进行校验
 * @return 0 - 成功
 * 2305001 - 未指定的错误.
 * 2305002 - 无法获取颁发者证书.
 * 2305003 - 无法获取证书吊销列表（CRL）.
 * 2305004 - 无法解密证书签名.
 * 2305005 - 无法解密CRL签名.
 * 2305006 - 无法解码颁发者公钥.
 * 2305007 - 证书签名失败.
 * 2305008 - CRL签名失败.
 * 2305009 - 证书尚未生效.
 * 2305010 - 证书已过期.
 * 2305011 - CRL尚未有效.
 * 2305012 - CRL已过期.
 * 2305023 - 证书已被吊销.
 * 2305024 - 证书颁发机构（CA）无效.
 * 2305027 - 证书不受信任.
 * @syscap SystemCapability.Communication.NetStack
 * @since 11
 * @version 1.0
 */
uint32_t OH_NetStack_VerifyCertification(const struct NetStack_CertBlob *cert, const struct NetStack_CertBlob *caCert);

/**
 * @brief 获取证书锁定信息.
 *
 * @param hostname 主机名.
 * @param pin 证书锁定信息的结构体.
 * @return 0 - 成功.
 *         401 - 参数设置错误.
 *         2305999 - 内存错误.
 * @syscap SystemCapability.Communication.NetStack
 * @since 12
 * @version 1.0
 */
int32_t OH_NetStack_GetPinSetForHostName(const char *hostname, NetStack_CertificatePinning *pin);

/**
 * @brief 获取证书信息.
 *
 * @param hostname 主机名.
 * @param certs 证书信息的结构体
 * @return 0 - 成功.
 *         401 - 参数设置错误.
 *         2305999 - 内存错误.
 * @syscap SystemCapability.Communication.NetStack
 * @since 12
 * @version 1.0
 */
int32_t OH_NetStack_GetCertificatesForHostName(const char *hostname, NetStack_Certificates *certs);

/**
 * @brief 释放证书内容, 当NetStack_Certificates使用结束时调用此方法释放该结构体中证书的内存.
 *
 * @param certs 证书信息的结构体.
 * @syscap SystemCapability.Communication.NetStack
 * @since 12
 * @version 1.0
 */
void OH_Netstack_DestroyCertificatesContent(NetStack_Certificates *certs);

#ifdef __cplusplus
}
#endif

#endif // NET_SSL_C_H
