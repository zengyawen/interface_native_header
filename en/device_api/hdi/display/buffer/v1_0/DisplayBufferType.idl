/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Display
 * @{
 *
 * @brief Defines driver interfaces of the display module.
 *
 * This module provides driver interfaces for upper-layer graphics services, including layer management, device control, and display buffer management.
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @file DisplayBufferType.idl
 *
 * @brief Declares the data types used by the interfaces related to display buffer operations.
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @brief Defines the information about the buffer to allocate.
 *
 */
struct AllocInfo {
    unsigned int width;               /**< Width of the buffer to allocate */
    unsigned int height;              /**< Height of the buffer to allocate */
    unsigned long usage;              /**< Expected usage of the buffer */
    unsigned int format;              /**< Format of the buffer to allocate */
    unsigned int expectedSize;        /**< Size of the buffer to allocate */
};

/**
 * @brief Defines the structure used to verify the allocated buffer information.
 *
 */
struct VerifyAllocInfo {
    unsigned int width;               /**< Width of the allocated buffer */
    unsigned int height;              /**< Height of the allocated buffer */
    unsigned long usage;              /**< Actual usage of the allocated buffer */
    unsigned int format;              /**< Pixel format of the allocated buffer */
};
/** @} */
