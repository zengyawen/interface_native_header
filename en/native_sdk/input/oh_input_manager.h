/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef OH_INPUT_MANAGER_H
#define OH_INPUT_MANAGER_H

/**
 * @addtogroup input
 * @{
 *
 * @brief Provides C APIs for the multimodal input module.
 *
 * @since 12
 */

/**
 * @file oh_input_manager.h
 *
 * @brief Provides functions such as event injection and status query.
 *
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @library libohinput.so
 * @since 12
 */

#include <stdint.h>

#include "oh_key_code.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Provides enum values of the key status.
 *
 * @since 12
 */
typedef enum Input_KeyStateAction {
    /** Default status */
    KEY_DEFAULT = -1,
    /** Pressing of a key */
    KEY_PRESSED = 0,
    /** Release of a key */
    KEY_RELEASED = 1,
    /** Enabling of the key switch */
    KEY_SWITCH_ON = 2,
    /** Disabling of the key switch */
    KEY_SWITCH_OFF = 3
} Input_KeyStateAction;

/**
 * @brief Provides enum values of the key event type.
 *
 * @since 12
 */
typedef enum Input_KeyEventAction {
    /** Button action canceled */
    KEY_ACTION_CANCEL = 0,
    /** Pressing of a key */
    KEY_ACTION_DOWN = 1,
    /** Release of a key */
    KEY_ACTION_UP = 2,
} Input_KeyEventAction;

/**
 * @brief Provides enum values of the mouse action.
 *
 * @since 12
 */
typedef enum Input_MouseEventAction {
    /** Mouse action canceled */
    MOUSE_ACTION_CANCEL = 0,
    /** Moving of the mouse pointer */
    MOUSE_ACTION_MOVE = 1,
    /** Pressing of the mouse button */
    MOUSE_ACTION_BUTTON_DOWN = 2,
    /** Release of the mouse button */
    MOUSE_ACTION_BUTTON_UP = 3,
    /** Beginning of the mouse axis event */
    MOUSE_ACTION_AXIS_BEGIN = 4,
    /** Updating of the mouse axis event */
    MOUSE_ACTION_AXIS_UPDATE = 5,
    /** End of the mouse axis event */
    MOUSE_ACTION_AXIS_END = 6,
} Input_MouseEventAction;

/**
 * @brief Provides enum values of the mouse axis event type.
 *
 * @since 12
 */
typedef enum InputEvent_MouseAxis {
    /** Vertical scroll axis */
    MOUSE_AXIS_SCROLL_VERTICAL = 0,
    /** Horizontal scroll axis */
    MOUSE_AXIS_SCROLL_HORIZONTAL = 1,
} InputEvent_MouseAxis;

/**
 * @brief Provides enum values of the mouse button.
 *
 * @since 12
 */
typedef enum Input_MouseEventButton {
    /** Invalid key */
    MOUSE_BUTTON_NONE = -1,
    /** Left button on the mouse */
    MOUSE_BUTTON_LEFT = 0,
    /** Middle mouse button */
    MOUSE_BUTTON_MIDDLE = 1,
    /** Right button on the mouse */
    MOUSE_BUTTON_RIGHT = 2,
    /** Forward button on the mouse */
    MOUSE_BUTTON_FORWARD = 3,
    /** Back button on the mouse */
    MOUSE_BUTTON_BACK = 4,
} Input_MouseEventButton;

/**
 * @brief Provides enum values of the touch action.
 *
 * @since 12
 */
typedef enum Input_TouchEventAction {
    /** Cancellation of touch */
    TOUCH_ACTION_CANCEL = 0,
    /** Pressing of a touch point */
    TOUCH_ACTION_DOWN = 1,
    /** Moving of a touch point */
    TOUCH_ACTION_MOVE = 2,
    /** Lifting of a touch point */
    TOUCH_ACTION_UP = 3,
} Input_TouchEventAction;

/**
 * @brief Defines key information, which identifies a key pressing behavior.
 * For example, the Ctrl key information contains the key value and key type.
 *
 * @since 12
 */
struct Input_KeyState;

/**
 * @brief Defines the key event to be injected.
 *
 * @since 12
 */
struct Input_KeyEvent;

/**
 * @brief Defines the mouse event to be injected.
 *
 * @since 12
 */
struct Input_MouseEvent;

/**
 * @brief Defines the touch event to be injected.
 *
 * @since 12
 */
struct Input_TouchEvent;

/**
 * @brief Provides the enum values of the error code.
 *
 * @since 12
 */
typedef enum Input_Result {
    /** Operation succeeded */
    INPUT_SUCCESS = 0,
    /** Permission verification failed */
    INPUT_PERMISSION_DENIED = 201,
    /** Non-system application */
    INPUT_NOT_SYSTEM_APPLICATION = 202,
    /** Parameter check failed */
    INPUT_PARAMETER_ERROR = 401
} Input_Result;

/**
 * @brief Defines the shortcut key structure.
 *
 * @since 13
 */
typedef struct Input_ShortcutKey Input_ShortcutKey;

/**
 * @brief Queries a key status enum object.
 *
 * @param keyState Key status enum object. For details, see {@Link Input_KeyStateAction}.
 *
 * @return {@Link Input_Result#INPUT_SUCCESS} if the operation is successful;
 * {@Link Input_Result} otherwise.
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
Input_Result OH_Input_GetKeyState(struct Input_KeyState* keyState);

/**
 * @brief Creates a key status enum object.
 *
 * @return Pointer to {@link Input_KeyState} if the operation is successful;
 * a null pointer otherwise.
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
struct Input_KeyState* OH_Input_CreateKeyState();

/**
 * @brief Destroys a key status enum object.
 *
 * @param keyState Key status enum object. For details, see {@Link Input_KeyStateAction}.
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
void OH_Input_DestroyKeyState(struct Input_KeyState** keyState);

/**
 * @brief Sets the key value of a key status enum object.
 *
 * @param keyState Key status enum object. For details, see {@Link Input_KeyStateAction}.
 * @param keyCode Key value.
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
void OH_Input_SetKeyCode(struct Input_KeyState* keyState, int32_t keyCode);

/**
 * @brief Obtains the key value of a key status enum object.
 * 
 * @param keyState Key status enum object. For details, see {@Link Input_KeyStateAction}.
 * @return Key value of the key status enum object.
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
int32_t OH_Input_GetKeyCode(const struct Input_KeyState* keyState);

/**
 * @brief Sets whether the key specific to a key status enumeration object is pressed.
 * 
 * @param keyState Key status enum object. For details, see {@Link Input_KeyStateAction}.
 * @param keyAction Whether a key is pressed. For details, see {@Link Input_KeyEventAction}.
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
void OH_Input_SetKeyPressed(struct Input_KeyState* keyState, int32_t keyAction);

/**
 * @brief Checks whether the key specific to a key status enum object is pressed.
 * 
 * @param keyState Key status enum object. For details, see {@Link Input_KeyStateAction}.
 * @return Key pressing status of the key status enum object.
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
int32_t OH_Input_GetKeyPressed(const struct Input_KeyState* keyState);

/**
 * @brief Sets the key switch of the key status enum object.
 * 
 * @param keyState Key status enum object. For details, see {@Link Input_KeyStateAction}.
 * @param keySwitch Key switch of the key status enum object.
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
void OH_Input_SetKeySwitch(struct Input_KeyState* keyState, int32_t keySwitch);

/**
 * @brief Obtains the key switch of the key status enum object.
 * 
 * @param keyState Key status enum object. For details, see {@Link Input_KeyStateAction}.
 * @return Key switch of the key status enum object.
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
int32_t OH_Input_GetKeySwitch(const struct Input_KeyState* keyState);

/**
 * @brief Injects a key event.
 *
 * @param keyEvent - Key event to be injected.
 * @return 0 - success
 *         201 - no permission.
 *         401 - parameter error
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
int32_t OH_Input_InjectKeyEvent(const struct Input_KeyEvent* keyEvent);

/**
 * @brief Creates a key event object.
 *
 * @return Pointer to {@link Input_KeyEvent} if the operation is successful;
 * a null pointer otherwise.
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
struct Input_KeyEvent* OH_Input_CreateKeyEvent();

/**
 * @brief Destroys a key event object.
 *
 * @param event Key event object.
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
void OH_Input_DestroyKeyEvent(struct Input_KeyEvent** keyEvent);

/**
 * @brief Sets the key event type.
 *
 * @param event Key event object.
 * @param action Key event type.
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
void OH_Input_SetKeyEventAction(struct Input_KeyEvent* keyEvent, int32_t action);

/**
 * @brief Obtains the key event type.
 *
 * @param event Key event object.
 * @return Key event type.
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
int32_t OH_Input_GetKeyEventAction(const struct Input_KeyEvent* keyEvent);

/**
 * @brief Sets the key value for a key event.
 *
 * @param event Key event object.
 * @param keyCode Key code.
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
void OH_Input_SetKeyEventKeyCode(struct Input_KeyEvent* keyEvent, int32_t keyCode);

/**
 * @brief Obtains the key value of a key event.
 *
 * @param event Key event object.
 * @return Key code.
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
int32_t OH_Input_GetKeyEventKeyCode(const struct Input_KeyEvent* keyEvent);

/**
 * @brief Sets the time when a key event occurs.
 *
 * @param event Key event object.
 * @param actionTime Time when a key event occurs.
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
void OH_Input_SetKeyEventActionTime(struct Input_KeyEvent* keyEvent, int64_t actionTime);

/**
 * @brief Obtains the time when a key event occurs.
 *
 * @param event Key event object.
 * @return Time when a key event occurs.
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
int64_t OH_Input_GetKeyEventActionTime(const struct Input_KeyEvent* keyEvent);

/**
 * @brief Injects a mouse event.
 *
 * @param mouseEvent - Mouse event to be injected.
 * @return 0 - success
 *         201 - no permission.
 *         401 - parameter error
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
int32_t OH_Input_InjectMouseEvent(const struct Input_MouseEvent* mouseEvent);

/**
 * @brief Creates a mouse event object.
 *
 * @return Pointer to {@link Input_MouseEvent} if the operation is successful;
 * a null pointer otherwise.
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
struct Input_MouseEvent* OH_Input_CreateMouseEvent();

/**
 * @brief Destroys a mouse event object.
 *
 * @param mouseEvent Mouse event object.
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
void OH_Input_DestroyMouseEvent(struct Input_MouseEvent** mouseEvent);

/**
 * @brief Sets the action for a mouse event.
 *
 * @param mouseEvent Mouse event object.
 * @param action Mouse action.
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
void OH_Input_SetMouseEventAction(struct Input_MouseEvent* mouseEvent, int32_t action);

/**
 * @brief Obtains the action of a mouse event.
 *
 * @param mouseEvent Mouse event object.
 * @return Mouse action.
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
int32_t OH_Input_GetMouseEventAction(const struct Input_MouseEvent* mouseEvent);

/**
 * @brief Sets the X coordinate for a mouse event.
 *
 * @param mouseEvent Mouse event object.
 * @param displayX X coordinate on the screen.
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
void OH_Input_SetMouseEventDisplayX(struct Input_MouseEvent* mouseEvent, int32_t displayX);

/**
 * @brief Obtains the X coordinate of a mouse event.
 *
 * @param mouseEvent Mouse event object.
 * @return X coordinate on the screen.
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
int32_t OH_Input_GetMouseEventDisplayX(const struct Input_MouseEvent* mouseEvent);

/**
 * @brief Sets the Y coordinate for a mouse event.
 *
 * @param mouseEvent Mouse event object.
 * @param displayY Y coordinate on the screen.
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
void OH_Input_SetMouseEventDisplayY(struct Input_MouseEvent* mouseEvent, int32_t displayY);

/**
 * @brief Obtains the Y coordinate of a mouse event.
 *
 * @param mouseEvent Mouse event object.
 * @return Y coordinate on the screen.
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
int32_t OH_Input_GetMouseEventDisplayY(const struct Input_MouseEvent* mouseEvent);

/**
 * @brief Sets the button for a mouse event.
 *
 * @param mouseEvent Mouse event object.
 * @param button Mouse button.
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
void OH_Input_SetMouseEventButton(struct Input_MouseEvent* mouseEvent, int32_t button);

/**
 * @brief Obtains the button of a mouse event.
 *
 * @param mouseEvent Mouse event object.
 * @return Mouse button.
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
int32_t OH_Input_GetMouseEventButton(const struct Input_MouseEvent* mouseEvent);

/**
 * @brief Sets the axis type for mouse event.
 *
 * @param mouseEvent Mouse event object.
 * @param axisType Axis type, for example, X axis or Y axis.
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
void OH_Input_SetMouseEventAxisType(struct Input_MouseEvent* mouseEvent, int32_t axisType);

/**
 * @brief Obtains the axis type of a mouse event.
 *
 * @param mouseEvent Mouse event object.
 * @return Axis type.
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
int32_t OH_Input_GetMouseEventAxisType(const struct Input_MouseEvent* mouseEvent);

/**
 * @brief Sets the axis value for a mouse axis event.
 *
 * @param mouseEvent Mouse event object.
 * @param axisValue Axis value. A positive value means scrolling forward,
 * and a negative number means scrolling backward.
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
void OH_Input_SetMouseEventAxisValue(struct Input_MouseEvent* mouseEvent, float axisValue);

/**
 * @brief Obtains the axis value of a mouse event.
 *
 * @param mouseEvent Mouse event object.
 * @return Axis value.
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
float OH_Input_GetMouseEventAxisValue(const struct Input_MouseEvent* mouseEvent);

/**
 * @brief Sets the time when a mouse event occurs.
 *
 * @param mouseEvent Mouse event object.
 * @param actionTime Time when a mouse event occurs.
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
void OH_Input_SetMouseEventActionTime(struct Input_MouseEvent* mouseEvent, int64_t actionTime);

/**
 * @brief Obtains the time when a mouse event occurs.
 *
 * @param mouseEvent Mouse event object.
 * @return Time when the mouse event occurs.
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
int64_t OH_Input_GetMouseEventActionTime(const struct Input_MouseEvent* mouseEvent);

/**
 * @brief Injects a touch event.
 *
 * @param touchEvent - Touch event to be injected.
 * @return 0 - success
 *         201 - no permission.
 *         401 - parameter error
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
int32_t OH_Input_InjectTouchEvent(const struct Input_TouchEvent* touchEvent);

/**
 * @brief Creates a touch event object.
 *
 * @return Pointer to {@link Input_TouchEvent} if the operation is successful;
 * a null pointer otherwise.
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
struct Input_TouchEvent* OH_Input_CreateTouchEvent();

/**
 * @brief Destroys a touch event object.
 *
 * @param touchEvent Touch event object.
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
void OH_Input_DestroyTouchEvent(struct Input_TouchEvent** touchEvent);

/**
 * @brief Sets the action for a touch event.
 *
 * @param touchEvent Touch event object.
 * @param action Action of a touch event.
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
void OH_Input_SetTouchEventAction(struct Input_TouchEvent* touchEvent, int32_t action);

/**
 * @brief Obtains the action of a touch event
 *
 * @param touchEvent Touch event object.
 * @return Touch action.
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
int32_t OH_Input_GetTouchEventAction(const struct Input_TouchEvent* touchEvent);

/**
 * @brief Sets the finger ID for the touch event.
 *
 * @param touchEvent Touch event object.
 * @param id Finger ID.
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
void OH_Input_SetTouchEventFingerId(struct Input_TouchEvent* touchEvent, int32_t id);

/**
 * @brief Obtains the finger ID of a touch event.
 *
 * @param touchEvent Touch event object.
 * @return Finger ID.
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
int32_t OH_Input_GetTouchEventFingerId(const struct Input_TouchEvent* touchEvent);

/**
 * @brief Sets the X coordinate for a touch event.
 *
 * @param touchEvent Touch event object.
 * @param displayX X coordinate.
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
void OH_Input_SetTouchEventDisplayX(struct Input_TouchEvent* touchEvent, int32_t displayX);

/**
 * @brief Obtains the X coordinate of a touch event.
 *
 * @param touchEvent Touch event object.
 * @return X coordinate.
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
int32_t OH_Input_GetTouchEventDisplayX(const struct Input_TouchEvent* touchEvent);

/**
 * @brief Sets the Y coordinate for a touch event.
 *
 * @param touchEvent Touch event object.
 * @param displayY Y coordinate.
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
void OH_Input_SetTouchEventDisplayY(struct Input_TouchEvent* touchEvent, int32_t displayY);

/**
 * @brief Obtains the Y coordinate of a touch event.
 *
 * @param touchEvent Touch event object.
 * @return Y coordinate.
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
int32_t OH_Input_GetTouchEventDisplayY(const struct Input_TouchEvent* touchEvent);

/**
 * @brief Sets the time when a touch event occurs.
 *
 * @param touchEvent Touch event object.
 * @param actionTime Time when a touch event occurs
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
void OH_Input_SetTouchEventActionTime(struct Input_TouchEvent* touchEvent, int64_t actionTime);

/**
 * @brief Obtains the time when a touch event occurs.
 *
 * @param touchEvent Touch event object.
 * @return Time when a touch event occurs.
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
int64_t OH_Input_GetTouchEventActionTime(const struct Input_TouchEvent* touchEvent);

/**
 * @brief Stops event injection and revokes authorization.
 *
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 12
 */
void OH_Input_CancelInjection();

/**
 * @brief Obtains the interval since the last system input event.
 * 
 * @param timeInterval Interval, in nanoseconds.
 * @return OH_Input_GetIntervalSinceLastInput status code, specifically,
 *         {@Link INPUT_SUCCESS} if the operation is successful;
 *         {@Link INPUT_SERVICE_EXCEPTION} otherwise.
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 13
 */
int32_t OH_Input_GetIntervalSinceLastInput(int64_t *timeInterval);

/**
 * @brief Creates a shortcut key object.
 *
 * @return Pointer to {@link Input_ShortcutKey} if the operation is successful;
 * a null pointer otherwise (possibly because of a memory allocation fail failure).
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 13
 */
Input_ShortcutKey* OH_Input_CreateShortcutKey();

/**
 * @brief Destroys a shortcut key object.
 *
 * @param shortcutKey Shortcut key object.
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 13
 */
void OH_Input_DestroyShortcutKey(Input_ShortcutKey** shortcutKey);

/**
 * @brief Sets a modifier key.
 *
 * @param shortcutKey Shortcut key object.
 * @param pressedKeys List of modifier keys.
 * @param pressedKeyNum Number of modifier keys. One or two modifier keys are supported.
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 13
 */
void OH_Input_SetPressedKeys(Input_ShortcutKey* shortcutKey, int32_t *pressedKeys, int32_t pressedKeyNum);

/**
 * @brief Obtains a modifier key.
 *
 * @param shortcutKey Shortcut key object.
 * @param pressedKeys List of modifier keys.
 * @param pressedKeyNum Number of modifier keys.
 * @return OH_Input_GetpressedKeys status code, specifically,
 *         {@link INPUT_SUCCESS} if the operation is successful;\n
 *         {@link INPUT_PARAMETER_ERROR} otherwise. \n
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 13
 */
Input_Result OH_Input_GetPressedKeys(const Input_ShortcutKey* shortcutKey, int32_t **pressedKeys,
                                    int32_t *pressedKeyNum);

/**
 * @brief Sets a modified key.
 *
 * @param shortcutKey Shortcut key object.
 * @param finalKey Modified key. Only one modified key is supported.
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 13
 */
void OH_Input_SetFinalKey(Input_ShortcutKey* shortcutKey, int32_t finalKey);

/**
 * @brief Obtains a modified key.
 *
 * @param shortcutKey Shortcut key object.
 * @param finalKeyCode Returns the key value of the decorated key.
 * @return OH_Input_GetfinalKey status code, specifically,
 *         {@link INPUT_SUCCESS} if the operation is successful;\n
 *         {@link INPUT_PARAMETER_ERROR} otherwise. \n
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 13
 */
Input_Result OH_Input_GetFinalKey(const Input_ShortcutKey* shortcutKey, int32_t *finalKeyCode);

/**
 * @brief Sets the status of the modified key.
 *
 * @param shortcutKey Shortcut key object.
 * @param isFinalKeyDown Status of the modified key.
 * true indicates that the key is pressed, and false indicates that the key is released.
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 13
 */
void OH_Input_SetIsFinalKeyDown(Input_ShortcutKey* shortcutKey, bool isFinalKeyDown);

/**
 * @brief Obtains the status of the modified key.
 *
 * @param shortcutKey Shortcut key object.
 * @param isFinalKeyDown Status of the modified key.
 * @return OH_Input_GetIsFinalKeyDown status code, specifically,
 *         {@link INPUT_SUCCESS} if the operation is successful;\n
 *         {@link INPUT_PARAMETER_ERROR} otherwise. \n
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 13
 */
Input_Result OH_Input_GetIsFinalKeyDown(const Input_ShortcutKey* shortcutKey, bool *isFinalKeyDown);

/**
 * @brief Sets whether a key event is repeated.
 *
 * @param shortcutKey Shortcut key object.
 * @param isRepeat whether a key event is repeated.
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 13
 */
void OH_Input_SetIsRepeat(Input_ShortcutKey* shortcutKey, bool isRepeat);

/**
 * @brief Checks whether a key event is repeated.
 *
 * @param shortcutKey Shortcut key object.
 * @param isRepeat Whether a key event is repeated.
 * @return OH_Input_GetIsRepeat status code, specifically,
 *         {@link INPUT_SUCCESS} if the operation is successful;\n
 *         {@link INPUT_PARAMETER_ERROR} otherwise. \n
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 13
 */
Input_Result OH_Input_GetIsRepeat(const Input_ShortcutKey* shortcutKey, bool *isRepeat);

/**
 * @brief Sets the duration for which the modified key is held down.
 *
 * @param shortcutKey Shortcut key object.
 * @param duration Duration for which the modified key is held down, in microseconds.
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 13
 */
void OH_Input_SetFinalKeyDownDuration(Input_ShortcutKey* shortcutKey, int32_t duration);

/**
 * @brief Obtains the duration for which the modified key is held down.
 *
 * @param shortcutKey Shortcut key object.
 * @param duration Duration for which the modified key is held down, in microseconds.
 * @return OH_Input_GetFinalKeyDownDuration status code, specifically,
 *         {@link INPUT_SUCCESS} if the operation is successful;\n
 *         {@link INPUT_PARAMETER_ERROR} otherwise. \n
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 13
 */
Input_Result OH_Input_GetFinalKeyDownDuration(const Input_ShortcutKey* shortcutKey, int32_t *duration);

/**
 * @brief Creates an array of {@Link Input_ShortcutKey} instances.
 *
 * @param count Number of {@link Input_ShortcutKey} instances to be created.
 * @return OH_Input_CreateAllSystemShortcutKey status code, specifically,
 *         {@link INPUT_SUCCESS} if the double pointer to the instance array is successfully created. \n
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 13
 */
Input_ShortcutKey **OH_Input_CreateAllSystemShortcutKey(int32_t count);

/**
 * @brief Destroys an array of {@link Input_ShortcutKey} instances and reclaims memory.
 *
 * @param shortcutKeys Double pointer to the array of {@link Input_ShortcutKey} instances.
 * @param count Number of {@link Input_ShortcutKey} instances.
 * @return OH_Input_DestroyAllSystemShortcutKey status code, specifically,
 *         {@link INPUT_SUCCESS} if the operation is successful. \n
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 13
 */
int32_t OH_Input_DestroyAllSystemShortcutKey(Input_ShortcutKey **shortcutKeys, int32_t count);

/**
 * @brief Obtains all shortcut keys supported by the system.
 *
 * @param shortcutKey Array of {@Link Input_KeyOptions} instances.
 * When calling this API for the first time, you can pass NULL to obtain the array length.
 * @param count Number of shortcut keys supported by the system.
 * @return OH_Input_GetAllSystemShortcutKey status code, specifically,
 *         {@link INPUT_SUCCESS} if the operation is successful;\n
 *         {@link INPUT_PARAMETER_ERROR} otherwise. \n
 * @syscap SystemCapability.MultimodalInput.Input.Core
 * @since 13
 */
int32_t OH_Input_GetAllSystemShortcutKey(Input_ShortcutKey** shortcutKey, int32_t *count);

#ifdef __cplusplus
}
#endif
/** @} */

#endif /* OH_INPUT_MANAGER_H */
