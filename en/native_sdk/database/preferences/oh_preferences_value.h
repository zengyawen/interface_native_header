/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Preferences
 * @{
 *
 * @brief Provides APIs for key-value (KV) data processing, including querying, modifying, and persisting KV data.
 *
 * @syscap SystemCapability.DistributedDataManager.Preferences.Core
 *
 * @since 13
 */

/**
 * @file oh_preferences_value.h
 *
 * @brief Provides APIs, enums, and structs for accessing the <b>PreferencesValue</b> object.
 *
 * File to include: <database/preferences/oh_preferences_value.h>
 * @library libohpreferences.so
 * @syscap SystemCapability.DistributedDataManager.Preferences.Core
 *
 * @since 13
 */

#ifndef OH_PREFERENCES_VALUE_H
#define OH_PREFERENCES_VALUE_H

#include <cstdint>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Enumerates the types of <b>PreferencesValue</b>.
 *
 * @since 13
 */
typedef enum Preference_ValueType {
    /**
     * Null.
     */
    PREFERENCE_TYPE_NULL = 0,
    /**
     * Integer.
     */
    PREFERENCE_TYPE_INT,
    /**
     * Boolean.
     */
    PREFERENCE_TYPE_BOOL,
    /**
     * String.
     */
    PREFERENCE_TYPE_STRING,
    /**
     * End type.
     */
    PREFERENCE_TYPE_BUTT
} Preference_ValueType;

/**
 * @brief Defines a struct for the <b>Preferences</b> data in KV format.
 *
 * @since 13
 */
typedef struct OH_PreferencesPair OH_PreferencesPair;

/**
 * @brief Defines a struct for the <b>PreferencesValue</b> object.
 *
 * @since 13
 */
typedef struct OH_PreferencesValue OH_PreferencesValue;

/**
 * @brief Obtains the key from the give KV pairs based on the specified index.
 *
 * @param pairs Pointer to the target {@link OH_PreferencesPair} instance.
 * @param index Index of the key to obtain.
 * @return Returns the pointer to the key obtained if the operation is successful;
 * returns a null pointer if the operation fails or invalid parameters are specified.
 * @see OH_PreferencesPair
 * @since 13
 */
const char *OH_PreferencesPair_GetKey(const OH_PreferencesPair *pairs, uint32_t index);

/**
 * @brief Obtains the value from the given KV pairs based on the specified index.
 *
 * @param pairs Pointer to the target {@link OH_PreferencesPair} instance.
 * @param index Index of the value to obtain.
 * @return Returns the pointer to the value obtained if the operation is successful;
 * returns a null pointer if the operation fails or invalid parameters are specified.
 * @see OH_PreferencesValue
 * @since 13
 */
const OH_PreferencesValue *OH_PreferencesPair_GetPreferencesValue(const OH_PreferencesPair *pairs, uint32_t index);

/**
 * @brief Obtains the data type of a <b>PreferencesValue</b> instance.
 *
 * @param object Pointer to the target {@link OH_PreferencesValue} instance.
 * @return Returns the data type obtained. If <b>PREFERENCE_TYPE_NULL</b> is returned, invalid parameters are passed in.
 * @see OH_PreferencesValue
 * @since 13
 */
Preference_ValueType OH_PreferencesValue_GetValueType(const OH_PreferencesValue *object);

/**
 * @brief Obtains an integer from an {@link OH_PreferencesValue} instance.
 *
 * @param object Pointer to the target {@link OH_PreferencesValue} instance.
 * @param value Pointer to the integer obtained.
 * @return Returns the error code.
 *         If <b>PREFERENCES_OK</b> is returned, the operation is successful.
 *         If <b>PREFERENCES_ERROR_INVALID_PARAM</b> is returned, invalid parameters are specified.
 *         If <b>PREFERENCES_ERROR_STORAGE</b> is returned, the data storage is abnormal.
 *         If <b>PREFERENCES_ERROR_MALLOC</b> is returned, memory allocation fails.
 * @see OH_PreferencesValue
 * @see OH_Preferences_ErrCode
 * @since 13
 */
int OH_PreferencesValue_GetInt(const OH_PreferencesValue *object, int *value);

/**
 * @brief Obtains a Boolean value from an {@link OH_PreferencesValue} instance.
 *
 * @param object Pointer to the target {@link OH_PreferencesValue} instance.
 * @param value Pointer to the Boolean value obtained.
 * @return Returns the error code.
 *         If <b>PREFERENCES_OK</b> is returned, the operation is successful.
 *         If <b>PREFERENCES_ERROR_INVALID_PARAM</b> is returned, invalid parameters are specified.
 *         If <b>PREFERENCES_ERROR_STORAGE</b> is returned, the data storage is abnormal.
 *         If <b>PREFERENCES_ERROR_MALLOC</b> is returned, memory allocation fails.
 * @see OH_PreferencesValue
 * @see OH_Preferences_ErrCode
 * @since 13
 */
int OH_PreferencesValue_GetBool(const OH_PreferencesValue *object, bool *value);

/**
 * @brief Obtains a string from an {@link OH_PreferencesValue} instance.
 *
 * @param object Pointer to the target {@link OH_PreferencesValue} instance.
 * @param value Double pointer to the string obtained. If the string is no longer required,
 * call {@link OH_Preferences_FreeString} to release the memory.
 * @param valueLen Pointer to the length of the string obtained.
 * @return Returns the error code.
 *         If <b>PREFERENCES_OK</b> is returned, the operation is successful.
 *         If <b>PREFERENCES_ERROR_INVALID_PARAM</b> is returned, invalid parameters are specified.
 *         If <b>PREFERENCES_ERROR_STORAGE</b> is returned, the data storage is abnormal.
 *         If <b>PREFERENCES_ERROR_MALLOC</b> is returned, memory allocation fails.
 * @see OH_PreferencesValue
 * @see OH_Preferences_ErrCode
 * @since 13
 */
int OH_PreferencesValue_GetString(const OH_PreferencesValue *object, char **value, uint32_t *valueLen);
#ifdef __cplusplus
};
#endif

/** @} */
#endif // OH_PREFERENCES_VALUE_H
