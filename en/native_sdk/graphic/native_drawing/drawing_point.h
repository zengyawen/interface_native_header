/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef C_INCLUDE_DRAWING_POINT_H
#define C_INCLUDE_DRAWING_POINT_H

/**
 * @addtogroup Drawing
 * @{
 *
 * @brief Provides the functions for 2D graphics rendering, text drawing, and image display.
 * This module uses the physical pixel unit, px.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file drawing_point.h
 *
 * @brief Declares the functions related to the coordinate point in the drawing module.
 *
 * File to include: native_drawing/drawing_point.h
 * @library libnative_drawing.so
 * @since 11
 * @version 1.0
 */

#include "drawing_error_code.h"
#include "drawing_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Creates an <b>OH_Drawing_Point</b> object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param x X coordinate of the point.
 * @param y Y coordinate of the point.
 * @return Returns the pointer to the <b>OH_Drawing_Point</b> object created.
 * @since 11
 * @version 1.0
 */
OH_Drawing_Point* OH_Drawing_PointCreate(float x, float y);

/**
 * @brief Obtains the X coordinate of a point.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param point Pointer to an {@link OH_Drawing_Point} object.
 * @param x Pointer to the X coordinate.
 * @return Returns either of the following result codes:
 *         <b>OH_DRAWING_SUCCESS</b> if the operation is successful.
 *         <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> if either <b>point</b> or <b>x</b> is NULL.
 * @since 12
 * @version 1.0
 */
OH_Drawing_ErrorCode OH_Drawing_PointGetX(const OH_Drawing_Point* point, float* x);

/**
 * @brief Obtains the Y coordinate of a point.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param point Pointer to an {@link OH_Drawing_Point} object.
 * @param y Pointer to the Y coordinate.
 * @return Returns either of the following result codes:
 *         <b>OH_DRAWING_SUCCESS</b> if the operation is successful.
 *         <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> if either <b>point</b> or <b>y</b> is NULL.
 * @since 12
 * @version 1.0
 */
OH_Drawing_ErrorCode OH_Drawing_PointGetY(const OH_Drawing_Point* point, float* y);

/**
 * @brief Sets the X and Y coordinates of a point.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param point Pointer to an {@link OH_Drawing_Point} object.
 * @param x Pointer to the X coordinate.
 * @param y Pointer to the Y coordinate.
 * @return Returns either of the following result codes:
 *         <b>OH_DRAWING_SUCCESS</b> if the operation is successful.
 *         <b>OH_DRAWING_ERROR_INVALID_PARAMETER</b> if <b>point</b> is NULL.
 * @since 12
 * @version 1.0
 */
OH_Drawing_ErrorCode OH_Drawing_PointSet(OH_Drawing_Point* point, float x, float y);

/**
 * @brief Destroys an <b>OH_Drawing_Point</b> object and reclaims the memory occupied by the object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Point Pointer to an <b>OH_Drawing_Point</b> object.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_PointDestroy(OH_Drawing_Point*);

#ifdef __cplusplus
}
#endif
/** @} */
#endif
